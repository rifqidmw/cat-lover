package dev.pawcat.di

import dev.pawcat.data.remote.PagedListService
import dev.pawcat.data.repo.PagedListRepo
import org.koin.dsl.module

val repositoryModule = module {
    single { createRepository(get()) }
}

fun createRepository(
    pagedListAppService: PagedListService
) : PagedListRepo = PagedListRepo(pagedListAppService)