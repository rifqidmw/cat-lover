package dev.pawcat.di

import dev.pawcat.ui.adoptionlist.AdoptionListViewModel
import dev.pawcat.ui.detailarticle.DetailArticleViewModel
import dev.pawcat.ui.detailcat.DetailCatViewModel
import dev.pawcat.ui.detailpost.DetailPostViewModel
import dev.pawcat.ui.detailreminder.DetailReminderViewModel
import dev.pawcat.ui.detailuser.DetailUserViewModel
import dev.pawcat.ui.editpassword.EditPasswordViewModel
import dev.pawcat.ui.edituserprofile.EditUserProfileViewModel
import dev.pawcat.ui.follow.FollowViewModel
import dev.pawcat.ui.inputpet.InputCatViewModel
import dev.pawcat.ui.inputpost.InputPostViewModel
import dev.pawcat.ui.inputreminder.CatChooserViewModel
import dev.pawcat.ui.inputreminder.InputReminderViewModel
import dev.pawcat.ui.login.LoginViewModel
import dev.pawcat.ui.main.adoption.AdoptionViewModel
import dev.pawcat.ui.main.article.ArticleViewModel
import dev.pawcat.ui.main.home.HomeViewModel
import dev.pawcat.ui.main.mypet.pet.CatListViewModel
import dev.pawcat.ui.main.mypet.reminder.ReminderListViewModel
import dev.pawcat.ui.main.profile.ProfileViewModel
import dev.pawcat.ui.register.RegisterViewModel
import dev.pawcat.ui.searchlist.SearchListViewModel
import dev.pawcat.ui.splash.SplashViewModel
import dev.pawcat.ui.userprofile.UserProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { CatListViewModel(get(), get()) }
    viewModel { ReminderListViewModel(get(), get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { RegisterViewModel(get()) }
    viewModel { UserProfileViewModel(get()) }
    viewModel { EditUserProfileViewModel(get()) }
    viewModel { EditPasswordViewModel(get()) }
    viewModel { InputPostViewModel(get()) }
    viewModel { InputCatViewModel(get(), get(), get()) }
    viewModel { InputReminderViewModel(get()) }
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { CatChooserViewModel(get(), get()) }
    viewModel { DetailReminderViewModel(get()) }
    viewModel { DetailCatViewModel(get(),get(), get()) }
    viewModel { DetailPostViewModel(get(),get(), get()) }
    viewModel { ProfileViewModel(get(),get(), get()) }
    viewModel { FollowViewModel(get(),get(), get()) }
    viewModel { AdoptionListViewModel(get(), get()) }
    viewModel { DetailUserViewModel(get(), get(), get()) }
    viewModel { SearchListViewModel(get(),get(), get(), get()) }
    viewModel { ArticleViewModel(get(),get(), get()) }
    viewModel { AdoptionViewModel(get(), get(), get()) }
    viewModel { DetailArticleViewModel(get(), get(), get()) }
    viewModel { SplashViewModel(get()) }
}