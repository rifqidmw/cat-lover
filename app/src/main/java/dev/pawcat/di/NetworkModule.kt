package dev.pawcat.di

import com.google.gson.Gson
import dev.pawcat.BuildConfig.BASE_URL_API
import dev.pawcat.data.remote.ApiService
import dev.pawcat.data.remote.PagedListService
import dev.pawcat.data.repo.*
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    single { createOkHttpClient() }
    single { createRetrofit(get()) }
    single { createApiService(get()) }
    single { createMovieAppService(get()) }

    //repo
    factory { AuthRepo(get()) }
    factory { PostRepo(get()) }
    factory { CatRepo(get()) }
    factory { ReminderRepo(get()) }
    factory { ArticleRepo(get()) }
}

fun createMovieAppService(
    api: ApiService
) : PagedListService = PagedListService(api)

fun createOkHttpClient(): OkHttpClient {

    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(20L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .writeTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()

}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL_API)
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .client(okHttpClient)
        .build()
}

fun createApiService(retrofit: Retrofit) : ApiService = retrofit.create(
    ApiService::class.java)
