package dev.pawcat.di

import android.content.Context
import dev.pawcat.data.local.Session
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single { createSharedPref(androidContext()) }
//    single { createRoomDatabase(androidContext()) }
}

fun createSharedPref(context: Context) : Session =
    Session(context)
