package dev.pawcat.ui.detailpost

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.AddCommentPostResponse
import dev.pawcat.data.remote.response.CommentPostListResponse
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.PostRepo
import dev.pawcat.data.repo.paged.CommentPostListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DetailPostViewModel(
    private val postRepo: PostRepo,
    private val pagedListRepo: PagedListRepo, private val context: Context
) : RxViewModel<DetailPostState>(), IDetailPostViewModel{
    override val TAG: String
        get() = DetailPostState::class.java.simpleName

    override fun successAddComment(addCommentPostResponse: AddCommentPostResponse) {
        logD(TAG,"success do add comment")
        onLoading(false)
        state.value = DetailPostState.OnSuccessAddComment(addCommentPostResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = DetailPostState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = DetailPostState.OnLoading(isLoading)
    }

    fun comments(postUUID: String): Flow<PagingData<CommentData.CommentItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            CommentPostListPagingSource(headers = headers, pagedListRepo = pagedListRepo, postUUID = postUUID)
        }.flow.map { pagingData -> pagingData.map { CommentData.CommentItem(it) } }
    }

    fun doAddComment(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do add comment post")
        launch {
            onLoading(true)
            postRepo.doAddComment(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddComment) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class CommentData{
    data class CommentItem(val commentPostModel: CommentPostListResponse.Comment) : CommentData()
}