package dev.pawcat.ui.detailpost.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ItemCommentBinding
import dev.pawcat.ui.detailpost.CommentData
import dev.pawcat.utils.*

class PostCommentAdapter(
    private val context: Context,
    private val session: Session
) : PagingDataAdapter<CommentData.CommentItem, PostCommentAdapter.PostCommentViewHolder>(DIIF_UTIL) {

    open inner class PostCommentViewHolder(
        val binding: ItemCommentBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(commentModel: CommentData.CommentItem){
            binding.apply {
                val data = commentModel.commentPostModel
                imgProfile.setPhotoCircularUrl(context, data.userAvatar?:"")
                tvUsername.text = data.userName
                tvComment.text = data.content
                tvDate.text = data.createdAt!!.changeDateFormat().setTimeAgo()

                imgProfile.setOnClickListener {
                    val userUUID = data.usersUUID
                    if (userUUID != session[USER_UUID]){
                        context.intentToUser(userUUID)
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: PostCommentViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostCommentViewHolder {
        val view = LayoutInflater.from(parent.context)

        return PostCommentViewHolder(
            ItemCommentBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<CommentData.CommentItem>(){
            override fun areItemsTheSame(oldItem: CommentData.CommentItem, newItem: CommentData.CommentItem): Boolean {
                return oldItem.commentPostModel.commentUUID == newItem.commentPostModel.commentUUID
            }

            override fun areContentsTheSame(oldItem: CommentData.CommentItem, newItem: CommentData.CommentItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}