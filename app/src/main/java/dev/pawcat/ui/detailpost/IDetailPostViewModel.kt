package dev.pawcat.ui.detailpost

import dev.pawcat.data.remote.response.AddCommentPostResponse

interface IDetailPostViewModel {
    fun successAddComment(addCommentPostResponse: AddCommentPostResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}