package dev.pawcat.ui.detailpost

import dev.pawcat.data.remote.response.AddCommentPostResponse

sealed class DetailPostState {
    data class OnSuccessAddComment(val addCommentPostResponse: AddCommentPostResponse) : DetailPostState()
    data class OnErrorState(val t: Throwable) : DetailPostState()
    data class OnLoading(val isLoading: Boolean) : DetailPostState()
}