package dev.pawcat.ui.detailuser

import dev.pawcat.data.remote.response.DetailUserResponse
import dev.pawcat.data.remote.response.base.BaseResponse

interface IDetailUserViewModel {
    fun successGetProfile(detailUserResponse: DetailUserResponse)
    fun successFollow(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
}