package dev.pawcat.ui.detailuser.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.databinding.ItemPostImageBinding
import dev.pawcat.ui.detailpost.DetailPostActivity
import dev.pawcat.ui.detailuser.PostData
import dev.pawcat.utils.DATA_POST
import dev.pawcat.utils.setImageviewUrl

class PostImageAdapter(
    private val context: Context
) : PagingDataAdapter<PostData.PostItem, PostImageAdapter.PostImageViewHolder>(DIIF_UTIL) {
    open inner class PostImageViewHolder (val binding: ItemPostImageBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(postModel: PostData.PostItem){
            binding.apply {
                val data = postModel.postModel
                imgPost.setImageviewUrl(context, data.image?:"")

                root.setOnClickListener {
                    val intent = Intent(context, DetailPostActivity::class.java)
                    intent.putExtra(DATA_POST, data)
                    context.startActivity(intent)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: PostImageViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostImageViewHolder {
        val view = LayoutInflater.from(parent.context)

        return PostImageViewHolder(
            ItemPostImageBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<PostData.PostItem>(){
            override fun areItemsTheSame(oldItem: PostData.PostItem, newItem: PostData.PostItem): Boolean {
                return oldItem.postModel.uuid == newItem.postModel.uuid
            }

            override fun areContentsTheSame(oldItem: PostData.PostItem, newItem: PostData.PostItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}