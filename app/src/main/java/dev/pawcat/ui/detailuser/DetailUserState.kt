package dev.pawcat.ui.detailuser

import dev.pawcat.data.remote.response.DetailUserResponse
import dev.pawcat.data.remote.response.base.BaseResponse

sealed class DetailUserState {
    data class OnSuccessGetProfile(val detailUserResponse: DetailUserResponse) : DetailUserState()
    data class OnSuccessFollow(val baseResponse: BaseResponse) : DetailUserState()
    data class OnErrorState(val t: Throwable): DetailUserState()
}