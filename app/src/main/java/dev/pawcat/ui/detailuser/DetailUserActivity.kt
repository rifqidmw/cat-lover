package dev.pawcat.ui.detailuser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityDetailUserBinding
import dev.pawcat.ui.adoptionlist.AdoptionListActivity
import dev.pawcat.ui.detailuser.adapter.PostImageAdapter
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class DetailUserActivity : AppCompatActivity(), Observer<DetailUserState> {
    
    private val binding: ActivityDetailUserBinding by lazy { 
        DataBindingUtil.setContentView<ActivityDetailUserBinding>(
            this, R.layout.activity_detail_user
        )
    }
    private val mViewModel: DetailUserViewModel by inject()
    private val session: Session by inject()
    private val postAdapter: PostImageAdapter by lazy { PostImageAdapter(this@DetailUserActivity) }

    private var userUUID: String = ""
    private var statusFollow: String = ""
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        mViewModel.state.observe(this, this@DetailUserActivity)
        
        setupViews()
        loadDataProfile()
    }

    private fun setupViews() {
        binding.apply {
            rvPost.adapter = postAdapter
            rvPost.adapter = postAdapter.withLoadStateFooter(
                footer = FooterLoadingAdapter { postAdapter.retry() }
            )

            btnAdoption.setOnClickListener {
                if (tvCats.text == "0") {
                    toast("No cats available for adoption")
                } else {
                    if (userUUID.isNotEmpty()){
                        val intent = Intent(this@DetailUserActivity, AdoptionListActivity::class.java)
                        intent.putExtra(USER_UUID, userUUID)
                        startActivity(intent)
                    }
                }
            }

            btnFollow.setOnClickListener {
                if (statusFollow.isNotEmpty() && userUUID.isNotEmpty()){
                    setStatusFollow()
                } else {
                    toast("Sorry, we have a problem! Please contact developer to fix this!")
                }
            }

            btnFollower.setOnClickListener {

            }

            btnFollowing.setOnClickListener {

            }

            navBack.setOnClickListener {
                onBackPressed()
                this@DetailUserActivity.finish()
            }
        }
    }

    private fun loadDataProfile() {
        intent.apply {
            userUUID = getStringExtra(USER_UUID)?:""
            mViewModel.doGetProfile(getParamHeader(), userUUID)
            loadDataPost(true)
        }
    }


    private fun loadDataPost(withLoading: Boolean) {
        binding.apply {
            lifecycleScope.launch {
                mViewModel.posts(userUUID, "").collectLatest {

                    postAdapter.submitData(it)
                }
            }

            postAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading) {
                    if (withLoading) {
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    if (loadState.append.endOfPaginationReached) {
                        if (postAdapter.itemCount > 0) {
                            errorState.let {
                                showSnackbar(it?.error?.message ?: "")
                            }
                        }
                    }
                }

            }
        }
    }

    private fun showSnackbar(msg: String) {
        if (msg.isNotEmpty()) {
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                postAdapter.retry()
            })
            snack.show()
        }
    }

    private fun setStatusFollow(){
        binding.apply {
            when(statusFollow){
                "0" -> {
                    statusFollow = "1"
                    btnFollow.text = "Unfollow"
                    btnFollow.background = ContextCompat.getDrawable(this@DetailUserActivity, R.drawable.button_rounded_gradient_red_filled)
                }

                "1" -> {
                    statusFollow = "0"
                    btnFollow.text = "Follow"
                    btnFollow.background = ContextCompat.getDrawable(this@DetailUserActivity, R.drawable.button_rounded_gradient_filled)
                }
            }

            mViewModel.doFollow(getParamHeader(), userUUID)
        }
    }

    override fun onChanged(state: DetailUserState?) {
        when (state) {
            is DetailUserState.OnSuccessGetProfile -> {
                val status = state.detailUserResponse.code ?: ""
                val msg = state.detailUserResponse.message ?: ""

                if (status == "200") {
                    val data = state.detailUserResponse.data

                    val uuid = data?.uuid ?: ""
                    val name = data?.usersName ?: ""
                    val email = data?.usersEmail ?: ""
                    val noTelp = data?.usersPhone ?: ""
                    val post = data?.post ?: ""
                    val cat = data?.cat ?: ""
                    val statusFollowing = data?.statusFollowing?:""
                    val followers = data?.follower ?: ""
                    val following = data?.following ?: ""
                    val avatar = data?.usersAvatar ?: ""

                    binding.apply {
                        imgProfile.setPhotoCircularUrl(this@DetailUserActivity, avatar ?: "")

                        tvName.text = name
                        tvEmail.text = email
                        tvPost.text = post
                        tvFollowers.text = followers
                        tvFollowing.text = following
                        tvCats.text = cat
                        statusFollow = statusFollowing

                        when(statusFollowing){
                            "0" -> {
                                btnFollow.text = "Follow"
                                btnFollow.background = ContextCompat.getDrawable(this@DetailUserActivity, R.drawable.button_rounded_gradient_filled)
                            }

                            "1" -> {
                                btnFollow.text = "Unfollow"
                                btnFollow.background = ContextCompat.getDrawable(this@DetailUserActivity, R.drawable.button_rounded_gradient_red_filled)
                            }
                        }
                    }
                }
            }

            is DetailUserState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }
}