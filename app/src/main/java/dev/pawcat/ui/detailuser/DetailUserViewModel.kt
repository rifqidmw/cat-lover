package dev.pawcat.ui.detailuser

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.DetailUserResponse
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.PostListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DetailUserViewModel(
    private val authRepo: AuthRepo,
    private val pagedListRepo: PagedListRepo,
    private val context: Context
) : RxViewModel<DetailUserState>(), IDetailUserViewModel{
    override val TAG: String
        get() = DetailUserState::class.java.simpleName

    override fun successGetProfile(detailUserResponse: DetailUserResponse) {
        logD(TAG,"success get profile")
        state.value = DetailUserState.OnSuccessGetProfile(detailUserResponse)
    }

    override fun successFollow(baseResponse: BaseResponse) {
        logD(TAG,"success do follow")
        state.value = DetailUserState.OnSuccessFollow(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = DetailUserState.OnErrorState(t)
    }

    fun posts(uuid: String, all:String): Flow<PagingData<PostData.PostItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            PostListPagingSource(headers = headers, uuid = uuid, all = all, pagedListRepo = pagedListRepo)
        }.flow.map { pagingData ->
            pagingData.map {
                PostData.PostItem(it)
            }
        }
    }

    fun doGetProfile(header:Map<String, String>, uuid: String) {
        logD(TAG,"do get detail user")
        launch {
            authRepo.doGetDetailUser(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetProfile) {
                    errorLoad(t = it)
                }
        }
    }

    fun doFollow(header:Map<String, String>, uuid: String) {
        logD(TAG,"do follow")
        launch {
            authRepo.doFollow(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successFollow) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class PostData{
    data class PostItem(val postModel: PostListResponse.Post) : PostData()
}