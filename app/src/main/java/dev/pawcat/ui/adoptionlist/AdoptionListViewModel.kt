package dev.pawcat.ui.adoptionlist

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.CatListPagingSource
import dev.pawcat.utils.getParamHeader
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class AdoptionListViewModel(private val pagedListRepo: PagedListRepo, private val context: Context) : ViewModel()  {

    fun cats(userUUID: String): Flow<PagingData<CatData.CatItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            CatListPagingSource(headers = headers, pagedListRepo = pagedListRepo, userUUID = userUUID, available = "1",
                name = "", jenisPet = "", provinsi = "", kota = "", gender = "", umurMax = "", umurMin = "")
        }.flow.map { pagingData -> pagingData.map { CatData.CatItem(it) } }
    }
}

sealed class CatData{
    data class CatItem(val catModel: CatListResponse.Cat) : CatData()
}