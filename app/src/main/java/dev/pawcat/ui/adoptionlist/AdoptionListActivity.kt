package dev.pawcat.ui.adoptionlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityAdoptionListBinding
import dev.pawcat.ui.adoptionlist.adapter.AdoptionAdapter
import dev.pawcat.utils.USER_UUID
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class AdoptionListActivity : AppCompatActivity() {

    private val binding: ActivityAdoptionListBinding by lazy {
        DataBindingUtil.setContentView<ActivityAdoptionListBinding>(
            this, R.layout.activity_adoption_list
        )
    }
    private val viewModel: AdoptionListViewModel by inject()
    private val session: Session by inject()
    private val adoptionAdapter by lazy { AdoptionAdapter(this@AdoptionListActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
        loadDataCat(true)
    }

    private fun setupViews(){
        binding.apply {
            tvToolbarTitle.text = "Cats for Adoption"

            rvAdoption.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@AdoptionListActivity)
                rvAdoption.adapter = adoptionAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ adoptionAdapter.retry() }
                )
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@AdoptionListActivity.finish()
            }

            executePendingBindings()
        }
    }

    private fun loadDataCat(withLoading: Boolean){
        binding.apply {
            val userUUID = intent.getStringExtra(USER_UUID)?:""
            lifecycleScope.launch {
                viewModel.cats(userUUID).collectLatest {
                    adoptionAdapter.submitData(it)
                }
            }

            adoptionAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if(withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    if (loadState.append.endOfPaginationReached) {
                        if (adoptionAdapter.itemCount > 0) {
                            errorState.let {
                                showSnackbar(it?.error?.message ?: "")
                            }
                        }
                    }
                }
            }

            executePendingBindings()
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                adoptionAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onResume() {
        super.onResume()
        loadDataCat(false)
    }
}