package dev.pawcat.ui.adoptionlist.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.databinding.ItemMypetBinding
import dev.pawcat.ui.adoptionlist.CatData
import dev.pawcat.ui.detailcat.DetailCatActivity
import dev.pawcat.utils.DATA_CAT
import dev.pawcat.utils.setImageviewUrl

class AdoptionAdapter(
    private val context: Context
) : PagingDataAdapter<CatData.CatItem, AdoptionAdapter.AdoptionViewHolder>(DIIF_UTIL){
    open inner class AdoptionViewHolder(
        val binding: ItemMypetBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(catModel: CatData.CatItem){
            val data = catModel.catModel
            binding.apply {
                imgCat.setImageviewUrl(context, data.image?:"")
                tvName.text = data.name
                tvDesc.text = data.content

                when(data.gender!!.toInt()){
                    1 -> {
                        imgGender.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_gender_male))
                    }

                    2 -> {
                        imgGender.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_gener_female))
                    }
                }

                btnDetail.setOnClickListener {
                    val intent = Intent(context, DetailCatActivity::class.java)
                    intent.putExtra(DATA_CAT, data)
                    context.startActivity(intent)
                }

                executePendingBindings()
            }
        }
    }

    override fun onBindViewHolder(holder: AdoptionViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdoptionViewHolder {
        val view = LayoutInflater.from(parent.context)

        return AdoptionViewHolder(
            ItemMypetBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<CatData.CatItem>(){
            override fun areItemsTheSame(oldItem: CatData.CatItem, newItem: CatData.CatItem): Boolean {
                return oldItem.catModel.uuid == newItem.catModel.uuid
            }

            override fun areContentsTheSame(oldItem: CatData.CatItem, newItem: CatData.CatItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}