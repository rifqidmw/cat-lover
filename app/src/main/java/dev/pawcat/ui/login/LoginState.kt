package dev.pawcat.ui.login

import dev.pawcat.data.remote.response.GetProfileResponse
import dev.pawcat.data.remote.response.LoginResponse

sealed class LoginState {
    data class OnSuccessLogin(val loginResponse: LoginResponse): LoginState()
    data class OnSuccessGetProfile(val getProfileResponse: GetProfileResponse) : LoginState()
    data class OnErrorState(val t: Throwable): LoginState()
}