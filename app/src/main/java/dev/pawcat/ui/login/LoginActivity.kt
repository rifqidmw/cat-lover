package dev.pawcat.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityLoginBinding
import dev.pawcat.ui.main.MainActivity
import dev.pawcat.ui.register.RegisterActivity
import dev.pawcat.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.*


class LoginActivity : AppCompatActivity(), Observer<LoginState> {

    private val TAG = LoginActivity::class.java.simpleName
    private val mViewModel: LoginViewModel by inject()
    private val session: Session by inject()
    private val binding: ActivityLoginBinding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(
            this, R.layout.activity_login
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@LoginActivity)
        setupViews()
    }

    private fun setupViews(){
        setupStatusBarGradient(window, this@LoginActivity)

        binding.apply {
            imgLogo.setImageviewLocal(this@LoginActivity, R.drawable.ic_cat)
            imgLogo.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.background_circle_white)

            btnSignin.setOnClickListener {
                doLogin()
            }

            btnSignup.setOnClickListener {
                val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                startActivity(intent)
            }
        }

    }

    private fun doLogin(){
        val email = binding.etEmail.getString()?:""
        val password = binding.etPassword.getString()?:""
        if (email.isNotEmpty() && password.isNotEmpty()){
            showDialogLoading()

            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"

            val jsonObject = JsonObject()
            try {
                jsonObject.addProperty("email", email)
                jsonObject.addProperty("password", password)

                mViewModel.doLogin(headers, jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            if (email.isEmpty()){
                binding.etEmail.error = "Must be fill"
            }

            if (password.isEmpty()){
                binding.etPassword.error = "Must be fill"
            }
        }
    }

    private fun doGetProfile(){
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

        mViewModel.doGetProfile(headers)
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)
    }

    override fun onChanged(state: LoginState?) {
        when(state){
            is LoginState.OnSuccessLogin -> {
                val status = state.loginResponse.code ?: ""

                if (status == "200") {
                    val token = state.loginResponse.token ?: ""

                    session.save(USER_TOKEN, token)

                    doGetProfile()
                } else {
                    val msg = state.loginResponse.error ?: ""
                    dialogLoading.dismiss()
                    toast(msg)
                }
            }

            is LoginState.OnSuccessGetProfile -> {
                val status = state.getProfileResponse.code ?: ""
                val msg = state.getProfileResponse.message ?: ""

                if (status == "200") {
                    val uuid = state.getProfileResponse.data?.uuid ?: ""
                    val name = state.getProfileResponse.data?.name ?: ""
                    val email = state.getProfileResponse.data?.email ?: ""
                    val noTelp = state.getProfileResponse.data?.noTelp ?: ""
                    val post = state.getProfileResponse.data?.post ?: ""
                    val cat = state.getProfileResponse.data?.cat ?: ""
                    val catAvailable = state.getProfileResponse.data?.catAvailable ?: ""
                    val reminder = state.getProfileResponse.data?.reminder ?: ""
                    val followers = state.getProfileResponse.data?.followers ?: ""
                    val following = state.getProfileResponse.data?.following ?: ""
                    val avatar = state.getProfileResponse.data?.avatar ?: ""

                    session.save(USER_UUID, uuid)
                    session.save(USERNAME, name)
                    session.save(USER_EMAIL, email)
                    session.save(USER_PHONE, noTelp)
                    session.save(USER_POST, post)
                    session.save(USER_CAT, cat)
                    session.save(USER_CAT_AVAILABLE, catAvailable)
                    session.save(USER_REMINDER, reminder)
                    session.save(USER_FOLLOWER, followers)
                    session.save(USER_FOLLOWING, following)
                    session.save(USER_AVATAR, avatar)

                    dialogLoading.dismiss()
                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(intent)
                    this@LoginActivity.finish()
                } else {
                    dialogLoading.dismiss()
                    toast(msg)
                }
            }


            is LoginState.OnErrorState -> {
                dialogLoading.dismiss()
                val msg = state.t.localizedMessage ?: ""
                toast(msg)
            }
        }
    }

}