package dev.pawcat.ui.login

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.GetProfileResponse
import dev.pawcat.data.remote.response.LoginResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginViewModel(val authRepo: AuthRepo) : RxViewModel<LoginState>(), ILoginViewModel {
    override val TAG: String
        get() = LoginState::class.java.simpleName

    override fun successLogin(loginResponse: LoginResponse) {
        logD(TAG,"success login")
        state.value = LoginState.OnSuccessLogin(loginResponse)
    }

    override fun successGetProfile(getProfileResponse: GetProfileResponse) {
        logD(TAG,"success get profile")
        state.value = LoginState.OnSuccessGetProfile(getProfileResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = LoginState.OnErrorState(t)
    }


    fun doLogin(header:Map<String, String>,data: JsonObject) {
        logD(TAG,"do login start")
        launch {
            authRepo.doLogin(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLogin) {
                    errorLoad(t = it)
                }
        }
    }

    fun doGetProfile(header:Map<String, String>) {
        logD(TAG,"do get profile")
        launch {
            authRepo.getProfile(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetProfile) {
                    errorLoad(t = it)
                }
        }
    }
}