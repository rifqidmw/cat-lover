package dev.pawcat.ui.login

import dev.pawcat.data.remote.response.GetProfileResponse
import dev.pawcat.data.remote.response.LoginResponse

interface ILoginViewModel {
    fun successLogin(loginResponse: LoginResponse)
    fun successGetProfile(getProfileResponse: GetProfileResponse)
    fun errorLoad(t: Throwable)
}