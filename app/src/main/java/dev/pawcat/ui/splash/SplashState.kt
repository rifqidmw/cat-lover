package dev.pawcat.ui.splash

import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.ui.inputreminder.InputReminderState

sealed class SplashState {
    data class OnSuccessGetProvinsi(val provinsiListResponse: ProvinsiListResponse) : SplashState()
    data class OnSuccessGetKota(val kotaListResponse: KotaListResponse) : SplashState()
    data class OnSuccessGetPetType(val petTypeResponse: PetTypeResponse) : SplashState()
    data class OnErrorState(val t: Throwable) : SplashState()
}