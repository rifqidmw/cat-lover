package dev.pawcat.ui.splash

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.ui.main.profile.ProfileState
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SplashViewModel(
    private val authRepo: AuthRepo
) : RxViewModel<SplashState>(), ISplashViewModel{
    override val TAG: String
        get() = SplashState::class.java.simpleName

    override fun successGetProvinsi(provinsiListResponse: ProvinsiListResponse) {
        logD(TAG,"success get provinsi")
        state.value = SplashState.OnSuccessGetProvinsi(provinsiListResponse)
    }

    override fun successGetKota(kotaListResponse: KotaListResponse) {
        logD(TAG,"success get kota")
        state.value = SplashState.OnSuccessGetKota(kotaListResponse)
    }

    override fun successGetPetType(petTypeResponse: PetTypeResponse) {
        logD(TAG,"success get pet type")
        state.value = SplashState.OnSuccessGetPetType(petTypeResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = SplashState.OnErrorState(t)
    }

    fun doGetProvinsi(header:Map<String, String>) {
        logD(TAG,"do get provinsi")
        launch {
            authRepo.getProvinsi(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetProvinsi) {
                    errorLoad(t = it)
                }
        }
    }

    fun doGetKota(header:Map<String, String>, id: String) {
        logD(TAG,"do get kota")
        launch {
            authRepo.getKota(header, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetKota) {
                    errorLoad(t = it)
                }
        }
    }

    fun doGetPetType(header:Map<String, String>) {
        logD(TAG,"do get pet type")
        launch {
            authRepo.getPetType(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetPetType) {
                    errorLoad(t = it)
                }
        }
    }
}