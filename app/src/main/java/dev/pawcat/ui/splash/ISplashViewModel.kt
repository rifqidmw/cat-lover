package dev.pawcat.ui.splash

import dev.pawcat.data.remote.response.AddReminderResponse
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.data.remote.response.base.BaseResponse

interface ISplashViewModel {
    fun successGetProvinsi(provinsiListResponse: ProvinsiListResponse)
    fun successGetKota(kotaListResponse: KotaListResponse)
    fun successGetPetType(petTypeResponse: PetTypeResponse)
    fun errorLoad(t: Throwable)
}