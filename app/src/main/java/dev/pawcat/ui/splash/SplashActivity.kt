package dev.pawcat.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivitySplashBinding
import dev.pawcat.ui.login.LoginActivity
import dev.pawcat.ui.main.MainActivity
import dev.pawcat.ui.onboard.OnboardActivity
import dev.pawcat.utils.*
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity(), Observer<SplashState> {

    private val binding: ActivitySplashBinding by lazy {
        DataBindingUtil.setContentView<ActivitySplashBinding>(
            this, R.layout.activity_splash
        )
    }
    private val session: Session by inject()
    private val mViewModel: SplashViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mViewModel.state.observe(this, this@SplashActivity)

        setupViews()
    }

    private fun setupViews(){
        setupStatusBarGradient(window, this@SplashActivity)
        binding.apply {
            rootLayout.background = ContextCompat.getDrawable(this@SplashActivity, R.drawable.background_gradient)
            imgLogo.setImageviewLocal(this@SplashActivity, R.drawable.ic_cat)
            imgLogo.background = ContextCompat.getDrawable(this@SplashActivity, R.drawable.background_circle_white)
        }

        Handler(Looper.getMainLooper()).postDelayed({
            setupIntent()
        }, 3000)
    }

    private fun setupIntent(){
        if (session.getBool(IS_FIRST_TIME) == false){
            if (session[USER_TOKEN] != null){
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(intent)
            }
        } else {
            val intent = Intent(this@SplashActivity, OnboardActivity::class.java)
            startActivity(intent)
        }

        this@SplashActivity.finish()
    }

    override fun onChanged(state: SplashState?) {
        when(state){
            is SplashState.OnSuccessGetProvinsi -> {

            }

            is SplashState.OnSuccessGetPetType -> {

            }
        }
    }
}