package dev.pawcat.ui.edituserprofile

import dev.pawcat.data.remote.response.UpdateProfileResponse

interface IEditUserProfileViewModel {
    fun successUpdateProfile(updateProfileResponse: UpdateProfileResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}