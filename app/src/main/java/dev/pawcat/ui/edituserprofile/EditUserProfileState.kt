package dev.pawcat.ui.edituserprofile

import dev.pawcat.data.remote.response.UpdateProfileResponse


sealed class EditUserProfileState {
    data class OnSuccessUpdateProfile(val updateProfileResponse: UpdateProfileResponse) : EditUserProfileState()
    data class OnErrorState(val t: Throwable) : EditUserProfileState()
    data class OnLoading(val isLoading: Boolean) : EditUserProfileState()
}