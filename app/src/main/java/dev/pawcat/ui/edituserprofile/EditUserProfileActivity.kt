package dev.pawcat.ui.edituserprofile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityEditUserProfileBinding
import dev.pawcat.utils.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class EditUserProfileActivity : AppCompatActivity(), Observer<EditUserProfileState> {

    private val binding: ActivityEditUserProfileBinding by lazy {
        DataBindingUtil.setContentView<ActivityEditUserProfileBinding>(
            this, R.layout.activity_edit_user_profile
        )
    }
    private val mViewModel: EditUserProfileViewModel by inject()
    private val session: Session by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@EditUserProfileActivity)

        setupViews()
    }

    private fun setupViews(){
        binding.apply {
            etName.setText(session[USERNAME])
            etEmail.setText(session[USER_EMAIL])
            etPhone.setText(session[USER_PHONE])

            btnUpdate.setOnClickListener {
                doUpdatePassword()
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@EditUserProfileActivity.finish()
            }
        }
    }

    private fun doUpdatePassword(){
        binding.apply {
            val name = etName.getString()
            val email = etEmail.getString()
            val phone = etPhone.getString()

            if (name.isNotEmpty() && email.isNotEmpty() &&
                phone.isNotEmpty()){

                removeErrorStateEdittext()

                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("name", name)
                    jsonObject.addProperty("email", email)
                    jsonObject.addProperty("no_telp", phone)

                    mViewModel.doUpdateProfile(headers, jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                if (name.isEmpty())
                    etName.error = "Must be fill"
                else
                    etLayoutName.isErrorEnabled = false
                if (email.isEmpty())
                    etEmail.error = "Must be fill"
                else
                    etLayoutEmail.isErrorEnabled = false
                if (phone.isEmpty())
                    etPhone.error = "Must be fill"
                else
                    etLayoutPhone.isErrorEnabled = false
            }
        }
    }

    private fun removeErrorStateEdittext(){
        binding.apply {
            etLayoutName.isErrorEnabled = false
            etLayoutEmail.isErrorEnabled = false
            etLayoutPhone.isErrorEnabled = false
        }
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(title: String){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)
        tvTitle.text = title
    }

    override fun onChanged(state: EditUserProfileState?) {
        when(state){
            is EditUserProfileState.OnSuccessUpdateProfile ->{
                val status = state.updateProfileResponse.code?:""
                val msg = state.updateProfileResponse.message?:""

                if (status == "200"){
                    binding.apply {
                        val name = etName.getString()
                        val email = etEmail.getString()
                        val phone = etPhone.getString()

                        session.save(USERNAME, name)
                        session.save(USER_EMAIL, email)
                        session.save(USER_PHONE, phone)
                    }
                    toast(msg)
                } else {
                    toast(msg)
                }
            }

            is EditUserProfileState.OnLoading -> {
                val loading = state.isLoading

                if (loading){
                    showDialogLoading("Update profile...")
                } else {
                    dialogLoading.dismiss()
                }
            }

            is EditUserProfileState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }

}