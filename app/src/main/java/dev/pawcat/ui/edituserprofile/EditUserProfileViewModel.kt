package dev.pawcat.ui.edituserprofile

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.UpdateProfileResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditUserProfileViewModel(
    val authRepo: AuthRepo
) : RxViewModel<EditUserProfileState>(), IEditUserProfileViewModel {
    override val TAG: String
        get() = EditUserProfileState::class.java.simpleName

    override fun successUpdateProfile(updateProfileResponse: UpdateProfileResponse) {
        logD(TAG,"success do update profile")
        onLoading(false)
        state.value = EditUserProfileState.OnSuccessUpdateProfile(updateProfileResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = EditUserProfileState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = EditUserProfileState.OnLoading(isLoading)
    }

    fun doUpdateProfile(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do register")
        launch {
            onLoading(true)
            authRepo.doUpdateProfile(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successUpdateProfile) {
                    errorLoad(t = it)
                }
        }
    }
}