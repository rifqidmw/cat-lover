package dev.pawcat.ui.main.mypet.pet

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.CatListPagingSource
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.getUserUUID
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class CatListViewModel(private val pagedListRepo: PagedListRepo, private val context: Context) : ViewModel() {
    val cats: Flow<PagingData<CatData.CatItem>> = getCatListStream()
        .map { pagingData -> pagingData.map { CatData.CatItem(it) } }

    private fun getCatListStream(): Flow<PagingData<CatListResponse.Cat>> {
        val headers = context.getParamHeader()
        val userUUID = context.getUserUUID()
        return Pager(PagingConfig(20)) {
            CatListPagingSource(headers = headers, pagedListRepo = pagedListRepo, userUUID = userUUID, available = "",
                name = "", jenisPet = "", provinsi = "", kota = "", gender = "", umurMax = "", umurMin = "")
        }.flow
    }
}

sealed class CatData{
    data class CatItem(val catModel: CatListResponse.Cat) : CatData()
}
