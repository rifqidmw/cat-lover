package dev.pawcat.ui.main.profile

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.FragmentProfileBinding
import dev.pawcat.ui.adoptionlist.AdoptionListActivity
import dev.pawcat.ui.follow.FollowActivity
import dev.pawcat.ui.inputpost.InputPostActivity
import dev.pawcat.ui.main.profile.adapter.PostImageAdapter
import dev.pawcat.ui.userprofile.UserProfileActivity
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ProfileFragment : Fragment(), Observer<ProfileState> {

    private val postAdapter by lazy { PostImageAdapter(requireContext()) }
    private val binding by FragmentBinding<FragmentProfileBinding>(R.layout.fragment_profile)
    private val mViewModel: ProfileViewModel by inject()
    private val session: Session by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.state.observe(viewLifecycleOwner, this)

        setupViews()
        setProfile()
//        loadDataPost(false)
    }

    private fun setupViews() {
        binding.apply {
            rvPost.adapter = postAdapter
            rvPost.adapter = postAdapter.withLoadStateFooter(
                footer = FooterLoadingAdapter { postAdapter.retry() }
            )

            btnAdoption.setOnClickListener {
                if (tvCats.text == "0") {
                    requireContext().toast("No cats available for adoption")
                } else {
                    val intent = Intent(requireContext(), AdoptionListActivity::class.java)
                    intent.putExtra(USER_UUID, session[USER_UUID])
                    requireContext().startActivity(intent)
                }
            }

            btnEditProfile.setOnClickListener {
                val intent = Intent(requireContext(), UserProfileActivity::class.java)
                requireContext().startActivity(intent)
            }

            btnFollower.setOnClickListener {
                val intent = Intent(requireContext(), FollowActivity::class.java)
                intent.putExtra(FOLLOW_TYPE, FOLLOW_FOLLOWER)
                requireContext().startActivity(intent)
            }

            btnFollowing.setOnClickListener {
                val intent = Intent(requireContext(), FollowActivity::class.java)
                intent.putExtra(FOLLOW_TYPE, FOLLOW_FOLLOWING)
                requireContext().startActivity(intent)
            }

            btnAddPost.setOnClickListener {
                val intent = Intent(requireContext(), InputPostActivity::class.java)
                requireContext().startActivity(intent)
            }
        }
    }

    private fun loadDataProfile() {
        mViewModel.doGetProfile(requireContext().getParamHeader())
    }


    private fun loadDataPost(withLoading: Boolean) {
        binding.apply {
            lifecycleScope.launch {
                mViewModel.posts(requireContext().getUserUUID(), "").collectLatest {
                    postAdapter.submitData(it)

                    if (postAdapter.itemCount > 0){
                        rvPost.visibility = View.VISIBLE
                        lyNoItem.visibility = View.GONE
                    } else {
                        rvPost.visibility = View.GONE
                        lyNoItem.visibility = View.VISIBLE
                    }
                }
            }

            postAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading) {
                    if (withLoading) {
                        progressBar.visibility = View.VISIBLE
                    }


                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        val errorMsg = it?.error?.message ?: ""
                        if (errorMsg.isNotEmpty()){
                            showSnackbar(it?.error?.message ?: "")
                        } else {
                            if (postAdapter.itemCount > 0){
                                rvPost.visibility = View.VISIBLE
                                lyNoItem.visibility = View.GONE
                            } else {
                                rvPost.visibility = View.GONE
                                lyNoItem.visibility = View.VISIBLE
                            }
                        }
                    }
                }

            }
        }
    }

    private fun showSnackbar(msg: String) {
        if (msg.isNotEmpty()) {
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                postAdapter.retry()
            })
            snack.show()
        }
    }

    private fun setProfile() {
        binding.apply {
            imgProfile.setPhotoCircularUrl(requireContext(), session[USER_AVATAR] ?: "")

            tvName.text = session[USERNAME]
            tvEmail.text = session[USER_EMAIL]
            tvPost.text = session[USER_POST]
            tvFollowers.text = session[USER_FOLLOWER]
            tvFollowing.text = session[USER_FOLLOWING]
            tvCats.text = session[USER_CAT_AVAILABLE]

        }
    }

    override fun onResume() {
        super.onResume()
        loadDataProfile()
        loadDataPost(false)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ProfileFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onChanged(state: ProfileState?) {
        when (state) {
            is ProfileState.OnSuccessGetProfile -> {
                val status = state.getProfileResponse.code ?: ""
                val msg = state.getProfileResponse.message ?: ""

                if (status == "200") {
                    val data = state.getProfileResponse.data

                    val uuid = data?.uuid ?: ""
                    val name = data?.name ?: ""
                    val email = data?.email ?: ""
                    val noTelp = data?.noTelp ?: ""
                    val post = data?.post ?: ""
                    val cat = data?.cat ?: ""
                    val catAvailable = data?.catAvailable ?:""
                    val reminder = data?.reminder ?: ""
                    val followers = data?.followers ?: ""
                    val following = data?.following ?: ""
                    val avatar = data?.avatar ?: ""

                    session.save(USER_UUID, uuid)
                    session.save(USERNAME, name)
                    session.save(USER_EMAIL, email)
                    session.save(USER_PHONE, noTelp)
                    session.save(USER_POST, post)
                    session.save(USER_CAT, cat)
                    session.save(USER_CAT_AVAILABLE, catAvailable)
                    session.save(USER_REMINDER, reminder)
                    session.save(USER_FOLLOWER, followers)
                    session.save(USER_FOLLOWING, following)
                    session.save(USER_AVATAR, avatar)

                    setProfile()
                }
            }

            is ProfileState.OnErrorState -> {

            }
        }
    }
}