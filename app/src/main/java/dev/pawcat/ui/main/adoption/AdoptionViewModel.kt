package dev.pawcat.ui.main.adoption

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.CatListPagingSource
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.getUserUUID
import dev.pawcat.utils.logD
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class AdoptionViewModel(
    private val pagedListRepo: PagedListRepo,
    private val authRepo: AuthRepo,
    private val context: Context) : ViewModel() {

    private val disposable = CompositeDisposable()

    fun cats(available: String, name: String, jenis: String, provinsi: String, kota: String, gender: String, umurMax: String, umurMin: String): Flow<PagingData<CatData.CatItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            CatListPagingSource(headers = headers, pagedListRepo = pagedListRepo, userUUID = "",
                name = name, available = available, jenisPet = jenis, provinsi = provinsi,
                kota = kota, gender = gender, umurMax = umurMax, umurMin = umurMin)
        }.flow.map { pagingData -> pagingData.map { CatData.CatItem(it) } }
    }

    fun doGetProvinsi(data: ((data: ProvinsiListResponse) -> Unit?)? = null, throwable: ((throwable: Throwable) -> Unit?)? = null) {
        val headers = context.getParamHeader()
        launch {
            authRepo.getProvinsi(headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    data?.invoke(it)
                }) {
                    throwable?.invoke(it)
                }
        }
    }

    fun doGetKota(id: String, data: ((data: KotaListResponse) -> Unit?)? = null, throwable: ((throwable: Throwable) -> Unit?)? = null) {
        val headers = context.getParamHeader()
        launch {
            authRepo.getKota(headers, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    data?.invoke(it)
                }) {
                    throwable?.invoke(it)
                }
        }
    }

    fun doGetTypePet(data: ((data: PetTypeResponse) -> Unit?)? = null, throwable: ((throwable: Throwable) -> Unit?)? = null) {
        val headers = context.getParamHeader()
        launch {
            authRepo.getPetType(headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    data?.invoke(it)
                }) {
                    throwable?.invoke(it)
                }
        }
    }

    fun launch(job: () -> Disposable) {
        disposable.add(job())

    }
}


sealed class CatData{
    data class CatItem(val catModel: CatListResponse.Cat) : CatData()
}
