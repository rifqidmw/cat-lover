package dev.pawcat.ui.main.profile

import dev.pawcat.data.remote.response.GetProfileResponse

sealed class ProfileState {
    data class OnSuccessGetProfile(val getProfileResponse: GetProfileResponse) : ProfileState()
    data class OnErrorState(val t: Throwable): ProfileState()
}