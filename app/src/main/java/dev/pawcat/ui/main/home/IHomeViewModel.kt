package dev.pawcat.ui.main.home

import dev.pawcat.data.remote.response.base.BaseResponse

interface IHomeViewModel {
    fun successLike(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
}