package dev.pawcat.ui.main.adoption.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse


class KotaAdapter(private val mContext: Context)
    : RecyclerView.Adapter<KotaAdapter.MyViewHolder>() {

    private val TAG = KotaAdapter::class.java.simpleName
    private lateinit var itemView: View

    private var dataList: MutableList<KotaListResponse.Data> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): KotaAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_location, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: KotaAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        holder.tvName.text = data.name

        holder.itemView.setOnClickListener {
            if (mListener != null) mListener!!.onItemClick(
                data
            )
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView = itemView.findViewById(R.id.tv_name)
    }

    fun updateData(data: List<KotaListResponse.Data>){
        this.dataList.clear()
        this.dataList.addAll(data)


        notifyDataSetChanged()
    }

    fun setFilter(newList: MutableList<KotaListResponse.Data>) {
        dataList = java.util.ArrayList<KotaListResponse.Data>()
        dataList.addAll(newList)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(data: KotaListResponse.Data)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        mListener = listener
    }

    companion object {
        private var mListener: OnItemClickListener? = null
    }
}