package dev.pawcat.ui.main.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.databinding.FragmentHomeBinding
import dev.pawcat.ui.inputpost.InputPostActivity
import dev.pawcat.ui.main.home.adapter.PostAdapter
import dev.pawcat.ui.searchlist.SearchListActivity
import dev.pawcat.utils.view.FooterLoadingAdapter
import dev.pawcat.utils.FragmentBinding
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logE
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class HomeFragment : Fragment(), Observer<HomeState>, PostAdapter.LikeClickListener {
    private val TAG = HomeFragment::class.java.simpleName
    private val binding by FragmentBinding<FragmentHomeBinding>(R.layout.fragment_home)
    private val mViewModel: HomeViewModel by inject()
    private val session: Session by inject()

    private val postAdapter by lazy { PostAdapter(requireContext(), session) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.state.observe(viewLifecycleOwner, this)

        setupViews()
        loadDataPost("0",true)
    }

    private fun setupViews(){
        binding.apply {

            tabLayout.addTab(tabLayout.newTab().setText("Recent"))
            tabLayout.addTab(tabLayout.newTab().setText("Following"))

            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    when(tabLayout.selectedTabPosition){
                        0 -> {
                            loadDataPost("0", false)
                        }

                        1 -> {
                            loadDataPost("1", false)
                        }
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })

            rvPost.apply {
                adapter = postAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                adapter = postAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ postAdapter.retry() }
                )
            }

            postAdapter.listener = this@HomeFragment

            btnAddPost.setOnClickListener {
                val intent = Intent(requireContext(), InputPostActivity::class.java)
                startActivity(intent)
            }

            btnSearch.setOnClickListener {
                val intent = Intent(requireContext(), SearchListActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun loadDataPost(type: String,withLoading: Boolean){
        binding.apply {
            lifecycleScope.launch {
                mViewModel.posts("", type).collectLatest {
                    postAdapter.submitData(it)
                }
            }

            postAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if(withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        showSnackbar(it?.error?.message ?: "")
                    }
                }
            }
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                postAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onItemLikeClicked(data: PostListResponse.Post) {
        mViewModel.doLike(requireContext().getParamHeader(), data.uuid?:"")
    }

    override fun onChanged(state: HomeState?) {
        when(state){
            is HomeState.OnSuccessLike -> {

            }

            is HomeState.OnErrorState -> {
                val msg = state.t.localizedMessage
                logE(TAG, msg)
            }
        }
    }
}