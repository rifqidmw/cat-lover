package dev.pawcat.ui.main.article

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.ArticleListResponse
import dev.pawcat.data.remote.response.CommentArticleListResponse
import dev.pawcat.data.remote.response.CommentPostListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.ArticleRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.ArticleListPagingSource
import dev.pawcat.data.repo.paged.CommentArticleListPagingSource
import dev.pawcat.data.repo.paged.CommentPostListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ArticleViewModel(
    private val articleRepo: ArticleRepo,
    private val pagedListRepo: PagedListRepo, private val context: Context
) : RxViewModel<ArticleState>(), IArticleViewModel{
    override val TAG: String
        get() = ArticleState::class.java.simpleName

    override fun successLike(baseResponse: BaseResponse) {
        logD(TAG,"success do like")
        state.value = ArticleState.OnSuccessLike(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = ArticleState.OnErrorState(t)
    }

    fun articles(uuid: String): Flow<PagingData<ArticleData.ArticleItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            ArticleListPagingSource(headers = headers, name = "", content = "", pagedListRepo = pagedListRepo)
        }.flow.map { pagingData ->
            pagingData.map {
                ArticleData.ArticleItem(it)
            }
        }
    }

    fun doLike(header:Map<String, String>, uuid: String) {
        logD(TAG,"do follow")
        launch {
            articleRepo.doLike(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLike) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class ArticleData{
    data class ArticleItem(val articleModel: ArticleListResponse.Article) : ArticleData()
}
