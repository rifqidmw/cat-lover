package dev.pawcat.ui.main.adoption

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.get
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.local.model.GenderModel
import dev.pawcat.data.local.model.TypePetModel
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.databinding.BottomsheetFilterBinding
import dev.pawcat.databinding.FragmentAdoptionBinding
import dev.pawcat.ui.main.adoption.adapter.*
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class AdoptionFragment : Fragment() {

    private val binding by FragmentBinding<FragmentAdoptionBinding>(R.layout.fragment_adoption)
    private val adoptionAdapter: AdoptionAdapter by lazy { AdoptionAdapter(requireContext()) }
    private val mViewModel: AdoptionViewModel by inject()
    private val session: Session by inject()

    //Filter data
    private var searchQuery: String = ""
    private var idProvince: String = ""
    private var titleProvince: String = "All"
    private var idCity: String = ""
    private var titleCity: String = "All"
    private var idGender: String = ""
    private var minAge: String = ""
    private var maxAge: String = ""
    private var idType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        loadDataCat(name = "", withLoading = true)
    }

    private fun setupViews(){
        binding.apply {
            /*val manager = SpannedGridLayoutManager(
                object : SpannedGridLayoutManager.GridSpanLookup {
                    override fun getSpanInfo(position: Int): SpannedGridLayoutManager.SpanInfo {
                        // Conditions for 2x2 items
                        return if (position % 12 == 0 || position % 12 == 7) {
                            SpannedGridLayoutManager.SpanInfo(2, 2)
                        } else {
                            SpannedGridLayoutManager.SpanInfo(1, 1)
                        }
                    }
                },
                3,
                1f
            )*/
            appBarLayout.outlineProvider = null
            rvAdoption.apply {
                setHasFixedSize(true)
                adapter = adoptionAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ adoptionAdapter.retry() }
                )
            }

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    searchQuery = query?:""
                    loadDataCat(searchQuery, true)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText.isNullOrEmpty()){
                        searchQuery = ""
                        loadDataCat(searchQuery, false)
                    }

                    return false
                }

            })

            btnFilter.setOnClickListener {
                showBottomsheetOption()
            }

            executePendingBindings()
        }
    }

    private fun loadDataCat(name: String,withLoading: Boolean){
        binding.apply {
            lifecycleScope.launch {
                mViewModel.cats(name = name, available = "1", jenis = idType, provinsi = idProvince,
                    kota = idCity, gender = idGender, umurMin = minAge, umurMax = maxAge).collectLatest {
                    adoptionAdapter.submitData(it)
                }
            }

            adoptionAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if(withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        showSnackbar(it?.error?.message ?: "")
                    }
                }
            }
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                adoptionAdapter.retry()
            })
            snack.show()
        }
    }

    private fun showBottomsheetOption(){

        val bottomSheet = layoutInflater.inflate(R.layout.bottomsheet_filter, null)
        val bseet = BottomsheetFilterBinding.bind(bottomSheet)
        val dialog = BottomSheetDialog(this.requireContext(), R.style.ThemeOverlay_Demo_BottomSheetDialog)
        dialog.setContentView(bottomSheet)

        bseet.apply {
            //Gender
            val genderList: ArrayList<GenderModel> = ArrayList()
            val genderAdapter = RadioGenderAdapter(requireContext())
            rvGender.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                adapter = genderAdapter
            }

            genderList.add(GenderModel("", "All"))
            genderList.add(GenderModel("1", "Male"))
            genderList.add(GenderModel("2", "Female"))
            genderAdapter.setData(genderList)

            genderAdapter.setOnItemClickListener(object : RadioGenderAdapter.OnItemClickListener{
                override fun onItemClick(data: GenderModel) {
                    idGender = data.id
                }
            })

            //Type Pet
            val typePetList: ArrayList<TypePetModel> = ArrayList()
            val typePetAdapter = RadioTypePetAdapter(requireContext())
            rvType.apply {
                setHasFixedSize(true)
                layoutManager = GridLayoutManager(requireContext(), 3)
                adapter = typePetAdapter
            }

            typePetList.add(TypePetModel("", "All"))
            mViewModel.doGetTypePet({
                val data = it.data!!
                for (i in data.indices){
                    typePetList.add(TypePetModel(data[i].id.toString(), data[i].title!!))
                }

                typePetAdapter.setData(typePetList)
                typePetAdapter.updateData(idType)
                print("success get type pet")
            }) {
                typePetAdapter.setData(typePetList)
            }

            typePetAdapter.setOnItemClickListener(object : RadioTypePetAdapter.OnItemClickListener{
                override fun onItemClick(data: TypePetModel) {
                    idType = data.id
                }
            })

            btnProvince.text = titleProvince
            btnCity.text = titleCity
            etAgeMin.setText(minAge)
            etAgeMax.setText(maxAge)
            genderAdapter.updateData(idGender)

            btnProvince.setOnClickListener {
                showProvinsiDialog { data ->
                    btnProvince.text = data.name
                    idProvince = data.idProvinsi!!
                    titleProvince = data.name!!

                    print("success get province")
                }
            }

            btnCity.setOnClickListener {
                if (idProvince.isEmpty()){
                    requireContext().toast("Pleace choose province first before city")
                } else {
                    showKotaDialog(idProvince) { data ->
                        btnCity.text = data.name
                        idCity = data.idKota!!
                        titleCity = data.name!!

                        print("success get city")
                    }
                }
            }

            btnReset.setOnClickListener {
                idProvince = ""
                titleProvince = "All"
                idCity = ""
                titleCity = "All"
                idGender = ""
                idType = ""
                minAge = ""
                maxAge = ""

                btnProvince.text = titleProvince
                btnCity.text = titleCity
                etAgeMin.setText("")
                etAgeMax.setText("")
                genderAdapter.updateData(idGender)
                typePetAdapter.updateData(idType)
            }

            btnApply.setOnClickListener {
                minAge = etAgeMin.text.toString()
                maxAge = etAgeMax.text.toString()

                loadDataCat(searchQuery, true)
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun showProvinsiDialog(item: ((data: ProvinsiListResponse.Data) -> Unit?)? = null){
        val customView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_location, null, false)
        val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialog.show()
        val tvTitle = customView.findViewById<TextView>(R.id.dialog_title)
        val btnClose = customView.findViewById<ImageView>(R.id.btn_close)
        val progressBar = customView.findViewById<ProgressBar>(R.id.progress_bar)
        val lyFailed = customView.findViewById<LinearLayout>(R.id.ly_failed)
        val btnTryagain = customView.findViewById<Button>(R.id.btn_try_again)
        val rvLocation = customView.findViewById<RecyclerView>(R.id.rv_location)
        val searchView = customView.findViewById<SearchView>(R.id.search_view)
        tvTitle.text = "Province"

        val adapterProvinsi = ProvinsiAdapter(requireContext())

        rvLocation.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = adapterProvinsi
        }

        mViewModel.doGetProvinsi({
            progressBar.visibility = View.GONE
            lyFailed.visibility = View.GONE
            val provinsi = it.data
            adapterProvinsi.updateData(provinsi!!)

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    var textSearch = newText.toString()
                    textSearch = textSearch.toLowerCase()
                    var newList: MutableList<ProvinsiListResponse.Data> = ArrayList()
                    if (textSearch.isEmpty()) {
                        newList = provinsi.toMutableList()
                    } else {
                        for (model in provinsi) {
                            val title: String = model.name!!.toLowerCase()
                            if (title.contains(textSearch)) {
                                newList.add(model)
                            }
                        }
                    }
                    adapterProvinsi.setFilter(newList)

                    return false
                }

            })
        }) {
            lyFailed.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            print(it.message)
        }

        adapterProvinsi.setOnItemClickListener(object : ProvinsiAdapter.OnItemClickListener{
            override fun onItemClick(data: ProvinsiListResponse.Data) {
                item?.invoke(data)
                dialog.dismiss()
            }

        })

        btnClose.setOnClickListener {
            dialog.dismiss()
        }

        btnTryagain.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            lyFailed.visibility = View.GONE
            mViewModel.doGetProvinsi({
                progressBar.visibility = View.GONE
                lyFailed.visibility = View.GONE
                val provinsi = it.data
                adapterProvinsi.updateData(provinsi!!)
            }) {
                lyFailed.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                print(it.message)
            }
        }
    }

    private fun showKotaDialog(id: String, item: ((data: KotaListResponse.Data) -> Unit?)? = null){
        val customView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_location, null, false)
        val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialog.show()
        val tvTitle = customView.findViewById<TextView>(R.id.dialog_title)
        val btnClose = customView.findViewById<ImageView>(R.id.btn_close)
        val progressBar = customView.findViewById<ProgressBar>(R.id.progress_bar)
        val lyFailed = customView.findViewById<LinearLayout>(R.id.ly_failed)
        val btnTryagain = customView.findViewById<Button>(R.id.btn_try_again)
        val rvLocation = customView.findViewById<RecyclerView>(R.id.rv_location)
        val searchView = customView.findViewById<SearchView>(R.id.search_view)
        tvTitle.text = "City"

        val adapterKota = KotaAdapter(requireContext())

        rvLocation.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = adapterKota
        }

        mViewModel.doGetKota(id, {
            lyFailed.visibility = View.GONE
            progressBar.visibility = View.GONE
            val data = it.data
            adapterKota.updateData(data!!)

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    var textSearch = newText.toString()
                    textSearch = textSearch.toLowerCase()
                    var newList: MutableList<KotaListResponse.Data> = ArrayList()
                    if (textSearch.isEmpty()) {
                        newList = data.toMutableList()
                    } else {
                        for (model in data) {
                            val title: String = model.name!!.toLowerCase()
                            if (title.contains(textSearch)) {
                                newList.add(model)
                            }
                        }
                    }
                    adapterKota.setFilter(newList)

                    return false
                }

            })
        }) {
            lyFailed.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            print(it.message)
        }

        adapterKota.setOnItemClickListener(object : KotaAdapter.OnItemClickListener{
            override fun onItemClick(data: KotaListResponse.Data) {
                item?.invoke(data)
                dialog.dismiss()
            }

        })

        btnTryagain.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            lyFailed.visibility = View.GONE
            mViewModel.doGetKota(id, {
                progressBar.visibility = View.GONE
                lyFailed.visibility = View.GONE
                val data = it.data
                adapterKota.updateData(data!!)
            }) {
                lyFailed.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                print(it.message)
            }
        }

        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AdoptionFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}