package dev.pawcat.ui.main.mypet.pet

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.databinding.FragmentPetListBinding
import dev.pawcat.ui.inputpet.InputCatActivity
import dev.pawcat.ui.main.mypet.pet.adapter.PetAdapter
import dev.pawcat.utils.FragmentBinding
import dev.pawcat.utils.INPUT_NEW
import dev.pawcat.utils.INPUT_TYPE
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class PetListFragment : Fragment() {

    private val binding by FragmentBinding<FragmentPetListBinding>(R.layout.fragment_pet_list)
    private val catAdapter by lazy { PetAdapter(requireContext()) }
    private val viewModel: CatListViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews(){
        binding.apply {
            rvCat.apply {
                setHasFixedSize(true)
                adapter = catAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ catAdapter.retry() }
                )
            }

            btnAddCat.setOnClickListener {
                val intent = Intent(requireContext(), InputCatActivity::class.java)
                intent.putExtra(INPUT_TYPE, INPUT_NEW)
                requireContext().startActivity(intent)
            }

        }
    }

    private fun loadDataCat(withLoading: Boolean){
        binding.apply {
            lifecycleScope.launch {
                viewModel.cats.collectLatest {
                    catAdapter.submitData(it)
                }
            }

            catAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if(withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        val errorMsg = it?.error?.message ?: ""
                        if (errorMsg.isNotEmpty()){
                            showSnackbar(it?.error?.message ?: "")
                        } else {
                            if (catAdapter.itemCount > 0){
                                rvCat.visibility = View.VISIBLE
                                lyNoItem.visibility = View.GONE
                            } else {
                                rvCat.visibility = View.GONE
                                lyNoItem.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(requireView(), msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                catAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onResume() {
        super.onResume()
        loadDataCat(false)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            PetListFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}