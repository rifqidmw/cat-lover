package dev.pawcat.ui.main.home

import dev.pawcat.data.remote.response.base.BaseResponse

sealed class HomeState {
    data class OnSuccessLike(val baseResponse: BaseResponse) : HomeState()
    data class OnErrorState(val t: Throwable) : HomeState()
}