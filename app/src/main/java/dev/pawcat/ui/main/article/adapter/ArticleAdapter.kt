package dev.pawcat.ui.main.article.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.data.remote.response.ArticleListResponse
import dev.pawcat.databinding.ItemArticleBinding
import dev.pawcat.ui.detailarticle.DetailArticleActivity
import dev.pawcat.ui.main.article.ArticleData
import dev.pawcat.utils.*

class ArticleAdapter(
    private val context: Context
) : PagingDataAdapter<ArticleData.ArticleItem, ArticleAdapter.ArticleViewHolder>(DIIF_UTIL) {

    var listener: LikeClickListener? = null

    open inner class ArticleViewHolder (val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(articleModel: ArticleData.ArticleItem){
            val data = articleModel.articleModel
            var likeStatus = data.statusLike

            binding.apply {
                imgArticle.setImageviewCenterUrl(context, data.image?:"")
                tvTitle.text = data.title
                tvCategory.text = data.kategoriTitle
                tvDate.text = data.createdAt!!.changeDateFormat().setTimeAgo()
                tvLiked.text = data.like
                tvComment.text = data.comment

                when(likeStatus){
                    "0" -> {
                        btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_inactive))
                    }

                    "1" -> {
                        btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_active))
                    }
                }

                btnMore.setOnClickListener {
                    val intent = Intent(context, DetailArticleActivity::class.java)
                    intent.putExtra(DATA_ARTICLE, data)
                    context.startActivity(intent)
                }

                btnLike.setOnClickListener {
                    when(likeStatus){
                        "0" -> {
                            likeStatus = "1"
                            btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_active))
                            tvLiked.text = (tvLiked.text.toString().toInt() + 1).toString()
                        }

                        "1" -> {
                            likeStatus = "0"
                            btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_inactive))
                            tvLiked.text = (tvLiked.text.toString().toInt() - 1).toString()
                        }
                    }
                    listener?.onItemLikeClicked(data)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent.context)

        return ArticleViewHolder(
            ItemArticleBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<ArticleData.ArticleItem>(){
            override fun areItemsTheSame(oldItem: ArticleData.ArticleItem, newItem: ArticleData.ArticleItem): Boolean {
                return oldItem.articleModel.uuid == newItem.articleModel.uuid
            }

            override fun areContentsTheSame(oldItem: ArticleData.ArticleItem, newItem: ArticleData.ArticleItem): Boolean {
                return oldItem == newItem
            }

        }
    }

    interface LikeClickListener {
        fun onItemLikeClicked(data: ArticleListResponse.Article)
    }
}