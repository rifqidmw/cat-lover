package dev.pawcat.ui.main.mypet.reminder

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.databinding.FragmentReminderListBinding
import dev.pawcat.ui.inputreminder.InputReminderActivity
import dev.pawcat.ui.main.mypet.reminder.adapter.ReminderAdapter
import dev.pawcat.utils.FragmentBinding
import dev.pawcat.utils.INPUT_NEW
import dev.pawcat.utils.INPUT_TYPE
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ReminderListFragment : Fragment() {

    private val binding by FragmentBinding<FragmentReminderListBinding>(R.layout.fragment_reminder_list)
    private val viewModel: ReminderListViewModel by inject()
    private val scheduleAdapter by lazy { ReminderAdapter(requireContext()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
//        loadDataReminder(true)
    }

    private fun setupViews(){
        binding.apply {
            rvSchedule.apply {
                setHasFixedSize(true)
                adapter = scheduleAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ scheduleAdapter.retry() }
                )
            }

            btnAddReminder.setOnClickListener {
                val intent = Intent(requireContext(), InputReminderActivity::class.java)
                intent.putExtra(INPUT_TYPE, INPUT_NEW)
                requireContext().startActivity(intent)
            }
        }
    }

    private fun loadDataReminder(withLoading: Boolean){
        binding.apply {
            lifecycleScope.launch {
                viewModel.reminders.collectLatest {
                    scheduleAdapter.submitData(it)

                    if (scheduleAdapter.itemCount > 0){
                        rvSchedule.visibility = View.VISIBLE
                        lyNoItem.visibility = View.GONE
                    } else {
                        rvSchedule.visibility = View.GONE
                        lyNoItem.visibility = View.VISIBLE
                    }
                }
            }

            scheduleAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if (withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        val errorMsg = it?.error?.message ?: ""
                        if (errorMsg.isNotEmpty()){
                            showSnackbar(it?.error?.message ?: "")
                        } else {
                            if (scheduleAdapter.itemCount > 0){
                                rvSchedule.visibility = View.VISIBLE
                                lyNoItem.visibility = View.GONE
                            } else {
                                rvSchedule.visibility = View.GONE
                                lyNoItem.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(requireView(), msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                scheduleAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onResume() {
        super.onResume()
        loadDataReminder(false)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            ReminderListFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}