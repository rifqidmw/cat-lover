package dev.pawcat.ui.main.mypet

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import dev.pawcat.ui.main.mypet.pet.PetListFragment
import dev.pawcat.ui.main.mypet.reminder.ReminderListFragment

class MypetAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle,
    private var numberOfTabs: Int
) : FragmentStateAdapter(fm, lifecycle){
    override fun getItemCount(): Int {
        return numberOfTabs
    }

    override fun createFragment(position: Int): Fragment {
       when(position){
           0 -> {
               val bundle = Bundle()
               bundle.putString("fragmentName", "Cat List Fragment")
               val catListFragment = PetListFragment()
               catListFragment.arguments = bundle
               return catListFragment
           }

           1 -> {
               val bundle = Bundle()
               bundle.putString("fragmentName", "Schedule List Fragment")
               val scheduleListFragment = ReminderListFragment()
               scheduleListFragment.arguments = bundle
               return scheduleListFragment
           }

           else ->{
               return PetListFragment.newInstance()
           }
       }
    }
}