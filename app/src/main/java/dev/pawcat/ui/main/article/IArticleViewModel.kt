package dev.pawcat.ui.main.article

import dev.pawcat.data.remote.response.base.BaseResponse

interface IArticleViewModel {
    fun successLike(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
}