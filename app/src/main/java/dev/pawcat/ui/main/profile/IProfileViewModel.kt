package dev.pawcat.ui.main.profile

import dev.pawcat.data.remote.response.GetProfileResponse

interface IProfileViewModel {
    fun successGetProfile(getProfileResponse: GetProfileResponse)
    fun errorLoad(t: Throwable)
}