package dev.pawcat.ui.main.adoption.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.data.local.model.GenderModel
import dev.pawcat.data.local.model.TypePetModel
import dev.pawcat.data.remote.response.ProvinsiListResponse

class RadioTypePetAdapter(private val mContext: Context)
    : RecyclerView.Adapter<RadioTypePetAdapter.MyViewHolder>() {

    private val TAG = RadioTypePetAdapter::class.java.simpleName
    private lateinit var itemView: View
    var mSelectedItem = -1
    private var dataList: MutableList<TypePetModel> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RadioTypePetAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_type_pet, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RadioTypePetAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        if (position == mSelectedItem){
            holder.radioButton.typeface = ResourcesCompat.getFont(mContext, R.font.roboto)
        } else {
            holder.radioButton.typeface = ResourcesCompat.getFont(mContext, R.font.roboto_bold)
        }

        holder.radioButton.isChecked = position == mSelectedItem

        holder.radioButton.text = data.title

        holder.radioButton.setOnClickListener {
            mSelectedItem = position
            if (mListener != null) mListener!!.onItemClick(
                data
            )

            notifyDataSetChanged()
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var radioButton: RadioButton = itemView.findViewById(R.id.radio_button)
    }

    fun setData(data: List<TypePetModel>){
        this.dataList.clear()
        this.dataList.addAll(data)

        notifyDataSetChanged()
    }

    fun updateData(selected: String){
        for (i in dataList.indices){
            if (dataList[i].id == selected){
                mSelectedItem = i
                mListener!!.onItemClick(dataList[i])
                notifyDataSetChanged()
            }
        }
    }

    fun resetAdapter(){
        mSelectedItem = -1
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(data: TypePetModel)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        mListener = listener
    }

    companion object {
        private var mListener: OnItemClickListener? = null
    }
}