package dev.pawcat.ui.main.mypet.reminder

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.ReminderListResponse
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.ReminderListPagingSource
import dev.pawcat.utils.getParamHeader
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.io.Serializable

class ReminderListViewModel(private val pagedListRepo: PagedListRepo, private val context: Context) : ViewModel() {
    val reminders: Flow<PagingData<ReminderData.ReminderItem>> = getReminderListStream()
        .map { pagingData -> pagingData.map { ReminderData.ReminderItem(it) } }

    private fun getReminderListStream(): Flow<PagingData<ReminderListResponse.Reminder>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            ReminderListPagingSource(headers = headers, pagedListRepo = pagedListRepo)
        }.flow
    }
}

sealed class ReminderData : Serializable{
    data class ReminderItem(val reminderModel: ReminderListResponse.Reminder) : ReminderData()
}
