package dev.pawcat.ui.main.home

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.PostRepo
import dev.pawcat.data.repo.paged.PostListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class HomeViewModel(
    private val postRepo: PostRepo,
    private val pagedListRepo: PagedListRepo,
    private val context: Context
) : RxViewModel<HomeState>(), IHomeViewModel {
    override val TAG: String
        get() = HomeState::class.java.simpleName

    override fun successLike(baseResponse: BaseResponse) {
        logD(TAG,"success do like")
        state.value = HomeState.OnSuccessLike(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = HomeState.OnErrorState(t)
    }

    fun posts(uuid: String, all:String): Flow<PagingData<PostData.PostItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            PostListPagingSource(headers = headers, uuid = uuid, all = all, pagedListRepo = pagedListRepo)
        }.flow.map { pagingData ->
            pagingData.map {
                PostData.PostItem(it)
            }
        }
    }

    fun doLike(header:Map<String, String>, uuid: String) {
        logD(TAG,"do like")
        launch {
            postRepo.doLike(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLike) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class PostData{
    data class PostItem(val postModel: PostListResponse.Post) : PostData()
}