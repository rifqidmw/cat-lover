package dev.pawcat.ui.main.profile

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.GetProfileResponse
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.PostListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ProfileViewModel(val authRepo: AuthRepo,
                       private val pagedListRepo: PagedListRepo,
                       private val context: Context
) : RxViewModel<ProfileState>(), IProfileViewModel {
    override val TAG: String
        get() = ProfileState::class.java.simpleName

    override fun successGetProfile(getProfileResponse: GetProfileResponse) {
        logD(TAG,"success get profile")
        state.value = ProfileState.OnSuccessGetProfile(getProfileResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = ProfileState.OnErrorState(t)
    }

    fun posts(uuid: String, all:String): Flow<PagingData<dev.pawcat.ui.main.home.PostData.PostItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            PostListPagingSource(headers = headers, uuid = uuid, all = all, pagedListRepo = pagedListRepo)
        }.flow.map { pagingData ->
            pagingData.map {
                dev.pawcat.ui.main.home.PostData.PostItem(it)
            }
        }
    }

    fun doGetProfile(header:Map<String, String>) {
        logD(TAG,"do get profile")
        launch {
            authRepo.getProfile(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetProfile) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class PostData{
    data class PostItem(val postModel: PostListResponse.Post) : PostData()
}