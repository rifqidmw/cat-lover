package dev.pawcat.ui.main.mypet

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayoutMediator
import dev.pawcat.R
import dev.pawcat.databinding.FragmentMycatBinding
import dev.pawcat.ui.inputpet.InputCatActivity
import dev.pawcat.ui.inputreminder.InputReminderActivity
import dev.pawcat.utils.FragmentBinding
import dev.pawcat.utils.INPUT_NEW
import dev.pawcat.utils.INPUT_TYPE

class MypetFragment : Fragment() {

    private val binding by FragmentBinding<FragmentMycatBinding>(R.layout.fragment_mycat)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            val adapter = MypetAdapter(requireActivity().supportFragmentManager, lifecycle, 2)
            viewPager.adapter = adapter
            viewPager.isUserInputEnabled = true
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                when(position){
                    0 -> {
                        tab.text = "Pets"
                    }
                    1 -> {
                        tab.text = "Reminder"
                    }
                }
            }.attach()

            btnAdd.setOnClickListener {
                showBottomsheetOption()
            }
        }
    }

    private fun showBottomsheetOption(){
        val bottomSheet = layoutInflater.inflate(R.layout.bottomsheet_mypet, null)
        val btnAddPet = bottomSheet.findViewById<LinearLayout>(R.id.btn_add_pet)
        val btnAddSchedule = bottomSheet.findViewById<LinearLayout>(R.id.btn_add_schedule)
        val dialog = BottomSheetDialog(this.requireContext(), R.style.ThemeOverlay_Demo_BottomSheetDialog)
        dialog.setContentView(bottomSheet)

        btnAddPet.setOnClickListener {
            val intent = Intent(requireContext(), InputCatActivity::class.java)
            intent.putExtra(INPUT_TYPE, INPUT_NEW)
            requireContext().startActivity(intent)
            dialog.dismiss()
        }

        btnAddSchedule.setOnClickListener {
            val intent = Intent(requireContext(), InputReminderActivity::class.java)
            intent.putExtra(INPUT_TYPE, INPUT_NEW)
            requireContext().startActivity(intent)
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            MypetFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}