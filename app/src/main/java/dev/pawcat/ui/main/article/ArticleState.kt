package dev.pawcat.ui.main.article

import dev.pawcat.data.remote.response.base.BaseResponse

sealed class ArticleState {
    data class OnSuccessLike(val baseResponse: BaseResponse) : ArticleState()
    data class OnErrorState(val t: Throwable) : ArticleState()
}