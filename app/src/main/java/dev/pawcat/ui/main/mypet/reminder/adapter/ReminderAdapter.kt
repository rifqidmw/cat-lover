package dev.pawcat.ui.main.mypet.reminder.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.databinding.ItemScheduleBinding
import dev.pawcat.ui.detailreminder.DetailReminderActivity
import dev.pawcat.ui.main.mypet.reminder.ReminderData
import dev.pawcat.utils.DATA_REMINDER
import dev.pawcat.utils.setImageviewLocal

class ReminderAdapter(
    private val context: Context
) : PagingDataAdapter<ReminderData.ReminderItem, ReminderAdapter.ScheduleViewHolder>(DIIF_UTIL) {
    open inner class ScheduleViewHolder(val binding: ItemScheduleBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(scheduleModel: ReminderData.ReminderItem){
            binding.apply {
                val data = scheduleModel.reminderModel
                tvName.text = data.catsName
                tvType.text = data.schedule
                tvDate.text = "${data.date} ${data.time}"

                when(data.schedule){
                    "Food" -> {
                        imgType.setImageviewLocal(context, R.drawable.ic_cat_food)
                    }

                    "Medicine" -> {
                        imgType.setImageviewLocal(context, R.drawable.ic_cat_medication)
                    }

                    "Vaccine" -> {
                        imgType.setImageviewLocal(context, R.drawable.ic_cat_vaccine)
                    }

                    else -> {
                        imgType.setImageviewLocal(context, R.drawable.ic_cat_food)
                    }
                }


                root.setOnClickListener {
                    val intent = Intent(context, DetailReminderActivity::class.java)
                    intent.putExtra(DATA_REMINDER, data)
                    context.startActivity(intent)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        val view = LayoutInflater.from(parent.context)

        return ScheduleViewHolder(
            ItemScheduleBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<ReminderData.ReminderItem>(){
            override fun areItemsTheSame(oldItem: ReminderData.ReminderItem, newItem: ReminderData.ReminderItem): Boolean {
                return oldItem.reminderModel.uuid == newItem.reminderModel.uuid
            }

            override fun areContentsTheSame(oldItem: ReminderData.ReminderItem, newItem: ReminderData.ReminderItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}