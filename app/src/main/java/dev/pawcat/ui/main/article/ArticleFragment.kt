package dev.pawcat.ui.main.article

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderView
import dev.pawcat.R
import dev.pawcat.data.remote.response.ArticleListResponse
import dev.pawcat.databinding.FragmentArticleBinding
import dev.pawcat.ui.main.article.adapter.ArticleAdapter
import dev.pawcat.utils.FragmentBinding
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logE
import dev.pawcat.utils.view.FooterLoadingAdapter
import dev.pawcat.utils.view.imageslider.ImageSliderAdapter
import dev.pawcat.utils.view.imageslider.ImageSliderModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ArticleFragment : Fragment(), Observer<ArticleState>, ArticleAdapter.LikeClickListener {

    private val TAG = ArticleFragment::class.java.simpleName

    private val binding by FragmentBinding<FragmentArticleBinding>(R.layout.fragment_article)
    private val mViewModel: ArticleViewModel by inject()
    private val articleAdapter: ArticleAdapter by lazy { ArticleAdapter(requireContext()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.state.observe(viewLifecycleOwner, this)

        setupViews()
        setupImageSlider()
    }

    private fun setupViews(){
        binding.apply {
            rvArticle.apply {
                adapter = articleAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                adapter = articleAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ articleAdapter.retry() }
                )

                articleAdapter.listener = this@ArticleFragment
            }
        }
    }

    private fun setupImageSlider(){
        val listImageSlider: ArrayList<ImageSliderModel> = ArrayList()
        listImageSlider.add(ImageSliderModel("1", "https://i.ibb.co/sH9bdfs/cat-food-ad-instagram-image-design-template-53d2309706cebf9366b1b2c469cceb01-screen-1.png"))
        listImageSlider.add(ImageSliderModel("2", "https://d3nuqriibqh3vw.cloudfront.net/nissan-x-trail-4dogs-improbable-moments-02.png?pOCcGs47.cRYrucJI9S9YnawYe1PWGja"))
        listImageSlider.add(ImageSliderModel("3", "http://www.ramanmedianetwork.com/wp-content/uploads/2014/02/pet.jpg"))
        listImageSlider.add(ImageSliderModel("4", "https://www.treehugger.com/thmb/LCErg_-1H_4f1GM6BImlARNK5DY=/644x364/filters:fill(auto,1)/__opt__aboutcom__coeus__resources__content_migration__mnn__images__2014__12__eddie-main-806df8c87f9b475ca21b9b567e12379a.jpg"))

        val imageSliderAdapter = ImageSliderAdapter(requireContext(), listImageSlider)

        binding.apply {
            imageSlider.setSliderAdapter(imageSliderAdapter)
            imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM)
            imageSlider.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_RIGHT
            imageSlider.indicatorSelectedColor = Color.WHITE
            imageSlider.indicatorUnselectedColor = Color.GRAY
            imageSlider.scrollTimeInSec = 2
            imageSlider.startAutoCycle()
        }
    }

    private fun loadDataArticle(withLoading: Boolean){
        binding.apply {
            lifecycleScope.launch {
                mViewModel.articles("").collectLatest {
                    articleAdapter.submitData(it)
                }
            }

            articleAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if(withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        showSnackbar(it?.error?.message ?: "")
                    }
                }
            }
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                articleAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onResume() {
        super.onResume()
        loadDataArticle(false)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            ArticleFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onItemLikeClicked(data: ArticleListResponse.Article) {
        mViewModel.doLike(requireContext().getParamHeader(), data.uuid?:"")
    }

    override fun onChanged(state: ArticleState?) {
        when(state){
            is ArticleState.OnSuccessLike -> {

            }

            is ArticleState.OnErrorState -> {
                val msg = state.t.localizedMessage
                logE(TAG, msg)
            }
        }
    }
}