package dev.pawcat.ui.detailreminder

import dev.pawcat.data.remote.response.base.BaseResponse

sealed class DetailReminderState {
    data class OnSuccessDeleteData(val baseResponse: BaseResponse) : DetailReminderState()
    data class OnErrorState(val t: Throwable) : DetailReminderState()
    data class OnLoading(val isLoading: Boolean) : DetailReminderState()
}