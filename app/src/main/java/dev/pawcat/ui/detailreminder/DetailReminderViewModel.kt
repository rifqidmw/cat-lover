package dev.pawcat.ui.detailreminder

import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.ReminderRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailReminderViewModel(
    private val reminderRepo: ReminderRepo
) : RxViewModel<DetailReminderState>(), IDetailReminderViewModel {
    override val TAG: String
        get() = DetailReminderState::class.java.simpleName

    override fun successDeleteData(baseResponse: BaseResponse) {
        logD(TAG,"success do delete data reminder")
        onLoading(false)
        state.value = DetailReminderState.OnSuccessDeleteData(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = DetailReminderState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = DetailReminderState.OnLoading(isLoading)
    }

    fun doDeleteData(header:Map<String, String>, uuid: String) {
        logD(TAG,"do register")
        launch {
            onLoading(true)
            reminderRepo.doDeleteReminder(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successDeleteData) {
                    errorLoad(t = it)
                }
        }
    }
}