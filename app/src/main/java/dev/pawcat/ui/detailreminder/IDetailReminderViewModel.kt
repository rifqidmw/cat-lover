package dev.pawcat.ui.detailreminder

import dev.pawcat.data.remote.response.base.BaseResponse

interface IDetailReminderViewModel {
    fun successDeleteData(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}