package dev.pawcat.ui.detailreminder

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.ReminderListResponse
import dev.pawcat.databinding.ActivityDetailReminderBinding
import dev.pawcat.ui.inputreminder.InputReminderActivity
import dev.pawcat.utils.*
import org.koin.android.ext.android.inject
import java.util.HashMap

class DetailReminderActivity : AppCompatActivity(), Observer<DetailReminderState> {

    private val binding: ActivityDetailReminderBinding by lazy {
        DataBindingUtil.setContentView<ActivityDetailReminderBinding>(
            this, R.layout.activity_detail_reminder
        )
    }
    private val mViewModel: DetailReminderViewModel by inject()
    private val session: Session by inject()

    class FinishReminderDetailActivity{
        companion object{
            var activity: Activity? = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_reminder)

        FinishReminderDetailActivity.activity =  this@DetailReminderActivity

        mViewModel.state.observe(this, this@DetailReminderActivity)

        setupViews()
    }

    private fun setupViews(){
        intent.apply {
            val data = getSerializableExtra(DATA_REMINDER) as ReminderListResponse.Reminder

            binding.apply {
                imgCat.setPhotoCircularUrl(this@DetailReminderActivity, data.catsImage?:"")
                tvCat.text = data.catsName
                tvReminder.text = data.schedule
                tvDate.text = data.date
                tvTime.text = data.time
                tvDesc.text = data.description

                navBack.setOnClickListener {
                    onBackPressed()
                    this@DetailReminderActivity.finish()
                }

                btnEdit.setOnClickListener {
                    val intent = Intent(this@DetailReminderActivity, InputReminderActivity::class.java)
                    intent.putExtra(INPUT_TYPE, INPUT_UPDATE)
                    intent.putExtra(DATA_REMINDER, data)
                    startActivity(intent)
                }

                btnDelete.setOnClickListener {
                    showDialogDelete("Reminder will be delete permanently"){
                        doDeleteData(data.uuid?:"")
                    }
                }
            }
        }
    }

    private fun doDeleteData(uuid: String){
        if (uuid.isNotEmpty()){
            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

            mViewModel.doDeleteData(headers, uuid)
        }
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
    }

    override fun onChanged(state: DetailReminderState?) {
        when(state){
            is DetailReminderState.OnSuccessDeleteData -> {
                val status = state.baseResponse.code?:""
                val msg = state.baseResponse.message?:""

                if (status == "200"){
                    toast(msg)
                    onBackPressed()
                    this@DetailReminderActivity.finish()
                } else {
                    toast(msg)
                }
            }

            is DetailReminderState.OnLoading -> {
                val loading = state.isLoading

                if (loading){
                    showDialogLoading()
                } else {
                    dialogLoading.dismiss()
                }
            }

            is DetailReminderState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }
}