package dev.pawcat.ui.inputpost

import dev.pawcat.data.remote.response.AddPostResponse


interface IInputPostViewModel {
    fun successAddPost(addPostResponse: AddPostResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}