package dev.pawcat.ui.inputpost

import dev.pawcat.data.remote.response.AddPostResponse
import dev.pawcat.data.repo.PostRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InputPostViewModel(
    val postRepo: PostRepo
) : RxViewModel<InputPostState>(), IInputPostViewModel {
    override val TAG: String
        get() = InputPostState::class.java.simpleName

    override fun successAddPost(addPostResponse: AddPostResponse) {
        logD(TAG,"success do add post")
        onLoading(false)
        state.value = InputPostState.OnSuccessAddPost(addPostResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = InputPostState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = InputPostState.OnLoading(isLoading)
    }

    fun doAddPost(header:Map<String, String>, filePath: String, content: String) {
        logD(TAG,"do register")
        launch {
            onLoading(true)
            postRepo.doAddPost(header, filePath, content)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddPost) {
                    errorLoad(t = it)
                }
        }
    }
}