package dev.pawcat.ui.inputpost

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.scale
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityInputPostBinding
import dev.pawcat.ui.inputsuccess.SuccessInputActivity
import dev.pawcat.utils.*
import dev.pawcat.utils.view.ImagePickerActivity
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap


class InputPostActivity : AppCompatActivity(), Observer<InputPostState> {
    private val REQUEST_IMAGE = 101

    private val binding: ActivityInputPostBinding by lazy {
        DataBindingUtil.setContentView<ActivityInputPostBinding>(
            this, R.layout.activity_input_post
        )
    }
    private val mViewModel: InputPostViewModel by inject()
    private val session: Session by inject()

    private var imageFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@InputPostActivity)

        setupViews()
    }

    private fun setupViews(){
        binding.apply {
            btnAddImage.setOnClickListener {
               onImageClick()
            }

            btnAddPost.setOnClickListener {
                doAddPost()
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@InputPostActivity.finish()
            }
        }
    }

    private fun doAddPost(){
        val content = binding.etContent.getString()

        if (content.isNotEmpty() && imageFile != null){
            val headers = HashMap<String, String>()
            headers["Accept"] = "application/json"
            headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

            if (imageFile!!.exists()){

                mViewModel.doAddPost(
                    header = headers,
                    filePath = imageFile!!.absolutePath,
                    content = content
                )
            } else {
                toast("Image file not found")
            }
        } else {
            if (content.isEmpty()){
                binding.etContent.error = "Must be fill"
            }

            if (imageFile == null){
                binding.etImage.error = "Must be fill"
            }
        }

    }

    private fun onImageClick() {
        setupPermissionsCameraView({
            showImagePickerOptions()
        }, {
            showSettingsDialog()
        })
    }

    @SuppressLint("NewApi")
    private fun loadImage(url: String, uri: Uri){

        val baos = ByteArrayOutputStream()
        val bitmap = when {
            Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
                this.contentResolver,
                uri
            )
            else -> {
                val source = ImageDecoder.createSource(this.contentResolver, uri)
                ImageDecoder.decodeBitmap(source)
            }
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        bitmap.scale(700, 700)

        //convert from bitmap to bytearray
        val imageBytes = baos.toByteArray()

        imageFile = saveByteArrayImageToLocal(imageBytes, session[USER_EMAIL] ?: "")

        binding.etImage.setText(imageFile!!.absolutePath)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    loadImage(uri!!.toString(), uri)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object :
                ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@InputPostActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)

        startActivityForResult(
            intent,
            REQUEST_IMAGE
        )
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@InputPostActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(
            intent,
            REQUEST_IMAGE
        )
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(title: String){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)

        tvTitle.text = title
    }

    override fun onChanged(state: InputPostState?) {
        when(state){
            is InputPostState.OnSuccessAddPost -> {
                val status = state.addPostResponse.code ?: ""
                val msg = state.addPostResponse.message ?: ""

                if (status == "200") {
                    val intent = Intent(this@InputPostActivity, SuccessInputActivity::class.java)
                    startActivity(intent)
                    this@InputPostActivity.finish()
                } else {
                    toast("Failed added Post!")
                }
            }

            is InputPostState.OnLoading -> {
                val loading = state.isLoading
                if (loading) {
                    showDialogLoading("Adding Post...")
                } else {
                    dialogLoading.dismiss()
                }
            }

            is InputPostState.OnErrorState -> {
                val msg = state.t.localizedMessage ?: ""
                toast(msg)
            }
        }
    }
}