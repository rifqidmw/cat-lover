package dev.pawcat.ui.inputpost

import dev.pawcat.data.remote.response.AddPostResponse


sealed class InputPostState {
    data class OnSuccessAddPost(val addPostResponse: AddPostResponse) : InputPostState()
    data class OnErrorState(val t: Throwable) : InputPostState()
    data class OnLoading(val isLoading: Boolean) : InputPostState()
}