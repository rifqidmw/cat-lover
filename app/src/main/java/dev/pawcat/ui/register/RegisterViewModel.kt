package dev.pawcat.ui.register

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.RegisterResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RegisterViewModel(
    val authRepo: AuthRepo
) : RxViewModel<RegisterState>(), IRegisterViewModel{
    override val TAG: String
        get() = RegisterState::class.java.simpleName

    override fun successRegister(registerResponse: RegisterResponse) {
        logD(TAG,"success do register")
        state.value = RegisterState.OnSuccessRegister(registerResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = RegisterState.OnErrorState(t)
    }

    fun doRegister(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do register")
        launch {

            authRepo.doRegister(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successRegister) {
                    errorLoad(t = it)
                }
        }
    }
}