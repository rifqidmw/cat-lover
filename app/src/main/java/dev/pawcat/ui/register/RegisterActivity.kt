package dev.pawcat.ui.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.databinding.ActivityRegisterBinding
import dev.pawcat.utils.getString
import dev.pawcat.utils.setupStatusBarGradient
import dev.pawcat.utils.toast
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class RegisterActivity : AppCompatActivity(), Observer<RegisterState> {

    private val binding: ActivityRegisterBinding by lazy {
        DataBindingUtil.setContentView<ActivityRegisterBinding>(
            this, R.layout.activity_register
        )
    }
    private val mViewModel: RegisterViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@RegisterActivity)

        setupViews()
    }

    private fun setupViews(){
        setupStatusBarGradient(window, this@RegisterActivity)

        binding.apply {
            btnSignup.setOnClickListener {
                doRegister()
            }

            btnSignin.setOnClickListener {
                onBackPressed()
                this@RegisterActivity.finish()
            }
        }
    }

    private fun doRegister(){
        binding.apply {
            val username = etName.getString()
            val email = etEmail.getString()
            val phone = etPhone.getString()
            val password = etPassword.getString()
            val repassword = etRepassword.getString()


            if (username.isNotEmpty() && email.isNotEmpty() &&
                phone.isNotEmpty() && password.isNotEmpty() &&
                repassword.isNotEmpty()){

                if (password == repassword){
                    showDialogLoading()
                    removeErrorStateEdittext()

                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"

                    val jsonObject = JsonObject()
                    try {
                        jsonObject.addProperty("name", username)
                        jsonObject.addProperty("email", email)
                        jsonObject.addProperty("password", password)
                        jsonObject.addProperty("c_password", repassword)
                        jsonObject.addProperty("no_telp", phone)

                        mViewModel.doRegister(headers, jsonObject)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    etRepassword.setText("")
                    etRepassword.error = "Re-Password must be same with Password"
                }
            } else {
                if (username.isEmpty())
                    etName.error = "Must be fill"
                else
                    etLayoutName.isErrorEnabled = false
                if (email.isEmpty())
                    etEmail.error = "Must be fill"
                else
                    etLayoutEmail.isErrorEnabled = false
                if (phone.isEmpty())
                    etPhone.error = "Must be fill"
                else
                    etLayoutPhone.isErrorEnabled = false
                if (password.isEmpty())
                    etPassword.error = "Must be fill"
                else
                    etLayoutPassword.isErrorEnabled = false
                if (repassword.isEmpty())
                    etRepassword.error = "Must be fill"
                else
                    etLayoutRepassword.isErrorEnabled = false
            }
        }
    }

    private fun removeErrorStateEdittext(){
        binding.apply {
            etLayoutName.isErrorEnabled = false
            etLayoutEmail.isErrorEnabled = false
            etLayoutPhone.isErrorEnabled = false
            etLayoutPassword.isErrorEnabled = false
            etLayoutRepassword.isErrorEnabled = false
        }
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)
    }

    override fun onChanged(state: RegisterState?) {
        when(state){
            is RegisterState.OnSuccessRegister -> {
                val status = state.registerResponse.code?:""
                val msg = state.registerResponse.message?:""

                if (status == "200"){
                    toast("Register successfull")
                    onBackPressed()
                    this@RegisterActivity.finish()
                } else {
                    dialogLoading.dismiss()
                    toast("User already exists!")
                }
            }

            is RegisterState.OnErrorState -> {
                dialogLoading.dismiss()
                val msg = state.t.localizedMessage?:""
                toast(msg)
            }
        }
    }

}