package dev.pawcat.ui.register

import dev.pawcat.data.remote.response.RegisterResponse

interface IRegisterViewModel {
    fun successRegister(registerResponse: RegisterResponse)
    fun errorLoad(t: Throwable)
}