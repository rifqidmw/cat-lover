package dev.pawcat.ui.register

import dev.pawcat.data.remote.response.RegisterResponse

sealed class RegisterState {
    data class OnSuccessRegister(val registerResponse: RegisterResponse) : RegisterState()
    data class OnErrorState(val t: Throwable) : RegisterState()
}