package dev.pawcat.ui.inputreminder

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.ReminderListResponse
import dev.pawcat.databinding.ActivityInputReminderBinding
import dev.pawcat.ui.detailreminder.DetailReminderActivity
import dev.pawcat.ui.inputreminder.adapter.CatChooserAdapter
import dev.pawcat.ui.inputsuccess.SuccessInputActivity
import dev.pawcat.ui.main.mypet.pet.CatData
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.json.JSONException
import org.koin.android.ext.android.inject

class InputReminderActivity : AppCompatActivity(), Observer<InputReminderState> {
    private val TAG = InputReminderActivity::class.java.simpleName
    private val binding: ActivityInputReminderBinding by lazy {
        DataBindingUtil.setContentView<ActivityInputReminderBinding>(
            this, R.layout.activity_input_reminder
        )
    }

    private val mViewModel: InputReminderViewModel by inject()
    private val viewModelCatChooser: CatChooserViewModel by inject()
    private val session: Session by inject()

    private var reminderUUID: String = ""
    private var catUUID: String = ""
    private var inputType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@InputReminderActivity)

        setupIntent()
    }

    private fun setupIntent(){
        intent.apply {
            inputType = getStringExtra(INPUT_TYPE)?:""

            when (inputType){
                INPUT_NEW -> {
                    setupViews()
                }

                INPUT_UPDATE -> {
                    setupViews()
                    setData()
                }
            }
        }
    }

    private fun setupViews(){
        binding.apply {
            etCat.setOnClickListener {
                if (inputType == INPUT_NEW){
                    showDialogCatChooser(etCat)
                }
            }

            etDate.setOnClickListener {
                showDatePickerDialog(){
                    etDate.setText(it)
                }
            }

            etTime.setOnClickListener {
                showTimePickerDialog() {
                    etTime.setText(it)
                }
            }

            btnAddReminder.setOnClickListener {
                when (inputType){
                    INPUT_NEW -> {
                        doAddReminder()
                    }

                    INPUT_UPDATE -> {
                        doUpdateReminder()
                    }
                }
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@InputReminderActivity.finish()
            }
        }
    }

    private fun setData(){
        intent.apply {
            val data = getSerializableExtra(DATA_REMINDER) as ReminderListResponse.Reminder

            binding.apply {
                reminderUUID = data.uuid?:""
                catUUID = data.catsUUID?:""
                etCat.setText(data.catsName)
                setScheduleChip(data.schedule)
                etDate.setText(data.date)
                etTime.setText(data.time)
                etIntroduction.setText(data.description)
                setSwitchRepeat(data.repeat)

                tvToolbarTitle.text = "Update Cat"
                btnAddReminder.text = "UPDATE"
            }
        }
    }

    private fun doAddReminder(){
        binding.apply {
            val schedule = getScheduleChip()
            val description = etIntroduction.getString()
            val date = etDate.getString()
            val time = etTime.getString()
            val repeat = getSwitchRepeat()

            if (catUUID.isNotEmpty() && description.isNotEmpty() &&
                date.isNotEmpty() && time.isNotEmpty() &&
                time.isNotEmpty() && repeat.isNotEmpty() &&
                    schedule.isNotEmpty()){

                removeErrorStateEdittext()

                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("cats_uuid", catUUID)
                    jsonObject.addProperty("schedule", schedule)
                    jsonObject.addProperty("date", date)
                    jsonObject.addProperty("time", time)
                    jsonObject.addProperty("description", description)
                    jsonObject.addProperty("repeat", repeat)

                    mViewModel.doAddReminder(headers, jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                if (catUUID.isEmpty()){
                    etCat.error = "Must be fill"
                }

                if (date.isEmpty()){
                    etDate.error = "Must be fill"
                }

                if (time.isEmpty()){
                    etTime.error = "Must be fill"
                }

                if (description.isEmpty()){
                    etIntroduction.error = "Must be fill"
                }
            }
        }
    }

    private fun doUpdateReminder(){
        binding.apply {
            val schedule = getScheduleChip()
            val description = etIntroduction.getString()
            val date = etDate.getString()
            val time = etTime.getString()
            val repeat = getSwitchRepeat()

            if (reminderUUID.isNotEmpty() && catUUID.isNotEmpty() &&
                description.isNotEmpty() && date.isNotEmpty() &&
                time.isNotEmpty() && repeat.isNotEmpty() &&
                schedule.isNotEmpty()){

                removeErrorStateEdittext()

                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("cats_uuid", catUUID)
                    jsonObject.addProperty("schedule", schedule)
                    jsonObject.addProperty("date", date)
                    jsonObject.addProperty("time", time)
                    jsonObject.addProperty("description", description)
                    jsonObject.addProperty("repeat", repeat)

                    mViewModel.doUpdateReminder(headers, jsonObject, reminderUUID)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                if (reminderUUID.isEmpty()){
                    toast("Error IDx101009")
                    logE(TAG, "reminder uuid is empty")
                }

                if (catUUID.isEmpty()){
                    etCat.error = "Must be fill"
                }

                if (date.isEmpty()){
                    etDate.error = "Must be fill"
                }

                if (time.isEmpty()){
                    etTime.error = "Must be fill"
                }

                if (description.isEmpty()){
                    etIntroduction.error = "Must be fill"
                }
            }
        }
    }

    private fun removeErrorStateEdittext(){
        binding.apply {
            etLayoutCat.isErrorEnabled = false
            etLayoutIntroduction.isErrorEnabled = false
            etLayoutDate.isErrorEnabled = false
            etLayoutTime.isErrorEnabled = false
        }
    }

    private fun clearEdittext(){
        binding.apply {
            etCat.setText("")
            etIntroduction.setText("")
            etTime.setText("")
            etDate.setText("")


            catUUID = ""
        }
    }

    private fun getSwitchRepeat(): String{
        binding.apply {
            val repeat = if (switchRepeat.isChecked){
                "1"
            } else {
                "0"
            }

            return repeat
        }
    }

    private fun setSwitchRepeat(repeat: String?){
        binding.apply {
            when(repeat){
                "0" -> {
                    switchRepeat.isChecked = false
                }

                "1" -> {
                    switchRepeat.isChecked = true
                }
                else -> {
                    switchRepeat.isChecked = false
                }
            }
        }
    }

    private fun getScheduleChip(): String{

        binding.apply {
            var checkedChip: String = ""
            if (chipFood.isChecked){
                checkedChip = chipFood.getString()
            }

            if (chipVaccine.isChecked){
                checkedChip = chipVaccine.getString()
            }

            if (chipMedicine.isChecked){
                checkedChip = chipMedicine.getString()
            }
            /*chipGroupSchedule.setOnCheckedChangeListener { group, checkedId ->
                val chip:Chip? = findViewById(checkedId)

                chip?.let {
                    checkedChip = it.getString()

                }
            }*/
            return checkedChip
        }
    }

    private fun setScheduleChip(schedule: String?){
        binding.apply {
            when(schedule){
                "Vaccine" -> {
                    chipVaccine.isChecked = true
                }

                "Medicine" -> {
                    chipMedicine.isChecked = true
                }

                "Food" -> {
                    chipFood.isChecked = true
                }

                else -> chipFood.isChecked = true
            }
        }
    }

    private fun showDialogCatChooser(inputView: TextInputEditText){
        var optionChoose: CatData.CatItem? = null

        val customView = LayoutInflater.from(this).inflate(R.layout.dialog_input_data_reminder, null, false)
        val dialog = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()
        dialog.show()

        val tvTitle = customView.findViewById<TextView>(R.id.tv_title_dialog)
        val rvOption = customView.findViewById<RecyclerView>(R.id.rv_option)
        val progessBar = customView.findViewById<ProgressBar>(R.id.progress_bar)
        val btnChoose = customView.findViewById<Button>(R.id.btn_choose)

        tvTitle.setText("Select a pet")

        val catAdapter = CatChooserAdapter(this@InputReminderActivity)

        rvOption.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvOption.itemAnimator = DefaultItemAnimator()
        rvOption.adapter = catAdapter.withLoadStateFooter(
            footer = FooterLoadingAdapter{ catAdapter.retry() }
        )

        lifecycleScope.launch {
            viewModelCatChooser.cats.collectLatest {
                catAdapter.submitData(it)
            }
        }

        catAdapter.listener = object : CatChooserAdapter.OptionClickListener {
            override fun onItemOptionClicked(data: CatData.CatItem) {
                optionChoose = data
            }
        }

        catAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading){
                progessBar.visibility = View.VISIBLE
            } else {
                progessBar.visibility = View.GONE
                val errorState = when {
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                    loadState.refresh is LoadState.Error -> {
                        loadState.refresh as LoadState.Error
                    }
                    else -> null
                }
                if (loadState.append.endOfPaginationReached) {
                    if (catAdapter.itemCount > 0) {
                        errorState.let {
                            showSnackbar(it?.error?.message ?: "", customView, catAdapter)
                        }
                    }
                }
            }
        }

        btnChoose.setOnClickListener {
            if (optionChoose != null){
                inputView.setText(optionChoose?.catModel?.name?:"")

                catUUID = optionChoose?.catModel?.uuid?:""

                dialog.dismiss()
            } else {
                toast("Choose a cat first!")
            }
        }
    }

    private fun showSnackbar(msg: String, view: View, catAdapter: CatChooserAdapter){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                catAdapter.retry()
            })
            snack.show()
        }
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(title: String){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)

        tvTitle.text = title
    }

    override fun onChanged(state: InputReminderState?) {
        when(state){
            is InputReminderState.OnSuccessAddReminder -> {
                val status = state.addReminderResponse.code?:""
                val msg = state.addReminderResponse.message?:""

                if (status == "200"){
                    clearEdittext()
                    val intent = Intent(this@InputReminderActivity, SuccessInputActivity::class.java)
                    intent.putExtra(INPUT_SUCCESS, INPUT_SUCCESS_REMINDER)
                    startActivity(intent)
                    this@InputReminderActivity.finish()
                } else {
                    toast("Add data reminder failed")
                    logE(TAG, msg)
                }
            }

            is InputReminderState.OnSuccessUpdateReminder -> {
                val status = state.baseResponse.code?:""
                val msg = state.baseResponse.message?:""

                if (status == "200"){
                    DetailReminderActivity.FinishReminderDetailActivity.activity?.finish()
                    val intent = Intent(this@InputReminderActivity, SuccessInputActivity::class.java)
                    intent.putExtra(INPUT_SUCCESS, INPUT_SUCCESS_UPDATE_REMINDER)
                    startActivity(intent)
                    this@InputReminderActivity.finish()
                } else {
                    toast("Update data reminder failed")
                    logE(TAG, msg)
                }
            }

            is InputReminderState.OnLoading -> {
                val loading = state.isLoading
                if (loading){
                    showDialogLoading("Processing...")
                } else {
                    dialogLoading.dismiss()
                }
            }

            is InputReminderState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }
}