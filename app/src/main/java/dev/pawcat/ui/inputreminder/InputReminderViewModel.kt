package dev.pawcat.ui.inputreminder

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.AddReminderResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.ReminderRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InputReminderViewModel(
    private val reminderRepo: ReminderRepo
) : RxViewModel<InputReminderState>(), IInputReminderViewModel{
    override val TAG: String
        get() = InputReminderState::class.java.simpleName

    override fun successAddReminder(addReminderResponse: AddReminderResponse) {
        logD(TAG,"success do add reminder")
        onLoading(false)
        state.value = InputReminderState.OnSuccessAddReminder(addReminderResponse)
    }

    override fun successUpdateReminder(baseResponse: BaseResponse) {
        logD(TAG,"success do update reminder")
        onLoading(false)
        state.value = InputReminderState.OnSuccessUpdateReminder(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = InputReminderState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = InputReminderState.OnLoading(isLoading)
    }

    fun doAddReminder(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do add reminder")
        launch {
            onLoading(true)
            reminderRepo.doAddReminder(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddReminder) {
                    errorLoad(t = it)
                }
        }
    }

    fun doUpdateReminder(header:Map<String, String>, data: JsonObject, uuid: String) {
        logD(TAG,"do update reminder")
        launch {
            onLoading(true)
            reminderRepo.doUpdateReminder(header, data, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successUpdateReminder) {
                    errorLoad(t = it)
                }
        }
    }
}