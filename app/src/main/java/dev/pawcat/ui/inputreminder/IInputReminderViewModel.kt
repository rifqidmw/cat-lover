package dev.pawcat.ui.inputreminder

import dev.pawcat.data.remote.response.AddReminderResponse
import dev.pawcat.data.remote.response.base.BaseResponse


interface IInputReminderViewModel {
    fun successAddReminder(addReminderResponse: AddReminderResponse)
    fun successUpdateReminder(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}