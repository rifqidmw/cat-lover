package dev.pawcat.ui.inputreminder

import dev.pawcat.data.remote.response.AddReminderResponse
import dev.pawcat.data.remote.response.base.BaseResponse

sealed class InputReminderState {
    data class OnSuccessAddReminder(val addReminderResponse: AddReminderResponse) : InputReminderState()
    data class OnSuccessUpdateReminder(val baseResponse: BaseResponse) : InputReminderState()
    data class OnErrorState(val t: Throwable) : InputReminderState()
    data class OnLoading(val isLoading: Boolean) : InputReminderState()
}