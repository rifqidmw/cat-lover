package dev.pawcat.ui.inputreminder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.databinding.ItemPetChooserBinding
import dev.pawcat.ui.main.mypet.pet.CatData

class CatChooserAdapter(
    private val context: Context
) : PagingDataAdapter<CatData.CatItem, CatChooserAdapter.CatViewHolder>(DIIF_UTIL)  {

    var mSelectedItem = -1
    var listener: OptionClickListener? = null

    open inner class CatViewHolder(val binding: ItemPetChooserBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(catModel: CatData.CatItem, position: Int){

            binding.apply {
                val data = catModel.catModel
                tvNameOption.text = data.name

                root.setOnClickListener {
                    mSelectedItem = position
                    listener?.onItemOptionClicked(catModel)
                    notifyDataSetChanged()
                }
            }

        }
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, position)
        }

        holder.apply {
            if (position == mSelectedItem){
                itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrayLight))
            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        val view = LayoutInflater.from(parent.context)

        return CatViewHolder(
            ItemPetChooserBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<CatData.CatItem>() {
            override fun areItemsTheSame(oldItem: CatData.CatItem, newItem: CatData.CatItem): Boolean =
                oldItem.catModel.uuid == newItem.catModel.uuid

            override fun areContentsTheSame(oldItem: CatData.CatItem, newItem: CatData.CatItem): Boolean =
                oldItem == newItem
        }
    }

    interface OptionClickListener {
        fun onItemOptionClicked(data: CatData.CatItem)
    }
}