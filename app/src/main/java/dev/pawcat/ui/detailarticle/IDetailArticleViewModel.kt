package dev.pawcat.ui.detailarticle

import dev.pawcat.data.remote.response.AddCommentArticleResponse
import dev.pawcat.data.remote.response.AddCommentPostResponse

interface IDetailArticleViewModel {
    fun successAddComment(addCommentArticleResponse: AddCommentArticleResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}