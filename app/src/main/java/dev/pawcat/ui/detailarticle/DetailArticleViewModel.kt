package dev.pawcat.ui.detailarticle

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.AddCommentArticleResponse
import dev.pawcat.data.remote.response.CommentArticleListResponse
import dev.pawcat.data.repo.ArticleRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.PostRepo
import dev.pawcat.data.repo.paged.CommentArticleListPagingSource
import dev.pawcat.data.repo.paged.CommentPostListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DetailArticleViewModel(
    private val articleRepo: ArticleRepo,
    private val pagedListRepo: PagedListRepo, private val context: Context
) : RxViewModel<DetailArticleState>(), IDetailArticleViewModel{
    override val TAG: String
        get() = DetailArticleState::class.java.simpleName

    override fun successAddComment(addCommentArticleResponse: AddCommentArticleResponse) {
        logD(TAG,"success do add comment")
        onLoading(false)
        state.value = DetailArticleState.OnSuccessAddComment(addCommentArticleResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = DetailArticleState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = DetailArticleState.OnLoading(isLoading)
    }

    fun comments(articleUUID: String): Flow<PagingData<CommentData.CommentItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            CommentArticleListPagingSource(headers = headers, pagedListRepo = pagedListRepo, articleUUID = articleUUID)
        }.flow.map { pagingData -> pagingData.map { CommentData.CommentItem(it) } }
    }

    fun doAddComment(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do add comment post")
        launch {
            onLoading(true)
            articleRepo.doAddComment(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddComment) {
                    errorLoad(t = it)
                }
        }
    }
}


sealed class CommentData{
    data class CommentItem(val commentPostModel: CommentArticleListResponse.Comment) : CommentData()
}