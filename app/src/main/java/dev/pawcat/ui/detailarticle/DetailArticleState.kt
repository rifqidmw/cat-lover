package dev.pawcat.ui.detailarticle

import dev.pawcat.data.remote.response.AddCommentArticleResponse
import dev.pawcat.data.remote.response.AddCommentPostResponse
import dev.pawcat.ui.detailpost.DetailPostState

sealed class DetailArticleState {
    data class OnSuccessAddComment(val addCommentArticleResponse: AddCommentArticleResponse) : DetailArticleState()
    data class OnErrorState(val t: Throwable) : DetailArticleState()
    data class OnLoading(val isLoading: Boolean) : DetailArticleState()
}