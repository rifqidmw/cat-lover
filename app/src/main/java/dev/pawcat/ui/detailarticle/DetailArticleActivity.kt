package dev.pawcat.ui.detailarticle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.ArticleListResponse
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.databinding.ActivityDetailArticleBinding
import dev.pawcat.ui.detailarticle.adapter.ArticleCommentAdapter
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class DetailArticleActivity : AppCompatActivity(), Observer<DetailArticleState> {

    private val binding: ActivityDetailArticleBinding by lazy {
        DataBindingUtil.setContentView<ActivityDetailArticleBinding>(
            this, R.layout.activity_detail_article
        )
    }
    private val mViewModel: DetailArticleViewModel by inject()
    private val session: Session by inject()
    private val commentAdapter: ArticleCommentAdapter by lazy { ArticleCommentAdapter(this@DetailArticleActivity, session) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@DetailArticleActivity)

        setupViews()
        loadDataComment()
    }

    private fun setupViews(){
        intent.apply {
            val data = getSerializableExtra(DATA_ARTICLE) as ArticleListResponse.Article

            binding.apply {
                imgArticle.setImageviewUrl(this@DetailArticleActivity, data.image?:"")
                tvCategory.text = data.kategoriTitle
                tvDate.text = data.createdAt!!.changeDateFormat().setTimeAgo()
                tvContent.text = data.content

                rvComment.adapter = commentAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ commentAdapter.retry() }
                )

                etComment.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {}
                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (count > 0){
                            btnComment.setTextColor(ContextCompat.getColor(this@DetailArticleActivity, R.color.colorGradientPurple))
                            btnComment.isClickable = true
                        } else {
                            btnComment.setTextColor(ContextCompat.getColor(this@DetailArticleActivity, R.color.colorTextGray))
                            btnComment.isClickable = false
                        }
                    }
                    override fun afterTextChanged(s: Editable?) {}
                })

                btnComment.setOnClickListener {
                    doAddComment(data.uuid)
                }

                navBack.setOnClickListener {
                    onBackPressed()
                    this@DetailArticleActivity.finish()
                }

                executePendingBindings()
            }
        }
    }

    private fun loadDataComment(){
        val data = intent.getSerializableExtra(DATA_ARTICLE) as ArticleListResponse.Article

        binding.apply {

            lifecycleScope.launch {
                mViewModel.comments(data.uuid!!).collectLatest {
                    commentAdapter.submitData(it)
                }
            }

            commentAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    if (loadState.append.endOfPaginationReached) {
                        if (commentAdapter.itemCount > 0) {
                            errorState.let {
                                showSnackbar(it?.error?.message ?: "", root)
                            }
                        }
                    }
                    if (commentAdapter.itemCount > 0){
                        tvNoComment.visibility = View.GONE
                    } else {
                        tvNoComment.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun doAddComment(articleUUID: String?){
        binding.apply {
            val comment = etComment.getString()

            if (comment.isNotEmpty()){
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("artikel_uuid", articleUUID)
                    jsonObject.addProperty("content", comment)
                    jsonObject.addProperty("parent_uuid", 0)

                    mViewModel.doAddComment(headers, jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun showSnackbar(msg: String, view: View){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                commentAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onChanged(state: DetailArticleState?) {
        when(state){
            is DetailArticleState.OnSuccessAddComment -> {
                val status = state.addCommentArticleResponse.code?:""
                val msg = state.addCommentArticleResponse.message?:""

                if (status == "200"){
                    loadDataComment()
                    binding.apply {
                        etComment.setText("")
                        rvComment.smoothScrollToPosition(0)
                    }
                } else {
                    toast(msg)
                }
            }

            is DetailArticleState.OnLoading -> {
                val loading = state.isLoading
                binding.apply {
                    if (loading){
                        btnComment.visibility = View.GONE
                        progressBarSend.visibility = View.VISIBLE
                    } else {
                        btnComment.visibility = View.VISIBLE
                        progressBarSend.visibility = View.GONE
                    }
                }
            }

            is DetailArticleState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }
}