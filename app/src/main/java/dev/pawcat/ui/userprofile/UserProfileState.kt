package dev.pawcat.ui.userprofile

import dev.pawcat.data.remote.response.LogoutResponse
import dev.pawcat.data.remote.response.UpdateAvatarResponse


sealed class UserProfileState {
    data class OnSuccessUpdateAvatar(val updateAvatarResponse: UpdateAvatarResponse) : UserProfileState()
    data class OnSuccessLogout(val logoutResponse: LogoutResponse) : UserProfileState()
    data class OnErrorState(val t: Throwable) : UserProfileState()
    data class OnErrorLogout(val t: Throwable) : UserProfileState()
    data class OnLoading(val isLoading: Boolean) : UserProfileState()
}