package dev.pawcat.ui.userprofile

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.graphics.scale
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityUserProfileBinding
import dev.pawcat.ui.editpassword.EditPasswordActivity
import dev.pawcat.ui.edituserprofile.EditUserProfileActivity
import dev.pawcat.ui.login.LoginActivity
import dev.pawcat.utils.*
import dev.pawcat.utils.view.ImagePickerActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.HashMap

class UserProfileActivity : AppCompatActivity(), Observer<UserProfileState> {

    private val REQUEST_IMAGE = 101

    private val binding: ActivityUserProfileBinding by lazy {
        DataBindingUtil.setContentView<ActivityUserProfileBinding>(
            this, R.layout.activity_user_profile
        )
    }
    private val session: Session by inject()
    private val mViewModel: UserProfileViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@UserProfileActivity)

        setupViews()
    }

    private fun setupViews(){
        binding.apply {

            imgProfile.setPhotoCircularUrl(this@UserProfileActivity, session[USER_AVATAR]?:"")
            imgProfile.setOnClickListener {
                onImageClick()
            }

            btnChangeProfile.setOnClickListener {
                val intent = Intent(this@UserProfileActivity,  EditUserProfileActivity::class.java)
                startActivity(intent)
            }

            btnChangePassword.setOnClickListener {
                val intent = Intent(this@UserProfileActivity,  EditPasswordActivity::class.java)
                startActivity(intent)
            }

            btnLogout.setOnClickListener {
                doLogout()
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@UserProfileActivity.finish()
            }
        }
    }

    private fun setProfile(){
        binding.apply {
            tvName.text = session[USERNAME]
            tvEmail.text = session[USER_EMAIL]
        }
    }

    private fun doLogout(){
        showDialogLoading("Loading...")

        val headers = HashMap<String, String>()
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

        mViewModel.doLogout(headers)
    }

    private fun doUpdateAvatar(imageUrl: String){
        val imageFile = File(imageUrl)
        val headers = HashMap<String, String>()
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

        if (imageFile.exists()){
            val requestBody = imageFile.asRequestBody("image/*".toMediaTypeOrNull())
            val dataImage = MultipartBody.Part.createFormData(
                "avatar",
                imageFile.name,
                requestBody
            )

            mViewModel.doUpdateAvatar(headers, dataImage)
        } else {
            toast("Image file not found")
        }
    }

    private fun onImageClick() {
        setupPermissionsCameraView({
            showImagePickerOptions()
        }, {
            showSettingsDialog()
        })
    }

    @SuppressLint("NewApi")
    private fun loadImage(url: String, uri: Uri){

        val baos = ByteArrayOutputStream()
        val bitmap = when {
            Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
                this.contentResolver,
                uri
            )
            else -> {
                val source = ImageDecoder.createSource(this.contentResolver, uri)
                ImageDecoder.decodeBitmap(source)
            }
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        bitmap.scale(700, 700)

        //convert from bitmap to bytearray
        val imageBytes = baos.toByteArray()

        val file = saveByteArrayImageToLocal(imageBytes, session[USER_EMAIL]?:"")

        doUpdateAvatar(file.absolutePath)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    loadImage(uri!!.toString(), uri)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object :
                ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@UserProfileActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)

        startActivityForResult(intent,
            REQUEST_IMAGE
        )
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@UserProfileActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent,
            REQUEST_IMAGE
        )
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(title: String){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)

        tvTitle.text = title
    }

    override fun onResume() {
        super.onResume()
        setProfile()
    }

    override fun onChanged(state: UserProfileState?) {
        when (state){
            is UserProfileState.OnSuccessUpdateAvatar -> {
                val status = state.updateAvatarResponse.code?:""

                if (status == "200"){
                    val avatar = state.updateAvatarResponse.avatar?:""
                    binding.imgProfile.setPhotoCircularUrl(this@UserProfileActivity, avatar)
                    session.save(USER_AVATAR, avatar)
                } else {
                    toast("Update avatar failed..")
                }
            }

            is UserProfileState.OnSuccessLogout -> {
                val status = state.logoutResponse.code?:""
                val msg = state.logoutResponse.message?:""

                if (status == "200"){
                    session.clear()
                    val intent = Intent(this@UserProfileActivity, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                } else {
                    toast(msg)
                }
            }

            is UserProfileState.OnLoading -> {
                val loading = state.isLoading
                if (loading){
                    showDialogLoading("Upload Avatar...")
                } else {
                    dialogLoading.dismiss()
                }
            }

            is UserProfileState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }

            is UserProfileState.OnErrorLogout -> {
                dialogLoading.dismiss()
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }
}