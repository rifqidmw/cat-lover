package dev.pawcat.ui.userprofile

import dev.pawcat.data.remote.response.LogoutResponse
import dev.pawcat.data.remote.response.UpdateAvatarResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

class UserProfileViewModel(
    val authRepo: AuthRepo
) : RxViewModel<UserProfileState>(), IUserProfileViewModel {
    override val TAG: String
        get() = UserProfileState::class.java.simpleName

    override fun successUpdateAvatar(updateAvatarResponse: UpdateAvatarResponse) {
        logD(TAG,"success do update avatar")
        onLoading(false)
        state.value = UserProfileState.OnSuccessUpdateAvatar(updateAvatarResponse)
    }

    override fun successLogout(logoutResponse: LogoutResponse) {
        logD(TAG,"success do update avatar")
        onLoading(false)
        state.value = UserProfileState.OnSuccessLogout(logoutResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = UserProfileState.OnErrorState(t)
    }

    override fun errorLogout(t: Throwable) {
        logE(TAG,"error logout : ${t.message}")
        state.value = UserProfileState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = UserProfileState.OnLoading(isLoading)
    }

    fun doUpdateAvatar(header:Map<String, String>, imageFile: MultipartBody.Part) {
        logD(TAG,"do update avatar")
        launch {
            onLoading(true)
            authRepo.doUpdateAvatar(header, imageFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successUpdateAvatar) {
                    errorLoad(t = it)
                }
        }
    }

    fun doLogout(header:Map<String, String>) {
        logD(TAG,"do logout")
        launch {
            authRepo.doLogout(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLogout) {
                    errorLogout(t = it)
                }
        }
    }
}