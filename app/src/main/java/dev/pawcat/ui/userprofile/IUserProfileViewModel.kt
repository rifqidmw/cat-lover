package dev.pawcat.ui.userprofile

import dev.pawcat.data.remote.response.LogoutResponse
import dev.pawcat.data.remote.response.UpdateAvatarResponse


interface IUserProfileViewModel {
    fun successUpdateAvatar(updateAvatarResponse: UpdateAvatarResponse)
    fun successLogout(logoutResponse: LogoutResponse)
    fun errorLoad(t: Throwable)
    fun errorLogout(t: Throwable)
    fun onLoading(isLoading: Boolean)
}