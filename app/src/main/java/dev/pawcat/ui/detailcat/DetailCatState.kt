package dev.pawcat.ui.detailcat

import dev.pawcat.data.remote.response.CatGalleryResponse
import dev.pawcat.data.remote.response.base.AddCommentCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.ui.inputpet.InputCatState

sealed class DetailCatState {
    data class OnSuccessDeleteData(val baseResponse: BaseResponse) : DetailCatState()
    data class OnSuccessAddComment(val addCommentCatResponse: AddCommentCatResponse) : DetailCatState()
    data class OnSuccessGetGallery(val catGalleryResponse: CatGalleryResponse) : DetailCatState()
    data class OnSuccessUpdateCat(val baseResponse: BaseResponse) : DetailCatState()
    data class OnErrorState(val t: Throwable) : DetailCatState()
    data class OnErrorGalleryState(val t: Throwable) : DetailCatState()
    data class OnErrorComment(val t: Throwable) : DetailCatState()
    data class OnLoading(val isLoading: Boolean) : DetailCatState()
    data class OnLoadingComment(val isLoading: Boolean) : DetailCatState()
}