package dev.pawcat.ui.detailcat

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.asura.library.posters.Poster
import com.asura.library.posters.RemoteImage
import com.asura.library.posters.RemoteVideo
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.databinding.ActivityDetailPetBinding
import dev.pawcat.ui.detailcat.adapter.CatCommentAdapter
import dev.pawcat.ui.inputpet.InputCatActivity
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class DetailCatActivity : AppCompatActivity(), Observer<DetailCatState> {

    private val binding: ActivityDetailPetBinding by lazy {
        DataBindingUtil.setContentView<ActivityDetailPetBinding>(
            this, R.layout.activity_detail_pet
        )
    }
    private val session: Session by inject()
    private val mViewModel: DetailCatViewModel by inject()
    private val commentAdapter by lazy { CatCommentAdapter(this@DetailCatActivity, session) }

    private val listPoster: ArrayList<Poster> = ArrayList()

    class FinishCatDetailActivity{
        companion object{
            var activity: Activity? = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@DetailCatActivity)

        FinishCatDetailActivity.activity = this@DetailCatActivity

        setupViews()
        loadDataComment()
    }

    private fun setupViews(){
        intent.apply {
             val data = getSerializableExtra(DATA_CAT) as CatListResponse.Cat
            binding.apply {
                if (data.usersUUID == session[USER_UUID]){
                    btnEdit.visibility = View.VISIBLE
                    btnDelete.visibility = View.VISIBLE
                    btnLike.visibility = View.INVISIBLE
                    lyContact.visibility = View.GONE
                    btnLike.isClickable = false

                } else {
                    btnEdit.visibility = View.GONE
                    btnDelete.visibility = View.GONE
                    btnLike.visibility = View.VISIBLE
                    lyContact.visibility = View.VISIBLE
                    btnLike.isClickable = true
                }

                if (data.available != "2"){
                    btnAdoption.visibility = View.VISIBLE
                } else {
                    btnAdoption.visibility = View.GONE
                }


                loadDataGallery(data.uuid!!)

                //image
                imgProfile.setPhotoCircularUrl(this@DetailCatActivity, data.usersAvatar?:"")
                when(data.gender?.toInt()){
                    1 -> {
                        imgGender.setImageviewLocal(this@DetailCatActivity, R.drawable.ic_gender_male)
                    }
                    2 -> {
                        imgGender.setImageviewLocal(this@DetailCatActivity, R.drawable.ic_gener_female)
                    }
                }

                //text
                tvBreed.text = data.breed
                tvGender.text = data.genderText
                tvWeight.text = data.weight
                tvAge.text = "${data.age!!.getBOD()} Months"
                tvName.text = data.name
                tvDesc.text = data.content
                tvUsername.text = data.usersName

                when(data.available){
                    "0" -> {
                        tvAdoption.text = "Not available for adoption"
                        tvAdoption.setTextColor(ContextCompat.getColor(this@DetailCatActivity, R.color.colorTextRed))
                        tvAdoption.background = ContextCompat.getDrawable(this@DetailCatActivity, R.drawable.background_adoption_unavailable)
                    }

                    "1" -> {
                        tvAdoption.text = "Available for adoption"
                        tvAdoption.setTextColor(ContextCompat.getColor(this@DetailCatActivity, R.color.colorTextGreen))
                        tvAdoption.background = ContextCompat.getDrawable(this@DetailCatActivity, R.drawable.background_adoption_available)
                    }

                    "2" -> {
                        tvAdoption.text = "Adopted"
                        tvAdoption.setTextColor(ContextCompat.getColor(this@DetailCatActivity, R.color.colorTextRed))
                        tvAdoption.background = ContextCompat.getDrawable(this@DetailCatActivity, R.drawable.background_adoption_unavailable)
                    }
                }

                etComment.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {}
                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (count > 0){
                            btnComment.setTextColor(ContextCompat.getColor(this@DetailCatActivity, R.color.colorGradientPurple))
                            btnComment.isClickable = true
                        } else {
                            btnComment.setTextColor(ContextCompat.getColor(this@DetailCatActivity, R.color.colorTextGray))
                            btnComment.isClickable = false
                        }
                    }
                    override fun afterTextChanged(s: Editable?) {}
                })

                rvComment.adapter = commentAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ commentAdapter.retry() }
                )

                btnComment.setOnClickListener {
                    doAddComment(data.uuid)
                }

                navBack.setOnClickListener {
                    onBackPressed()
                    this@DetailCatActivity.finish()
                }

                btnEdit.setOnClickListener {
                    val intent = Intent(this@DetailCatActivity, InputCatActivity::class.java)
                    intent.putExtra(INPUT_TYPE, INPUT_UPDATE)
                    intent.putExtra(DATA_CAT, data)
                    startActivity(intent)
                }

                btnDelete.setOnClickListener {
                    showDialogDelete("Profile of pet will be delete permanently"){
                        doDeleteData(data.uuid?:"")
                    }
                }

                btnAdoption.setOnClickListener {
                    showDialogAdopted("Profile of cat will be set as Adopted"){
                        doUpdateCat(data)
                    }
                }

                btnContact.setOnClickListener {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:"+data.usersPhone)
                    startActivity(intent)
                }

                imgProfile.setOnClickListener {
                    val userUUID = data.usersUUID
                    if (userUUID != session[USER_UUID]){
                        intentToUser(userUUID)
                    }
                }

                executePendingBindings()
            }
        }
    }

    private fun doAddComment(catUUID: String?){
        binding.apply {
            val comment = etComment.getString()

            if (comment.isNotEmpty()){
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("cat_uuid", catUUID)
                    jsonObject.addProperty("content", comment)
                    jsonObject.addProperty("parent_uuid", 0)

                    mViewModel.doAddComment(headers, jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            executePendingBindings()
        }
    }

    private fun loadDataGallery(uuid: String){
        mViewModel.doGetGallery(getParamHeader(), uuid)
    }

    private fun loadDataComment(){
        val data = intent.getSerializableExtra(DATA_CAT) as CatListResponse.Cat
        binding.apply {
            lifecycleScope.launch {
                mViewModel.comments(data.uuid!!).collectLatest {
                    commentAdapter.submitData(it)
                }
            }

            commentAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    if (loadState.append.endOfPaginationReached) {
                        if (commentAdapter.itemCount > 0) {
                            errorState.let {
                                showSnackbar(it?.error?.message ?: "", root)
                            }
                        }
                    }

                    if (commentAdapter.itemCount > 0){
                        tvNoComment.visibility = View.GONE
                    } else {
                        tvNoComment.visibility = View.VISIBLE
                    }
                }
            }

            executePendingBindings()
        }
    }

    private fun showSnackbar(msg: String, view: View){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                commentAdapter.retry()
            })
            snack.show()
        }
    }

    private fun doDeleteData(uuid: String){
        if (uuid.isNotEmpty()){
            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

            mViewModel.doDeleteData(headers, uuid)
        }
    }

    private fun doUpdateCat(data: CatListResponse.Cat){
        val headers = HashMap<String, String>()
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

        val jsonObject = JsonObject()
        jsonObject.addProperty("name", data.name)
        jsonObject.addProperty("content", data.content)
        jsonObject.addProperty("breed", data.breed)
        jsonObject.addProperty("gender", data.gender)
        jsonObject.addProperty("age", data.age)
        jsonObject.addProperty("weight", data.weight)
        jsonObject.addProperty("available", 2)
        jsonObject.addProperty("jenis_pet", data.jenisPet?:"")
        jsonObject.addProperty("id_provinsi", data.idProvinsi?:"")
        jsonObject.addProperty("id_kota", data.idKota?:"")
        jsonObject.addProperty("uuid", data.uuid)

        mViewModel.doUpdateCat(headers, jsonObject)
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
    }

    override fun onChanged(state: DetailCatState?) {
        when (state){
            is DetailCatState.OnSuccessDeleteData -> {
                val status = state.baseResponse.code?:""
                val msg= state.baseResponse.message?:""

                if (status == "200"){
                    toast(msg)
                    onBackPressed()
                    this@DetailCatActivity.finish()
                } else {
                    toast(msg)
                }
            }

            is DetailCatState.OnSuccessAddComment -> {
                val status = state.addCommentCatResponse.code?:""
                val msg = state.addCommentCatResponse.message?:""

                if (status == "200"){
                    loadDataComment()
                    binding.apply {
                        etComment.setText("")
                        rvComment.smoothScrollToPosition(0)

                        executePendingBindings()
                    }
                } else {
                    toast(msg)
                }
            }

            is DetailCatState.OnSuccessGetGallery -> {
                val status = state.catGalleryResponse.code?:""
                val msg = state.catGalleryResponse.message?:""

                if (status == "200"){
                    val data = state.catGalleryResponse.data?.post

                    if (data != null) {
                        if (data.isNotEmpty()){

                            for (i in data.indices){
                                if (data[i].urlFile!!.contains("mp4")){
                                    val videoUri = Uri.parse(data[i].urlFile)
                                    listPoster.add(RemoteVideo(videoUri))
                                } else {
                                    listPoster.add(RemoteImage(data[i].urlFile))
                                }
                            }
                        }
                    }
                }

                binding.imgCat.setPosters(listPoster)
            }

            is DetailCatState.OnSuccessUpdateCat -> {
                binding.btnAdoption.visibility = View.GONE
            }

            is DetailCatState.OnLoading -> {
                val loading = state.isLoading

                if (loading){
                    showDialogLoading()
                } else {
                    dialogLoading.dismiss()
                }
            }

            is DetailCatState.OnLoadingComment -> {
                val loading = state.isLoading
                binding.apply {
                    if (loading){
                        btnComment.visibility = View.GONE
                        progressBarSend.visibility = View.VISIBLE
                    } else {
                        btnComment.visibility = View.VISIBLE
                        progressBarSend.visibility = View.GONE
                    }

                    executePendingBindings()
                }
            }

            is DetailCatState.OnErrorState -> {
                val msg = state.t.localizedMessage
                toast(msg)
            }
        }
    }
}