package dev.pawcat.ui.detailcat

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.CatGalleryResponse
import dev.pawcat.data.remote.response.CommentCatListResponse
import dev.pawcat.data.remote.response.base.AddCommentCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.CatRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.CommentCatListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DetailCatViewModel(
    private val catRepo: CatRepo,
    private val pagedListRepo: PagedListRepo, private val context: Context
) : RxViewModel<DetailCatState>(), IDetailCatViewModel{
    override val TAG: String
        get() = DetailCatState::class.java.simpleName

    override fun successDeleteData(baseResponse: BaseResponse) {
        logD(TAG,"success do delete data cat")
        onLoading(false)
        state.value = DetailCatState.OnSuccessDeleteData(baseResponse)
    }

    override fun successUpdateCat(baseResponse: BaseResponse) {
        logD(TAG,"success do update data cat")
        onLoading(false)
        state.value = DetailCatState.OnSuccessUpdateCat(baseResponse)
    }

    override fun successAddComment(addCommentCatResponse: AddCommentCatResponse) {
        logD(TAG,"success do add comment")
        onLoadingComment(false)
        state.value = DetailCatState.OnSuccessAddComment(addCommentCatResponse)
    }

    override fun successGetGallery(catGalleryResponse: CatGalleryResponse) {
        logD(TAG,"success do get gallery")
        state.value = DetailCatState.OnSuccessGetGallery(catGalleryResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = DetailCatState.OnErrorState(t)
    }

    override fun errorLoadGallery(t: Throwable) {
        logE(TAG,"error load gallery: ${t.message}")
        state.value = DetailCatState.OnErrorState(t)
    }

    override fun errorComment(t: Throwable) {
        logE(TAG,"error load comment: ${t.message}")
        onLoadingComment(false)
        state.value = DetailCatState.OnErrorComment(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = DetailCatState.OnLoading(isLoading)
    }

    override fun onLoadingComment(isLoading: Boolean) {
        logE(TAG,"on loading comment: $isLoading")
        state.value = DetailCatState.OnLoadingComment(isLoading)
    }

    fun comments(catUUID: String): Flow<PagingData<CommentData.CommentItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            CommentCatListPagingSource(headers = headers, pagedListRepo = pagedListRepo, catUUID = catUUID)
        }.flow.map { pagingData -> pagingData.map { CommentData.CommentItem(it) } }
    }

    fun doDeleteData(header:Map<String, String>, uuid: String) {
        logD(TAG,"do register")
        launch {
            onLoading(true)
            catRepo.doDeleteCat(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successDeleteData) {
                    errorLoad(t = it)
                }
        }
    }

    fun doUpdateCat(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do update reminder")
        launch {
            onLoading(true)
            catRepo.doUpdateCat(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successUpdateCat) {
                    errorLoad(t = it)
                }
        }
    }

    fun doAddComment(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do add comment cat")
        launch {
            onLoadingComment(true)
            catRepo.doAddComment(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddComment) {
                    errorComment(t = it)
                }
        }
    }

    fun doGetGallery(header:Map<String, String>, uuid: String) {
        logD(TAG,"do add comment cat")
        launch {
            catRepo.doGetGallery(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetGallery) {
                    errorLoadGallery(t = it)
                }
        }
    }
}

sealed class CommentData{
    data class CommentItem(val commentCatModel: CommentCatListResponse.Comment) : CommentData()
}