package dev.pawcat.ui.detailcat

import dev.pawcat.data.remote.response.CatGalleryResponse
import dev.pawcat.data.remote.response.base.AddCommentCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse

interface IDetailCatViewModel {
    fun successDeleteData(baseResponse: BaseResponse)
    fun successUpdateCat(baseResponse: BaseResponse)
    fun successAddComment(addCommentCatResponse: AddCommentCatResponse)
    fun successGetGallery(catGalleryResponse: CatGalleryResponse)
    fun errorLoad(t: Throwable)
    fun errorLoadGallery(t: Throwable)
    fun errorComment(t: Throwable)
    fun onLoading(isLoading: Boolean)
    fun onLoadingComment(isLoading: Boolean)
}