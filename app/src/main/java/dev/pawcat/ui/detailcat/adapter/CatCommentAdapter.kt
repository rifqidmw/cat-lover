package dev.pawcat.ui.detailcat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ItemCommentBinding
import dev.pawcat.ui.detailcat.CommentData
import dev.pawcat.utils.*

class CatCommentAdapter(
    private val context: Context,
    private val session: Session
) : PagingDataAdapter<CommentData.CommentItem, CatCommentAdapter.CatCommentViewHolder>(DIIF_UTIL) {
    open inner class CatCommentViewHolder(
        val binding: ItemCommentBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(commentModel: CommentData.CommentItem){
            val data = commentModel.commentCatModel
            binding.apply {
                imgProfile.setPhotoCircularUrl(context, data.userAvatar?:"")
                tvUsername.text = data.userName
                tvComment.text = data.content
                tvDate.text = data.createdAt!!.changeDateFormat().setTimeAgo()

                imgProfile.setOnClickListener {
                    val userUUID = data.usersUUID
                    if (userUUID != session[USER_UUID]){
                        context.intentToUser(userUUID)
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: CatCommentViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatCommentViewHolder {
        val view = LayoutInflater.from(parent.context)

        return CatCommentViewHolder(
            ItemCommentBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<CommentData.CommentItem>(){
            override fun areItemsTheSame(oldItem: CommentData.CommentItem, newItem: CommentData.CommentItem): Boolean {
                return oldItem.commentCatModel.commentUUID == newItem.commentCatModel.commentUUID
            }

            override fun areContentsTheSame(oldItem: CommentData.CommentItem, newItem: CommentData.CommentItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}