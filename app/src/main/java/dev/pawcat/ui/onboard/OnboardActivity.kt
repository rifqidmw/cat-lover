package dev.pawcat.ui.onboard

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityOnboardBinding
import dev.pawcat.ui.login.LoginActivity
import dev.pawcat.ui.onboard.adapter.PagerOnboardingAdapter
import dev.pawcat.utils.IS_FIRST_TIME
import kotlinx.android.synthetic.main.activity_onboard.*
import org.koin.android.ext.android.inject

class OnboardActivity : AppCompatActivity() {

    private val session: Session by inject()
    private val binding: ActivityOnboardBinding by lazy {
        DataBindingUtil.setContentView<ActivityOnboardBinding>(
            this, R.layout.activity_onboard
        )
    }
    private var page: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboard)
//        throw RuntimeException("crash testing 123")
        setupViews()
    }

    private fun setupViews(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        session.save(IS_FIRST_TIME, false)

        binding.apply {
            viewPager.adapter = PagerOnboardingAdapter(supportFragmentManager)
            viewPager.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

            //dots indicator adalah titik untuk menampilkan posisi slide image
            dotsIndicator.setViewPager(viewPager)

            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                }

                override fun onPageSelected(position: Int) {
                    //menentukan text tergantuk pada posisi slide image
                    if (position == 0){
                        btnBack.visibility = View.GONE
                    } else {
                        btnBack.visibility = View.VISIBLE
                    }
                    page = position
                }

            })

            btnNext.setOnClickListener {
                if (page < 2){
                    view_pager.currentItem = page + 1
                } else {
                    val intent = Intent(this@OnboardActivity, LoginActivity::class.java)
                    startActivity(intent)
                    this@OnboardActivity.finish()
                }
            }

            btnBack.setOnClickListener {
                if (page > 0){
                    view_pager.currentItem = page - 1
                }
            }

            btnSkip.setOnClickListener {
                val intent = Intent(this@OnboardActivity, LoginActivity::class.java)
                startActivity(intent)
                this@OnboardActivity.finish()
            }
        }
    }
}