package dev.pawcat.ui.onboard.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.pawcat.R
import dev.pawcat.databinding.FragmentOnboard1Binding
import dev.pawcat.utils.FragmentBinding
import dev.pawcat.utils.setImageviewLocal

class OnboardFragment1 : Fragment() {

    private val binding by FragmentBinding<FragmentOnboard1Binding>(R.layout.fragment_onboard1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.imgOnboard.setImageviewLocal(requireContext(), R.drawable.ic_onboard_1)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            OnboardFragment1().apply {
                arguments = Bundle().apply {

                }
            }
    }
}