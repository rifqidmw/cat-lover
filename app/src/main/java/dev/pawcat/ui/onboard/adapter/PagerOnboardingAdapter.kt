package dev.pawcat.ui.onboard.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import dev.pawcat.ui.onboard.fragment.OnboardFragment1
import dev.pawcat.ui.onboard.fragment.OnboardFragment2
import dev.pawcat.ui.onboard.fragment.OnboardFragment3

class PagerOnboardingAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return OnboardFragment1()
            1 -> return OnboardFragment2()
            2 -> return OnboardFragment3()
            else -> return null!!
        }
    }

    override fun getCount(): Int {
        return 3
    }
}
