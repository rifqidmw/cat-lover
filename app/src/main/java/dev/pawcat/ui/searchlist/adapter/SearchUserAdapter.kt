package dev.pawcat.ui.searchlist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.databinding.ItemFollowBinding
import dev.pawcat.ui.inputpet.adapter.ProvinsiAdapter
import dev.pawcat.ui.searchlist.UserData
import dev.pawcat.utils.USER_UUID
import dev.pawcat.utils.intentToUser
import dev.pawcat.utils.setPhotoCircularUrl

class SearchUserAdapter(
    private val context: Context,
    private val session: Session
) : PagingDataAdapter<UserData.UserItem, SearchUserAdapter.FollowViewHolder>(DIIF_UTIL) {

    open inner class FollowViewHolder(
        val binding: ItemFollowBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(followModel: UserData.UserItem?){
            val data = followModel?.userModel
            var follow = data?.statusFollowing

            binding.apply {
                imgProfile.setPhotoCircularUrl(context, data?.userAvatar?:"")
                tvName.text = data?.usersName
                tvEmail.text = data?.usersEmail

                if (data?.uuid == session[USER_UUID]){
                    btnFollow.visibility = View.GONE
                }

                when(follow){
                    "0" -> {
                        btnFollow.text = "Follow"
                        btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_filled)
                    }

                    "1" -> {
                        btnFollow.text = "Unfollow"
                        btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_red_filled)
                    }
                }

                btnFollow.setOnClickListener {
                    when(follow){
                        "0" -> {
                            follow = "1"
                            btnFollow.text = "Unfollow"
                            btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_red_filled)
                        }

                        "1" -> {
                            follow = "0"
                            btnFollow.text = "Follow"
                            btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_filled)
                        }
                    }
                    if (mListener != null) mListener!!.onItemClick(
                        data!!
                    )
                }

                root.setOnClickListener {
                    val userUUID = data?.uuid
                    if (userUUID != session[USER_UUID]){
                        context.intentToUser(userUUID)
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: FollowViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FollowViewHolder {
        val view = LayoutInflater.from(parent.context)

        return FollowViewHolder(
            ItemFollowBinding.inflate(view, parent, false)
        )
    }

    interface OnItemClickListener {
        fun onItemClick(data: UserListResponse.User)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        mListener = listener
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<UserData.UserItem>(){
            override fun areItemsTheSame(oldItem: UserData.UserItem, newItem: UserData.UserItem): Boolean {
                return oldItem.userModel.uuid == newItem.userModel.uuid
            }

            override fun areContentsTheSame(oldItem: UserData.UserItem, newItem: UserData.UserItem): Boolean {
                return oldItem == newItem
            }

        }

        private var mListener: OnItemClickListener? = null
    }
}