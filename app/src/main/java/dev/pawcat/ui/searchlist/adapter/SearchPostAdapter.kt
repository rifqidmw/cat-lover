package dev.pawcat.ui.searchlist.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.databinding.ItemPostBinding
import dev.pawcat.ui.detailpost.DetailPostActivity
import dev.pawcat.ui.searchlist.PostData
import dev.pawcat.utils.*

class SearchPostAdapter(
    private val context: Context,
    private val session: Session
    ) : PagingDataAdapter<PostData.PostItem, SearchPostAdapter.PostViewHolder>(DIIF_UTIL){

    open inner class PostViewHolder (val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(postModel: PostData.PostItem){
            val data = postModel.postModel
            var likeStatus = data.statusLike

            binding.apply {

                imgPost.setImageviewUrl(context, data.image?:"")
                imgProfile.setImageviewUrl(context, data.usersAvatar?:"")
                tvUsername.text = data.usersName
                tvDesc.text = data.content
                tvLiked.text = data.like
                tvComment.text = data.comment
                tvDate.text = data.createdAt!!.changeDateFormat().setTimeAgo()

                tvComment.setOnClickListener {
                    val intent = Intent(context, DetailPostActivity::class.java)
                    intent.putExtra(DATA_POST, data)
                    context.startActivity(intent)
                }

                btnComment.setOnClickListener {
                    val intent = Intent(context, DetailPostActivity::class.java)
                    intent.putExtra(DATA_POST, data)
                    context.startActivity(intent)
                }

                imgProfile.setOnClickListener {
                    val userUUID = data.usersUUID
                    if (userUUID != session[USER_UUID]){
                        context.intentToUser(userUUID)
                    }
                }

                btnShare.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(data.urlShare)
                    context.startActivity(intent)
                }

                when(likeStatus){
                    "0" -> {
                        btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_inactive))
                    }

                    "1" -> {
                        btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_active))
                    }
                }

                btnLike.setOnClickListener {
                    when(likeStatus){
                        "0" -> {
                            likeStatus = "1"
                            btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_active))
                            tvLiked.text = (tvLiked.text.toString().toInt() + 1).toString()
                        }

                        "1" -> {
                            likeStatus = "0"
                            btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_inactive))
                            tvLiked.text = (tvLiked.text.toString().toInt() - 1).toString()
                        }
                    }
                    if (mListener != null) mListener!!.onItemClick(
                        data
                    )
                }
            }

        }
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context)

        return PostViewHolder(
            ItemPostBinding.inflate(view, parent, false)
        )
    }

    interface OnItemClickListener {
        fun onItemClick(data: PostListResponse.Post)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        mListener = listener
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<PostData.PostItem>(){
            override fun areItemsTheSame(oldItem: PostData.PostItem, newItem: PostData.PostItem): Boolean {
                return oldItem.postModel.uuid == newItem.postModel.uuid
            }

            override fun areContentsTheSame(oldItem: PostData.PostItem, newItem: PostData.PostItem): Boolean {
                return oldItem == newItem
            }

        }

        private var mListener: OnItemClickListener? = null
    }
}