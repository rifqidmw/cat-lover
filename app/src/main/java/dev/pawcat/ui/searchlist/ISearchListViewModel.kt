package dev.pawcat.ui.searchlist

import dev.pawcat.data.remote.response.base.BaseResponse

interface ISearchListViewModel {
    fun successFollow(baseResponse: BaseResponse)
    fun successLike(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
}