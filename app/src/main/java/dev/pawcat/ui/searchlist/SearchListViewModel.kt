package dev.pawcat.ui.searchlist

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.PostRepo
import dev.pawcat.data.repo.paged.*
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class SearchListViewModel(
    private val authRepo: AuthRepo,
    private val postRepo: PostRepo,
    private val pagedListRepo: PagedListRepo, private val context: Context
) : RxViewModel<SearchListState>(), ISearchListViewModel{
    override val TAG: String
        get() = SearchListState::class.java.simpleName

    override fun successFollow(baseResponse: BaseResponse) {
        logD(TAG,"success do follow")
        state.value = SearchListState.OnSuccessFollow(baseResponse)
    }

    override fun successLike(baseResponse: BaseResponse) {
        logD(TAG,"success do like")
        state.value = SearchListState.OnSuccessLike(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = SearchListState.OnErrorState(t)
    }

    fun userList(title: String): Flow<PagingData<UserData.UserItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            SearchUserListPagingSource(headers = headers, pagedListRepo = pagedListRepo, title = title, type = "1")
        }.flow.map { pagingData -> pagingData.map { UserData.UserItem(it) } }
    }

    fun postList(title: String): Flow<PagingData<PostData.PostItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            SearchPostListPagingSource(headers = headers, pagedListRepo = pagedListRepo, title = title, type = "2")
        }.flow.map { pagingData -> pagingData.map { PostData.PostItem(it) } }
    }

    fun petList(title: String): Flow<PagingData<CatData.CatItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            SearchPetListPagingSource(headers = headers, pagedListRepo = pagedListRepo, title = title, type = "3")
        }.flow.map { pagingData -> pagingData.map { CatData.CatItem(it) } }
    }

    fun doFollow(header:Map<String, String>, uuid: String) {
        logD(TAG,"do follow")
        launch {
            authRepo.doFollow(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successFollow) {
                    errorLoad(t = it)
                }
        }
    }

    fun doLike(header:Map<String, String>, uuid: String) {
        logD(TAG,"do like")
        launch {
            postRepo.doLike(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLike) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class UserData{
    data class UserItem(val userModel: UserListResponse.User) : UserData()
}

sealed class CatData{
    data class CatItem(val catModel: CatListResponse.Cat) : CatData()
}

sealed class PostData{
    data class PostItem(val postModel: PostListResponse.Post) : PostData()
}