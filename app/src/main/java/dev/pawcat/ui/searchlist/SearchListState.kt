package dev.pawcat.ui.searchlist

import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.ui.main.home.HomeState

sealed class SearchListState {
    data class OnSuccessFollow(val baseResponse: BaseResponse) : SearchListState()
    data class OnSuccessLike(val baseResponse: BaseResponse) : SearchListState()
    data class OnErrorState(val t: Throwable) : SearchListState()
}