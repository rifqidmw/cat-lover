package dev.pawcat.ui.searchlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.databinding.ActivitySearchListBinding
import dev.pawcat.ui.searchlist.adapter.SearchPetAdapter
import dev.pawcat.ui.searchlist.adapter.SearchPostAdapter
import dev.pawcat.ui.searchlist.adapter.SearchUserAdapter
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logE
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SearchListActivity : AppCompatActivity(), Observer<SearchListState> {
    private val TAG = SearchListActivity::class.java.simpleName
    private val binding: ActivitySearchListBinding by lazy {
        DataBindingUtil.setContentView<ActivitySearchListBinding>(
            this, R.layout.activity_search_list)
    }
    private val mViewModel: SearchListViewModel by inject()
    private val session: Session by inject()
    private val searchUserAdapter: SearchUserAdapter by lazy { SearchUserAdapter(this@SearchListActivity, session) }
    private val searchPostAdapter: SearchPostAdapter by lazy { SearchPostAdapter(this@SearchListActivity, session) }
    private val searchPetAdapter: SearchPetAdapter by lazy { SearchPetAdapter(this@SearchListActivity) }

    private var searchQuery: String = ""
    private var tabSelected: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@SearchListActivity)

        setupViews()
        setupUserView()
    }

    private fun setupViews(){
        binding.apply {

            tabLayout.addTab(tabLayout.newTab().setText("User"))
            tabLayout.addTab(tabLayout.newTab().setText("Post"))
            tabLayout.addTab(tabLayout.newTab().setText("Pet"))

            tabLayout.getTabAt(0)

            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    when(tabLayout.selectedTabPosition){
                        0 -> {
                            tabSelected = 0
                            setupUserView()
                        }

                        1 -> {
                            tabSelected = 1

                            setupPostView()
                        }

                        2 -> {
                            tabSelected = 2

                            setupPetView()
                        }
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })

            navBack.setOnClickListener {
                onBackPressed()
                this@SearchListActivity.finish()
            }
            setupSearchView()
        }
    }

    private fun setupUserView(){
        binding.apply {
            loadDataUserSearch(searchQuery, true)
            rvSearch.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@SearchListActivity, RecyclerView.VERTICAL, false)
                adapter = searchUserAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ searchUserAdapter.retry() }
                )
            }

            searchUserAdapter.setOnItemClickListener(object : SearchUserAdapter.OnItemClickListener{
                override fun onItemClick(data: UserListResponse.User) {
                    mViewModel.doFollow(getParamHeader(), data.uuid?:"")
                }

            })
        }
    }

    private fun setupPostView(){
        binding.apply {
            loadDataPostSearch(searchQuery, true)
            rvSearch.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@SearchListActivity, RecyclerView.VERTICAL, false)
                adapter = searchPostAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ searchPostAdapter.retry() }
                )
            }

            searchPostAdapter.setOnItemClickListener(object : SearchPostAdapter.OnItemClickListener{

                override fun onItemClick(data: PostListResponse.Post) {
                    mViewModel.doLike(getParamHeader(), data.uuid?:"")
                }

            })
        }
    }

    private fun setupPetView(){
        binding.apply {
            loadDataPetSearch(searchQuery, true)
            rvSearch.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@SearchListActivity, RecyclerView.VERTICAL, false)
                adapter = searchPetAdapter.withLoadStateFooter(
                    footer = FooterLoadingAdapter{ searchPetAdapter.retry() }
                )
            }
        }
    }

    private fun setupSearchView(){
        binding.apply {

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    searchQuery = query?:""
                    when(tabSelected){
                        0 -> {
                            loadDataUserSearch(searchQuery, false)
                        }
                        
                        1 -> {
                            loadDataPostSearch(searchQuery, false)
                        }
                        
                        2 -> {
                            loadDataPetSearch(searchQuery, false)
                        }
                    }
                    
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText.isNullOrEmpty()){
                        searchQuery = ""
                        when(tabSelected){
                            0 -> {
                                loadDataUserSearch(searchQuery, false)
                            }

                            1 -> {
                                loadDataPostSearch(searchQuery, false)
                            }

                            2 -> {
                                loadDataPetSearch(searchQuery, false)
                            }
                        }
                    }

                    return false
                }

            })

            val clearButton = searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn)
            clearButton.setOnClickListener {
                searchView.setQuery("", false)
                when(tabSelected){
                    0 -> {
                        loadDataUserSearch("", false)
                    }

                    1 -> {
                        loadDataPostSearch("", false)
                    }

                    2 -> {
                        loadDataPetSearch("", false)
                    }
                }
            }
        }
    }

    private fun loadDataUserSearch(query: String, withLoading: Boolean){
        lifecycleScope.launch {
            mViewModel.userList(query).collectLatest {
                searchUserAdapter.submitData(it)
            }
        }

        binding.apply {
            searchUserAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if (withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        val msg = it?.error?.localizedMessage?:""
                        if (msg.isNotEmpty()){
                            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
                            snack.setAction("Retry", View.OnClickListener {
                                searchUserAdapter.retry()
                            })
                            snack.show()
                        }

                    }
                }
            }
        }
    }

    private fun loadDataPostSearch(query: String, withLoading: Boolean){
        lifecycleScope.launch {
            mViewModel.postList(query).collectLatest {
                searchPostAdapter.submitData(it)
            }
        }

        binding.apply {
            searchPostAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if (withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        val msg = it?.error?.localizedMessage?:""
                        if (msg.isNotEmpty()){
                            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
                            snack.setAction("Retry", View.OnClickListener {
                                searchPostAdapter.retry()
                            })
                            snack.show()
                        }
                    }
                }
            }
        }
    }

    private fun loadDataPetSearch(query: String, withLoading: Boolean){
        lifecycleScope.launch {
            mViewModel.petList(query).collectLatest {
                searchPetAdapter.submitData(it)
            }
        }

        binding.apply {
            searchPetAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if (withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState.let {
                        val msg = it?.error?.localizedMessage?:""
                        if (msg.isNotEmpty()){
                            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
                            snack.setAction("Retry", View.OnClickListener {
                                searchPetAdapter.retry()
                            })
                            snack.show()
                        }
                    }
                }
            }
        }
    }


    override fun onChanged(state: SearchListState?) {
        when(state){
            is SearchListState.OnSuccessFollow -> {

            }

            is SearchListState.OnSuccessLike -> {

            }

            is SearchListState.OnErrorState ->{
                val msg = state.t.localizedMessage
                logE(TAG, msg)
            }
        }
    }
}