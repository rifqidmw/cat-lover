package dev.pawcat.ui

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.asura.library.posters.*
import dev.pawcat.R
import dev.pawcat.databinding.ActivityTestBinding
import dev.pawcat.ui.main.mypet.pet.CatListViewModel
import dev.pawcat.ui.main.mypet.pet.adapter.PetAdapter
import org.koin.android.ext.android.inject

class TestActivity : AppCompatActivity() {

    private val binding: ActivityTestBinding by lazy {
        DataBindingUtil.setContentView<ActivityTestBinding>(
            this, R.layout.activity_test
        )
    }

    private val catAdapter by lazy { PetAdapter(this@TestActivity) }
    private val viewModel: CatListViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val url1 = Uri.parse("http://techslides.com/demos/sample-videos/small.mp4")
        val url2 = Uri.parse("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")

        val list: ArrayList<Poster> = ArrayList()
        list.add(RemoteImage("https://wallpapercave.com/wp/wp4326841.jpg"))
        list.add(RemoteVideo(url1))
        list.add(RemoteImage("https://cdn.statically.io/img/www.tokkoro.com/picsup/2636113-anime-girl-4k-wallpaper-backgrounds-hd.jpg"))
        list.add(RemoteVideo(url2))

        binding.apply {
            posterSlider.setPosters(list)
        }
    }
}