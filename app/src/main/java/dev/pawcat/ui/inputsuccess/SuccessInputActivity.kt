package dev.pawcat.ui.inputsuccess

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import dev.pawcat.R
import dev.pawcat.databinding.ActivitySuccessInputBinding
import dev.pawcat.utils.*

class SuccessInputActivity : AppCompatActivity() {

    private val binding: ActivitySuccessInputBinding by lazy {
        DataBindingUtil.setContentView<ActivitySuccessInputBinding>(
            this, R.layout.activity_success_input
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupIntent()
    }

    private fun setupIntent(){
        intent.apply {
            val type = getStringExtra(INPUT_SUCCESS)

            setupViews(type?:"")
        }
    }

    private fun setupViews(type: String){
        binding.apply {
            when(type){
                INPUT_SUCCESS_POST -> {
                    tvTitle.text = "You Are a Good Cat Owner..."
                    tvDesc.text = "Your daily activity has been added successfully."

                    btnBack.text = "SEE YOUR POST"
                }

                INPUT_SUCCESS_CAT -> {
                    tvTitle.text = "Horree... You Did It."
                    tvDesc.text = "Your lovely cat has been added successfully."

                    btnBack.text = "SEE YOUR CAT"
                }

                INPUT_SUCCESS_UPDATE_CAT -> {
                    tvTitle.text = "Horree... You Did It."
                    tvDesc.text = "Your lovely cat has been update successfully."

                    btnBack.text = "SEE YOUR CAT"
                }

                INPUT_SUCCESS_REMINDER -> {
                    tvTitle.text = "Awesome..."
                    tvDesc.text = "Your reminder for your cat has been added successfully."

                    btnBack.text = "SEE YOUR REMINDER"
                }

                INPUT_SUCCESS_UPDATE_REMINDER -> {
                    tvTitle.text = "Awesome..."
                    tvDesc.text = "Your reminder for your cat has been update successfully."

                    btnBack.text = "SEE YOUR REMINDER"
                }
            }

            btnBack.setOnClickListener {
                onBackPressed()
                this@SuccessInputActivity.finish()
            }
        }
    }
}