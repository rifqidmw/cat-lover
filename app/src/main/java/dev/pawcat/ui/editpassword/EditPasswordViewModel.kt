package dev.pawcat.ui.editpassword

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.UpdatePasswordResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditPasswordViewModel(
    val authRepo: AuthRepo
) : RxViewModel<EditPasswordState>(), IEditPasswordViewModel {
    override val TAG: String
        get() = EditPasswordState::class.java.simpleName

    override fun successUpdatePassword(updatePasswordResponse: UpdatePasswordResponse) {
        logD(TAG,"success do update password")
        onLoading(false)
        state.value = EditPasswordState.OnSuccessUpdatePassword(updatePasswordResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = EditPasswordState.OnErrorState(t)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = EditPasswordState.OnLoading(isLoading)
    }

    fun doUpdatePassword(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do register")
        launch {
            onLoading(true)
            authRepo.doUpdatePassword(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successUpdatePassword) {
                    errorLoad(t = it)
                }
        }
    }
}