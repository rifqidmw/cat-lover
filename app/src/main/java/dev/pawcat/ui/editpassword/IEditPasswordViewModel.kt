package dev.pawcat.ui.editpassword

import dev.pawcat.data.remote.response.UpdatePasswordResponse


interface IEditPasswordViewModel {
    fun successUpdatePassword(updatePasswordResponse: UpdatePasswordResponse)
    fun errorLoad(t: Throwable)
    fun onLoading(isLoading: Boolean)
}