package dev.pawcat.ui.editpassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.databinding.ActivityEditPasswordBinding
import dev.pawcat.utils.USER_TOKEN
import dev.pawcat.utils.getString
import dev.pawcat.utils.toast
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class EditPasswordActivity : AppCompatActivity(), Observer<EditPasswordState> {

    private val binding: ActivityEditPasswordBinding by lazy {
        DataBindingUtil.setContentView<ActivityEditPasswordBinding>(
            this, R.layout.activity_edit_password
        )
    }

    private val mViewModel: EditPasswordViewModel by inject()
    private val session: Session by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@EditPasswordActivity)

        setupViews()
    }

    private fun setupViews(){
        binding.apply {
            btnUpdate.setOnClickListener {
                doUpdatePassword()
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@EditPasswordActivity.finish()
            }
        }
    }

    private fun doUpdatePassword(){
        binding.apply {
            val oldPassword = etOldPassword.getString()
            val newPassword = etNewPassword.getString()
            val confirmNewPassword = etConfirmNewPassword.getString()

            if (oldPassword.isNotEmpty() && newPassword.isNotEmpty() &&
                confirmNewPassword.isNotEmpty()){

                if (newPassword == confirmNewPassword){
                    removeErrorStateEdittext()

                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                    val jsonObject = JsonObject()
                    try {
                        jsonObject.addProperty("old_password", oldPassword)
                        jsonObject.addProperty("new_password", newPassword)
                        jsonObject.addProperty("new_confirm_password", confirmNewPassword)

                        mViewModel.doUpdatePassword(headers, jsonObject)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    etConfirmNewPassword.setText("")
                    etConfirmNewPassword.error = "Re-Password must be same with Password"
                }
            } else {
                if (oldPassword.isEmpty())
                    etOldPassword.error = "Must be fill"
                else
                    etLayoutOldPassword.isErrorEnabled = false
                if (newPassword.isEmpty())
                    etNewPassword.error = "Must be fill"
                else
                    etLayoutNewPassword.isErrorEnabled = false
                if (confirmNewPassword.isEmpty())
                    etConfirmNewPassword.error = "Must be fill"
                else
                    etLayoutConfirmNewPassword.isErrorEnabled = false
            }
        }
    }

    private fun removeErrorStateEdittext(){
        binding.apply {
            etLayoutOldPassword.isErrorEnabled = false
            etLayoutNewPassword.isErrorEnabled = false
            etLayoutConfirmNewPassword.isErrorEnabled = false
        }
    }

    private fun clearEdittext(){
        binding.apply {
            etOldPassword.setText("")
            etNewPassword.setText("")
            etConfirmNewPassword.setText("")
        }
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(title: String){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)
        tvTitle.text = title
    }

    override fun onChanged(state: EditPasswordState?) {
        when(state){
            is EditPasswordState.OnSuccessUpdatePassword -> {
                val status = state.updatePasswordResponse.code?:""
                val msg = state.updatePasswordResponse.message?:""

                if (status == "200"){
                    toast("Update password successfull!")
                    clearEdittext()
                }
            }

            is EditPasswordState.OnLoading -> {
                val loading = state.isLoading

                if (loading){
                    showDialogLoading("Update password...")
                } else {
                    dialogLoading.dismiss()
                }
            }

            is EditPasswordState.OnErrorState -> {
                val msg = state.t.localizedMessage?:""
                toast(msg)
            }
        }
    }
}