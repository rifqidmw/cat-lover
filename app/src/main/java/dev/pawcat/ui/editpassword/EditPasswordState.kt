package dev.pawcat.ui.editpassword

import dev.pawcat.data.remote.response.UpdatePasswordResponse


sealed class EditPasswordState {
    data class OnSuccessUpdatePassword(val updatePasswordResponse: UpdatePasswordResponse) : EditPasswordState()
    data class OnErrorState(val t: Throwable) : EditPasswordState()
    data class OnLoading(val isLoading: Boolean) : EditPasswordState()
}