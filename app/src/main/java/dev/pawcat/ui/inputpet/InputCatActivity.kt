package dev.pawcat.ui.inputpet

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.core.graphics.scale
import androidx.core.view.setPadding
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abedelazizshe.lightcompressorlibrary.CompressionListener
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor
import com.abedelazizshe.lightcompressorlibrary.VideoQuality
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.local.model.UploadModel
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.databinding.ActivityInputPetBinding
import dev.pawcat.ui.detailcat.DetailCatActivity
import dev.pawcat.ui.inputpet.adapter.KotaAdapter
import dev.pawcat.ui.inputpet.adapter.PetTypeAdapter
import dev.pawcat.ui.inputpet.adapter.ProvinsiAdapter
import dev.pawcat.ui.inputsuccess.SuccessInputActivity
import dev.pawcat.utils.*
import dev.pawcat.utils.view.ImagePickerActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.io.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class InputCatActivity : AppCompatActivity(), Observer<InputCatState> {
    private val TAG = InputCatActivity::class.java.simpleName
    private val REQUEST_IMAGE = 101
    private val REQUEST_VIDEO = 102

    private val binding: ActivityInputPetBinding by lazy {
        DataBindingUtil.setContentView<ActivityInputPetBinding>(
            this, R.layout.activity_input_pet
        )
    }
    private val mViewModel: InputCatViewModel by inject()
    private val session: Session by inject()

    private val fileList: ArrayList<UploadModel> = ArrayList()
    private var selectedImage: Int? = null
    private var videoPath: String = ""
    private var catUUID: String = ""
    private var idProvince: String = ""
    private var idCity: String = ""
    private var idPetType: String = ""
    private var inputType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@InputCatActivity)

        setupIntent()
    }

    private fun setupIntent(){
        intent.apply {
            inputType = getStringExtra(INPUT_TYPE)?:""

            when (inputType){
                INPUT_NEW -> {
                    setupViews()
                }

                INPUT_UPDATE -> {
                    setupViews()
                    setData()
                }
            }
        }
    }

    private fun setupViews(){
        binding.apply {
            btnAddImage1.setOnClickListener {
                onImageClick()
                selectedImage = 1
            }

            btnAddImage2.setOnClickListener {
                onImageClick()
                selectedImage = 2
            }

            btnAddImage3.setOnClickListener {
                onImageClick()
                selectedImage = 3
            }

            btnAddVideo.setOnClickListener {
                setupPermissionsCameraView({
                    val intent = Intent()
                    intent.type = "video/*"
                    intent.action = Intent.ACTION_PICK
                    startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_VIDEO)
                },{

                })

            }
            
            etTypePet.setOnClickListener { 
                showPetTypeDialog()
            }
            
            etProvince.setOnClickListener { 
                showProvinsiDialog()
            }
            
            etCity.setOnClickListener { 
                if (idProvince.isEmpty()){
                    toast("Please choose province first!!")
                } else {
                    showKotaDialog(idProvince)
                }
            }

            btnAddCat.setOnClickListener {
                when (inputType){
                    INPUT_NEW -> {
                        doAddCat()
                    }

                    INPUT_UPDATE -> {
                        doUpdateCat()
                    }
                }
            }

            etAge.setOnClickListener {
                showDatePickerDialog {
                    etAge.setText(it)
                }
            }

            navBack.setOnClickListener {
                onBackPressed()
                this@InputCatActivity.finish()
            }

            executePendingBindings()
        }
    }

    private fun setData(){
        intent.apply {
            val data = getSerializableExtra(DATA_CAT) as CatListResponse.Cat

            binding.apply {
                lyVideo.visibility = View.GONE
                tvVideo.visibility = View.GONE
                lyImage.visibility = View.GONE
                tvImage.visibility = View.GONE

                catUUID = data.uuid?:""
                idProvince = data.idProvinsi?:""
                idCity = data.idKota?:""
                idPetType = data.jenisPet?:""
                etName.setText(data.name)
                etTypePet.setText(data.jenisPetText)
                etBreed.setText(data.breed)
                setGender(data.gender)
                etAge.setText(data.age)
                etWeight.setText(data.weight)
                etIntroduction.setText(data.content)
                etProvince.setText(data.provinsi)
                etCity.setText(data.kota)
                setSwitchAdoption(data.available)

                tvToolbarTitle.text = "Update Cat"
                btnAddCat.text = "UPDATE"

                executePendingBindings()
            }
        }
    }

    private fun doAddCat(){
        binding.apply {
            val name = etName.getString()
            val content = etIntroduction.getString()
            val breed = etBreed.getString()
            val gender = getGender()
            val age = etAge.getString()
            val weight = etWeight.getString()
            val province = idProvince
            val city = idCity
            val petType = idPetType
            val available = getSwitchAdoption()
            val mainImage = fileList.singleOrNull {
                it.id == 1
            }

            if (name.isNotEmpty() && content.isNotEmpty() &&
                breed.isNotEmpty() && gender.isNotEmpty() &&
                age.isNotEmpty() && weight.isNotEmpty() &&
                province.isNotEmpty() && city.isNotEmpty() &&
                petType.isNotEmpty() && available.isNotEmpty() && 
                mainImage != null){

                removeErrorStateEdittext()

                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val filePath = fileList.single {
                    it.id == 1
                }

                mViewModel.doAddCat(
                    header = headers,
                    filePath = filePath.fileUrl,
                    name = name,
                    content = content,
                    breed = breed,
                    gender = gender,
                    age = age,
                    weight = weight,
                    available = available,
                    petType = petType,
                    idProvince = province,
                    idCity = city
                )
            } else {
                if (content.isEmpty()){
                    etIntroduction.error = "Must be fill"
                }

                if (name.isEmpty()){
                    etName.error = "Must be fill"
                }

                if (breed.isEmpty()){
                    etBreed.error = "Must be fill"
                }

                if (age.isEmpty()){
                    etAge.error = "Must be fill"
                }

                if (weight.isEmpty()){
                    etWeight.error = "Must be fill"
                }
                
                if (idProvince.isEmpty()){
                    etProvince.error = "Must be fill"
                }
                
                if (idCity.isEmpty()){
                    etCity.error = "Must be fill"
                }
                
                if (idPetType.isEmpty()){
                    etTypePet.error = "Must be fill"
                }

                if (mainImage == null){
                    showSnackbar("Please choose Main Photo first")
                }
                /*if (imageFile == null){
                    etImage.error = "Must be fill"
                    toast("Image file not found")
                }*/
            }

            executePendingBindings()
        }
    }

    private fun doUpdateCat(){
        binding.apply {
            val name = etName.getString()
            val content = etIntroduction.getString()
            val breed = etBreed.getString()
            val gender = getGender()
            val age = etAge.getString()
            val weight = etWeight.getString()
            val petType = idPetType
            val province = idProvince
            val city = idCity
            val available = getSwitchAdoption()

            if (name.isNotEmpty() && content.isNotEmpty() &&
                breed.isNotEmpty() && gender.isNotEmpty() &&
                age.isNotEmpty() && weight.isNotEmpty() &&
                petType.isNotEmpty() && province.isNotEmpty() && city.isNotEmpty() &&
                available.isNotEmpty() && catUUID.isNotEmpty()){

                removeErrorStateEdittext()

                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("name", name)
                    jsonObject.addProperty("content", content)
                    jsonObject.addProperty("breed", breed)
                    jsonObject.addProperty("gender", gender)
                    jsonObject.addProperty("age", age)
                    jsonObject.addProperty("weight", weight)
                    jsonObject.addProperty("available", available)
                    jsonObject.addProperty("jenis_pet", petType)
                    jsonObject.addProperty("id_provinsi", province)
                    jsonObject.addProperty("id_kota", city)
                    jsonObject.addProperty("uuid", catUUID)

                    mViewModel.doUpdateCat(headers, jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                if (content.isEmpty()){
                    etIntroduction.error = "Must be fill"
                }

                if (name.isEmpty()){
                    etName.error = "Must be fill"
                }

                if (breed.isEmpty()){
                    etBreed.error = "Must be fill"
                }

                if (age.isEmpty()){
                    etAge.error = "Must be fill"
                }

                if (weight.isEmpty()){
                    etWeight.error = "Must be fill"
                }

                if (petType.isEmpty()){
                    etTypePet.error = "Must be fill"
                }

                if (province.isEmpty()){
                    etProvince.error = "Must be fill"
                }

                if (city.isEmpty()){
                    etCity.error = "Must be fill"
                }

                if (catUUID.isEmpty()){
                    toast("Error IDx101009")
                    logE(TAG, "reminder uuid is empty")
                }
            }

            executePendingBindings()
        }
    }

    private fun removeErrorStateEdittext(){
        binding.apply {
            etLayoutName.isErrorEnabled = false
            etLayoutIntroduction.isErrorEnabled = false
            etLayoutBreed.isErrorEnabled = false
            etLayoutAge.isErrorEnabled = false
            etLayoutWeight.isErrorEnabled = false

            executePendingBindings()
        }
    }

    private fun clearEdittext(){
        binding.apply {
            etName.setText("")
            etIntroduction.setText("")
            etVideo.setText("")
            etBreed.setText("")
            etAge.setText("")
            etWeight.setText("")
            switchAdoption.isChecked = false

            for (i in fileList.indices){
                val file = File(fileList[i].fileUrl)
                if (file.exists()){
                    file.delete()
                }
            }
            executePendingBindings()
        }
    }

    private fun getGender(): String{
        binding.apply {
            val genderId = radioGender.checkedRadioButtonId
            val genderString = resources.getResourceEntryName(genderId)
            val gender = if (genderString == "rbMale"){
                "1"
            } else {
                "2"
            }

            executePendingBindings()

            return gender
        }
    }

    private fun setGender(gender: String?){
        binding.apply {
            when (gender){
                "1" -> {
                    rbMale.isChecked = true
                }
                "2" -> {
                    rbFemale.isChecked = true
                }

                else -> {
                    rbMale.isChecked = true
                }
            }

            executePendingBindings()
        }
    }

    private fun getSwitchAdoption(): String{
        binding.apply {
            val adoption = if (switchAdoption.isChecked){
                "1"
            } else {
                "0"
            }

            executePendingBindings()

            return adoption
        }
    }

    private fun setSwitchAdoption(available: String?){
        binding.apply {
            when(available){
                "0" -> {
                    switchAdoption.isChecked = false
                }

                "1" -> {
                    switchAdoption.isChecked = true
                }

                else -> {
                    switchAdoption.isChecked = false
                }
            }

            executePendingBindings()
        }
    }

    private fun showSnackbar(msg: String){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(binding.root, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Dismiss", View.OnClickListener {
                snack.dismiss()
            })
            snack.show()
        }
    }

    private fun onImageClick() {
        setupPermissionsCameraView({
            showImagePickerOptions()
        }, {
            showSettingsDialog()
        })
    }

    @SuppressLint("NewApi")
    private fun loadImage(url: String, uri: Uri){

        val baos = ByteArrayOutputStream()
        val bitmap = when {
            Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
                this.contentResolver,
                uri
            )
            else -> {
                val source = ImageDecoder.createSource(this.contentResolver, uri)
                ImageDecoder.decodeBitmap(source)
            }
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        bitmap.scale(700, 700)

        //convert from bitmap to bytearray
        val imageBytes = baos.toByteArray()

        val imageFile = saveByteArrayImageToLocal(imageBytes, session[USER_EMAIL] ?: "")

        when(selectedImage){
            1 -> {
                fileList.removeIf { filter ->
                    filter.id == 1
                }
                setImage(imageFile.absolutePath)
                fileList.add(UploadModel(1, imageFile.absolutePath, "image"))
            }

            2 -> {
                fileList.removeIf { filter ->
                    filter.id == 2
                }
                setImage(imageFile.absolutePath)
                fileList.add(UploadModel(2, imageFile.absolutePath, "image"))
            }

            3 -> {
                fileList.removeIf { filter ->
                    filter.id == 3
                }
                setImage(imageFile.absolutePath)
                fileList.add(UploadModel(3, imageFile.absolutePath, "image"))
            }
        }
    }

    private fun setImage(imagePath: String){
        if (selectedImage == 1){
            binding.btnAddImage1.setImageviewUrl(this@InputCatActivity, imagePath)
            binding.btnAddImage1.setPadding(0)
        } else if (selectedImage == 2){
            binding.btnAddImage2.setImageviewUrl(this@InputCatActivity, imagePath)
            binding.btnAddImage2.setPadding(0)
        } else if (selectedImage == 3){
            binding.btnAddImage3.setImageviewUrl(this@InputCatActivity, imagePath)
            binding.btnAddImage3.setPadding(0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uri = data!!.getParcelableExtra<Uri>("path")
                    try {
                        loadImage(uri!!.toString(), uri)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }

            REQUEST_VIDEO -> {
                if (resultCode == Activity.RESULT_OK){
                    if (data?.data != null) {
                        val uriPathHelper = URIPathHelper()
                        val videoFullPath = uriPathHelper.getPath(this, data.data!!)

                        if (videoFullPath != null) {
                            processVideo(data.data)
                        } else {
                            toast("video not found")
                        }
                    }
                }
            }
        }
    }

    private fun processVideo(uri: Uri?) {
        uri?.let {

            GlobalScope.launch {
                val job = async { getMediaPath(applicationContext, uri) }
                videoPath = job.await()

                val desFile = saveVideoFile(videoPath)

                desFile?.let {
                    var time = 0L
                    VideoCompressor.start(
                        videoPath,
                        desFile.path,
                        object : CompressionListener {
                            override fun onProgress(percent: Float) {
                                //Update UI
                                if (percent <= 100 && percent.toInt() % 5 == 0)
                                    runOnUiThread {
                                        Log.d(TAG, "compress video: ${percent.toInt()}")
                                    }
                            }

                            override fun onStart() {
                                time = System.currentTimeMillis()
                                showDialogLoading("Compressing")
                            }

                            override fun onSuccess() {
                                val newSizeValue = desFile.length()

                                Log.d(TAG, "Size after compression: ${getFileSize(newSizeValue)}")

                                videoPath = desFile.path
                                dialogLoading.dismiss()
                                binding.etVideo.setText(videoPath)
                                fileList.add(UploadModel(4, videoPath, "video"))
                            }

                            override fun onFailure(failureMessage: String) {
                                Log.wtf("failureMessage", failureMessage)
                                Log.e("failureMessage", failureMessage)
                            }

                            override fun onCancelled() {
                                Log.wtf("TAG", "compression has been cancelled")
                                // make UI changes, cleanup, etc
                            }
                        },
                        VideoQuality.MEDIUM,
                        isMinBitRateEnabled = false,
                        keepOriginalResolution = false
                    )
                }
            }
        }
    }

    @Suppress("DEPRECATION")
    private fun saveVideoFile(filePath: String?): File? {
        filePath?.let {
            createVideoCacheDir()
            val videoFile = File(filePath)
            val videoFileName = "${System.currentTimeMillis()}_${videoFile.name}"
            val folderName = Environment.DIRECTORY_MOVIES
            if (Build.VERSION.SDK_INT >= 30) {

                val values = ContentValues().apply {

                    put(
                        MediaStore.Images.Media.DISPLAY_NAME,
                        videoFileName
                    )
                    put(MediaStore.Images.Media.MIME_TYPE, "video/mp4")
                    put(MediaStore.Images.Media.RELATIVE_PATH, folderName)
                    put(MediaStore.Images.Media.IS_PENDING, 1)
                }

                val collection =
                    MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)

                val fileUri = applicationContext.contentResolver.insert(collection, values)

                fileUri?.let {
                    application.contentResolver.openFileDescriptor(fileUri, "rw")
                        .use { descriptor ->
                            descriptor?.let {
                                FileOutputStream(descriptor.fileDescriptor).use { out ->
                                    FileInputStream(videoFile).use { inputStream ->
                                        val buf = ByteArray(4096)
                                        while (true) {
                                            val sz = inputStream.read(buf)
                                            if (sz <= 0) break
                                            out.write(buf, 0, sz)
                                        }
                                    }
                                }
                            }
                        }

                    values.clear()
                    values.put(MediaStore.Video.Media.IS_PENDING, 0)
                    applicationContext.contentResolver.update(fileUri, values, null, null)

                    return File(getMediaPath(applicationContext, fileUri))
                }
            } else {
                val downloadsPath =
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                val desFile = File(downloadsPath.absolutePath + "/pawcats/cache/video", videoFileName)

                if (desFile.exists())
                    desFile.delete()

                try {
                    desFile.createNewFile()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                return desFile
            }
        }
        return null
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object :
                ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@InputCatActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)

        startActivityForResult(
            intent,
            REQUEST_IMAGE
        )
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@InputCatActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(
            intent,
            REQUEST_IMAGE
        )
    }

    lateinit var dialogLoading: AlertDialog
    private fun showDialogLoading(title: String){

        val customView = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null, false)
        dialogLoading = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .create()

        if (title == "Compressing"){
            dialogLoading.setCancelable(false)
        } else {
            dialogLoading.setCancelable(true)
        }


        dialogLoading.show()
        val tvTitle = customView.findViewById<TextView>(R.id.tv_title)

        tvTitle.text = title
    }

    private fun showProvinsiDialog(){
        val customView = LayoutInflater.from(this@InputCatActivity).inflate(R.layout.dialog_location, null, false)
        val dialog = MaterialAlertDialogBuilder(this@InputCatActivity, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialog.show()
        val tvTitle = customView.findViewById<TextView>(R.id.dialog_title)
        val btnClose = customView.findViewById<ImageView>(R.id.btn_close)
        val progressBar = customView.findViewById<ProgressBar>(R.id.progress_bar)
        val lyFailed = customView.findViewById<LinearLayout>(R.id.ly_failed)
        val btnTryagain = customView.findViewById<Button>(R.id.btn_try_again)
        val rvLocation = customView.findViewById<RecyclerView>(R.id.rv_location)
        val searchView = customView.findViewById<SearchView>(R.id.search_view)
        tvTitle.text = "Province"

        val adapterProvinsi = ProvinsiAdapter(this@InputCatActivity)

        rvLocation.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@InputCatActivity, RecyclerView.VERTICAL, false)
            adapter = adapterProvinsi
        }

        mViewModel.doGetProvinsi({
            progressBar.visibility = View.GONE
            lyFailed.visibility = View.GONE
            val provinsi = it.data
            adapterProvinsi.updateData(provinsi!!)

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    var textSearch = newText.toString()
                    textSearch = textSearch.toLowerCase()
                    var newList: MutableList<ProvinsiListResponse.Data> = ArrayList()
                    if (textSearch.isEmpty()) {
                        newList = provinsi.toMutableList()
                    } else {
                        for (model in provinsi) {
                            val title: String = model.name!!.toLowerCase()
                            if (title.contains(textSearch)) {
                                newList.add(model)
                            }
                        }
                    }
                    adapterProvinsi.setFilter(newList)

                    return false
                }

            })
        }) {
            lyFailed.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            print(it.message)
        }

        adapterProvinsi.setOnItemClickListener(object : ProvinsiAdapter.OnItemClickListener{
            override fun onItemClick(data: ProvinsiListResponse.Data) {
                binding.etProvince.setText(data.name)
                idProvince = data.idProvinsi!!
                dialog.dismiss()
            }

        })

        btnClose.setOnClickListener {
            dialog.dismiss()
        }

        btnTryagain.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            lyFailed.visibility = View.GONE
            mViewModel.doGetProvinsi({
                progressBar.visibility = View.GONE
                lyFailed.visibility = View.GONE
                val provinsi = it.data
                adapterProvinsi.updateData(provinsi!!)
            }) {
                lyFailed.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                print(it.message)
            }
        }
    }

    private fun showKotaDialog(id: String){
        val customView = LayoutInflater.from(this@InputCatActivity).inflate(R.layout.dialog_location, null, false)
        val dialog = MaterialAlertDialogBuilder(this@InputCatActivity, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialog.show()
        val tvTitle = customView.findViewById<TextView>(R.id.dialog_title)
        val btnClose = customView.findViewById<ImageView>(R.id.btn_close)
        val progressBar = customView.findViewById<ProgressBar>(R.id.progress_bar)
        val lyFailed = customView.findViewById<LinearLayout>(R.id.ly_failed)
        val btnTryagain = customView.findViewById<Button>(R.id.btn_try_again)
        val rvLocation = customView.findViewById<RecyclerView>(R.id.rv_location)
        val searchView = customView.findViewById<SearchView>(R.id.search_view)
        tvTitle.text = "City"

        val adapterKota = KotaAdapter(this@InputCatActivity)

        rvLocation.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@InputCatActivity, RecyclerView.VERTICAL, false)
            adapter = adapterKota
        }

        mViewModel.doGetKota(id, {
            lyFailed.visibility = View.GONE
            progressBar.visibility = View.GONE
            val data = it.data
            adapterKota.updateData(data!!)

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    var textSearch = newText.toString()
                    textSearch = textSearch.toLowerCase()
                    var newList: MutableList<KotaListResponse.Data> = ArrayList()
                    if (textSearch.isEmpty()) {
                        newList = data.toMutableList()
                    } else {
                        for (model in data) {
                            val title: String = model.name!!.toLowerCase()
                            if (title.contains(textSearch)) {
                                newList.add(model)
                            }
                        }
                    }
                    adapterKota.setFilter(newList)

                    return false
                }

            })
        }) {
            lyFailed.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            print(it.message)
        }

        adapterKota.setOnItemClickListener(object : KotaAdapter.OnItemClickListener{
            override fun onItemClick(data: KotaListResponse.Data) {
                binding.etCity.setText(data.name)
                idCity = data.idKota!!
                dialog.dismiss()
            }

        })

        btnTryagain.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            lyFailed.visibility = View.GONE
            mViewModel.doGetKota(id, {
                progressBar.visibility = View.GONE
                lyFailed.visibility = View.GONE
                val data = it.data
                adapterKota.updateData(data!!)
            }) {
                lyFailed.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                print(it.message)
            }
        }

        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun showPetTypeDialog(){
        val customView = LayoutInflater.from(this@InputCatActivity).inflate(R.layout.dialog_location, null, false)
        val dialog = MaterialAlertDialogBuilder(this@InputCatActivity, R.style.MaterialAlertDialog_rounded)
            .setView(customView)
            .setCancelable(true)
            .create()

        dialog.show()
        val tvTitle = customView.findViewById<TextView>(R.id.dialog_title)
        val btnClose = customView.findViewById<ImageView>(R.id.btn_close)
        val progressBar = customView.findViewById<ProgressBar>(R.id.progress_bar)
        val lyFailed = customView.findViewById<LinearLayout>(R.id.ly_failed)
        val btnTryagain = customView.findViewById<Button>(R.id.btn_try_again)
        val rvLocation = customView.findViewById<RecyclerView>(R.id.rv_location)
        val searchView = customView.findViewById<SearchView>(R.id.search_view)
        tvTitle.text = "Pet Type"

        val adapterPetType = PetTypeAdapter(this@InputCatActivity)

        rvLocation.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@InputCatActivity, RecyclerView.VERTICAL, false)
            adapter = adapterPetType
        }

        mViewModel.doGetTypePet({
            lyFailed.visibility = View.GONE
            progressBar.visibility = View.GONE
            val data = it.data
            adapterPetType.updateData(data!!)

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    var textSearch = newText.toString()
                    textSearch = textSearch.toLowerCase()
                    var newList: MutableList<PetTypeResponse.Data> = ArrayList()
                    if (textSearch.isEmpty()) {
                        newList = data.toMutableList()
                    } else {
                        for (model in data) {
                            val title: String = model.title!!.toLowerCase()
                            if (title.contains(textSearch)) {
                                newList.add(model)
                            }
                        }
                    }
                    adapterPetType.setFilter(newList)

                    return false
                }

            })
        }) {
            lyFailed.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            print(it.message)
        }

        adapterPetType.setOnItemClickListener(object : PetTypeAdapter.OnItemClickListener{
            override fun onItemClick(data: PetTypeResponse.Data) {
                binding.etTypePet.setText(data.title)
                idPetType = data.id.toString()
                dialog.dismiss()
            }

        })

        btnTryagain.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            lyFailed.visibility = View.GONE
            mViewModel.doGetTypePet({
                progressBar.visibility = View.GONE
                lyFailed.visibility = View.GONE
                val data = it.data
                adapterPetType.updateData(data!!)
            }) {
                lyFailed.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                print(it.message)
            }
        }

        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

    override fun onChanged(state: InputCatState?) {
        when(state){
            is InputCatState.OnSuccessAddCat -> {
                val status = state.addCatResponse.code?:""
                val msg = state.addCatResponse.message?:""

                if (status == "200"){
                    val uuid = state.addCatResponse.data?.uuid?:""
                    if (fileList.size > 1){
                        mViewModel.doAddGallery(header = getParamHeader(), catsUUID = uuid, filePath = fileList)
                    } else {
                        clearEdittext()
                        val intent = Intent(this@InputCatActivity, SuccessInputActivity::class.java)
                        intent.putExtra(INPUT_SUCCESS, INPUT_SUCCESS_CAT)
                        startActivity(intent)
                        this@InputCatActivity.finish()
                    }
                } else {
                    toast(msg)
                }
            }

            is InputCatState.OnSuccessAddGallery -> {
                val status = state.baseResponse.code?:""
                val msg = state.baseResponse.message?:""

                if (status == "200"){
                    clearEdittext()
                    val intent = Intent(this@InputCatActivity, SuccessInputActivity::class.java)
                    intent.putExtra(INPUT_SUCCESS, INPUT_SUCCESS_CAT)
                    startActivity(intent)
                    this@InputCatActivity.finish()
                } else {
                    toast(msg)
                }
            }

            is InputCatState.OnSuccessUpdateCat -> {
                val status = state.baseResponse.code?:""
                val msg = state.baseResponse.message?:""

                if (status == "200"){
                    DetailCatActivity.FinishCatDetailActivity.activity?.finish()
                    val intent = Intent(this@InputCatActivity, SuccessInputActivity::class.java)
                    intent.putExtra(INPUT_SUCCESS, INPUT_SUCCESS_UPDATE_CAT)
                    startActivity(intent)
                    this@InputCatActivity.finish()
                } else {
                    toast("Update data cat failed")
                    logE(TAG, msg)
                }
            }

            is InputCatState.OnLoading -> {
                val loading = state.isLoading
                if (loading){
                    showDialogLoading("Processing...")
                } else {
                    dialogLoading.dismiss()
                }
            }

            is InputCatState.OnErrorState -> {
                val msg = state.t.localizedMessage?:""
                toast(msg)
            }

            is InputCatState.OnErrorUploadState -> {
                val msg = state.t.localizedMessage?:""
                val header = state.header
                val catUUID = state.catsUUID
                val filePath = state.filePath
                toastLong("$msg, Failed upload photo/video, please try again!")

                binding.btnAddCat.setOnClickListener {
                    mViewModel.doAddGallery(header = header, catsUUID = catUUID, filePath = filePath)
                }
            }
        }
    }
}