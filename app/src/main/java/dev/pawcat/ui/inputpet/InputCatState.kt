package dev.pawcat.ui.inputpet

import dev.pawcat.data.local.model.UploadModel
import dev.pawcat.data.remote.response.AddCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse

sealed class InputCatState {
    data class OnSuccessAddCat(val addCatResponse: AddCatResponse) : InputCatState()
    data class OnSuccessAddGallery(val baseResponse: BaseResponse) : InputCatState()
    data class OnSuccessUpdateCat(val baseResponse: BaseResponse) : InputCatState()
    data class OnErrorState(val t: Throwable) : InputCatState()
    data class OnErrorUploadState(val t: Throwable,
                                  val header:Map<String, String>,
                                  val filePath: List<UploadModel>,
                                  val catsUUID: String) : InputCatState()
    data class OnLoading(val isLoading: Boolean) : InputCatState()
}