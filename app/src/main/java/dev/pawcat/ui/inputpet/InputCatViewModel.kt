package dev.pawcat.ui.inputpet

import android.content.Context
import com.google.gson.JsonObject
import dev.pawcat.data.local.model.UploadModel
import dev.pawcat.data.remote.response.AddCatResponse
import dev.pawcat.data.remote.response.KotaListResponse
import dev.pawcat.data.remote.response.PetTypeResponse
import dev.pawcat.data.remote.response.ProvinsiListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.data.repo.CatRepo
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class InputCatViewModel(
    val catRepo: CatRepo,
    val authRepo: AuthRepo,
    val context: Context
) : RxViewModel<InputCatState>(), IInputCatViewModel {
    override val TAG: String
        get() = InputCatState::class.java.simpleName

    private val disposable = CompositeDisposable()

    override fun successAddCat(addCatResponse: AddCatResponse) {
        logD(TAG,"success do delete cat")
        onLoading(false)
        state.value = InputCatState.OnSuccessAddCat(addCatResponse)
    }

    override fun successAddGallery(baseResponse: BaseResponse) {
        logD(TAG,"success do add gallery")
        onLoading(false)
        state.value = InputCatState.OnSuccessAddGallery(baseResponse)
    }

    override fun successUpdateCat(baseResponse: BaseResponse) {
        logD(TAG,"success do update cat")
        onLoading(false)
        state.value = InputCatState.OnSuccessUpdateCat(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        onLoading(false)
        state.value = InputCatState.OnErrorState(t)
    }


    override fun errorUpload(t: Throwable,
                             header: Map<String, String>,
                             filePath: List<UploadModel>,
                             catsUUID: String) {
        logE(TAG,"error uplload")
        onLoading(false)
        state.value = InputCatState.OnErrorUploadState(t,header, filePath, catsUUID)
    }

    override fun onLoading(isLoading: Boolean) {
        logE(TAG,"on loading : $isLoading")
        state.value = InputCatState.OnLoading(isLoading)
    }

    fun doAddCat(header:Map<String, String>,
                 filePath: String,
                 name: String,
                 content: String,
                 breed: String,
                 gender: String,
                 age: String,
                 weight: String,
                 available: String,
                 idProvince: String,
                 idCity: String,
                 petType: String
    ) {
        logD(TAG,"do add cat")
        launch {
            onLoading(true)
            catRepo.doAddCat(header,
                filePath,
                name,
                content,
                breed,
                gender,
                age,
                weight,
                available,
                idProvince, idCity, petType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddCat) {
                    errorLoad(t = it)
                }
        }
    }

    fun doAddGallery(header:Map<String, String>,
                 filePath: List<UploadModel>,
                 catsUUID: String
    ) {
        logD(TAG,"do add cat")
        launch {
            onLoading(true)
            catRepo.doAddGallery(header = header,
                filePath = filePath,
                catUUID = catsUUID
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddGallery) {
                    errorUpload(it, header, filePath, catsUUID)
                }
        }
    }

    fun doUpdateCat(header:Map<String, String>, data: JsonObject) {
        logD(TAG,"do update reminder")
        launch {
            onLoading(true)
            catRepo.doUpdateCat(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successUpdateCat) {
                    errorLoad(t = it)
                }
        }
    }

    fun doGetProvinsi(data: ((data: ProvinsiListResponse) -> Unit?)? = null, throwable: ((throwable: Throwable) -> Unit?)? = null) {
        val headers = context.getParamHeader()
        launch {
            authRepo.getProvinsi(headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    data?.invoke(it)
                }) {
                    throwable?.invoke(it)
                }
        }
    }

    fun doGetKota(id: String, data: ((data: KotaListResponse) -> Unit?)? = null, throwable: ((throwable: Throwable) -> Unit?)? = null) {
        val headers = context.getParamHeader()
        launch {
            authRepo.getKota(headers, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    data?.invoke(it)
                }) {
                    throwable?.invoke(it)
                }
        }
    }

    fun doGetTypePet(data: ((data: PetTypeResponse) -> Unit?)? = null, throwable: ((throwable: Throwable) -> Unit?)? = null) {
        val headers = context.getParamHeader()
        launch {
            authRepo.getPetType(headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    data?.invoke(it)
                }) {
                    throwable?.invoke(it)
                }
        }
    }
}