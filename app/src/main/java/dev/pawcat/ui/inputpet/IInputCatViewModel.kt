package dev.pawcat.ui.inputpet

import dev.pawcat.data.local.model.UploadModel
import dev.pawcat.data.remote.response.AddCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse

interface IInputCatViewModel {
    fun successAddCat(addCatResponse: AddCatResponse)
    fun successAddGallery(baseResponse: BaseResponse)
    fun successUpdateCat(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
    fun errorUpload(t: Throwable,
                    header:Map<String, String>,
                    filePath: List<UploadModel>,
                    catsUUID: String)
    fun onLoading(isLoading: Boolean)
}