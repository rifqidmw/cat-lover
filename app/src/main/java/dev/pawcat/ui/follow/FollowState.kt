package dev.pawcat.ui.follow

import dev.pawcat.data.remote.response.base.BaseResponse

sealed class FollowState {
    data class OnSuccessFollow(val baseResponse: BaseResponse) : FollowState()
    data class OnErrorState(val t: Throwable) : FollowState()
}