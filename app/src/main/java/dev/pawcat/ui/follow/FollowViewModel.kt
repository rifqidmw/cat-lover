package dev.pawcat.ui.follow

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import dev.pawcat.data.repo.AuthRepo
import dev.pawcat.data.repo.PagedListRepo
import dev.pawcat.data.repo.paged.UserListPagingSource
import dev.pawcat.utils.RxViewModel
import dev.pawcat.utils.getParamHeader
import dev.pawcat.utils.logD
import dev.pawcat.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class FollowViewModel(
    private val authRepo: AuthRepo,
    private val pagedListRepo: PagedListRepo, private val context: Context
) : RxViewModel<FollowState>(), IFollowViewModel{
    override val TAG: String
        get() = FollowState::class.java.simpleName

    override fun successFollow(baseResponse: BaseResponse) {
        logD(TAG,"success do follow")
        state.value = FollowState.OnSuccessFollow(baseResponse)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = FollowState.OnErrorState(t)
    }

    fun userList(name: String, status: Int): Flow<PagingData<UserData.UserItem>> {
        val headers = context.getParamHeader()
        return Pager(PagingConfig(20)) {
            UserListPagingSource(headers = headers, pagedListRepo = pagedListRepo, name = name, status = status)
        }.flow.map { pagingData -> pagingData.map { UserData.UserItem(it) } }
    }

    fun doFollow(header:Map<String, String>, uuid: String) {
        logD(TAG,"do follow")
        launch {
            authRepo.doFollow(header, uuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successFollow) {
                    errorLoad(t = it)
                }
        }
    }
}

sealed class UserData{
    data class UserItem(val userModel: UserListResponse.User) : UserData()
}