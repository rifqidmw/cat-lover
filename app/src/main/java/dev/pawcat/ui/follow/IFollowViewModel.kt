package dev.pawcat.ui.follow

import dev.pawcat.data.remote.response.base.BaseResponse

interface IFollowViewModel {
    fun successFollow(baseResponse: BaseResponse)
    fun errorLoad(t: Throwable)
}