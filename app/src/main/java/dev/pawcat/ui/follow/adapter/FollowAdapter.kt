package dev.pawcat.ui.follow.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.databinding.ItemFollowBinding
import dev.pawcat.ui.follow.UserData
import dev.pawcat.utils.USER_UUID
import dev.pawcat.utils.intentToUser
import dev.pawcat.utils.setPhotoCircularUrl

class FollowAdapter(
    private val context: Context,
    private val session: Session
) : PagingDataAdapter<UserData.UserItem, FollowAdapter.FollowViewHolder>(DIIF_UTIL) {

    var listener: FollowClickListener? = null

    open inner class FollowViewHolder(
        val binding: ItemFollowBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(followModel: UserData.UserItem?){
            val data = followModel?.userModel
            var follow = data?.statusFollowing

            binding.apply {
                imgProfile.setPhotoCircularUrl(context, data?.userAvatar?:"")
                tvName.text = data?.usersName
                tvEmail.text = data?.usersEmail

                root.setOnClickListener {
                    val userUUID = data?.uuid
                    if (userUUID != session[USER_UUID]){
                        context.intentToUser(userUUID)
                    }
                }

                when(follow){
                    "0" -> {
                        btnFollow.text = "Follow"
                        btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_filled)
                    }

                    "1" -> {
                        btnFollow.text = "Unfollow"
                        btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_red_filled)
                    }
                }

                btnFollow.setOnClickListener {
                    when(follow){
                        "0" -> {
                            follow = "1"
                            btnFollow.text = "Unfollow"
                            btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_red_filled)
                        }

                        "1" -> {
                            follow = "0"
                            btnFollow.text = "Follow"
                            btnFollow.background = ContextCompat.getDrawable(context, R.drawable.button_rounded_gradient_filled)
                        }
                    }
                    listener?.onItemFollowClicked(data!!)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: FollowViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FollowViewHolder {
        val view = LayoutInflater.from(parent.context)

        return FollowViewHolder(
            ItemFollowBinding.inflate(view, parent, false)
        )
    }

    companion object{
        val DIIF_UTIL = object : DiffUtil.ItemCallback<UserData.UserItem>(){
            override fun areItemsTheSame(oldItem: UserData.UserItem, newItem: UserData.UserItem): Boolean {
                return oldItem.userModel.uuid == newItem.userModel.uuid
            }

            override fun areContentsTheSame(oldItem: UserData.UserItem, newItem: UserData.UserItem): Boolean {
                return oldItem == newItem
            }

        }
    }

    interface FollowClickListener {
        fun onItemFollowClicked(data: UserListResponse.User)
    }
}