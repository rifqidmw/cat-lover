package dev.pawcat.ui.follow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.databinding.ActivityFollowBinding
import dev.pawcat.ui.follow.adapter.FollowAdapter
import dev.pawcat.utils.*
import dev.pawcat.utils.view.FooterLoadingAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class FollowActivity : AppCompatActivity(), Observer<FollowState>, FollowAdapter.FollowClickListener {
    private val TAG = FollowActivity::class.java.simpleName
    private val binding: ActivityFollowBinding by lazy {
        DataBindingUtil.setContentView<ActivityFollowBinding>(
            this, R.layout.activity_follow
        )
    }
    private val mViewModel: FollowViewModel by inject()
    private val session: Session by inject()
    private val followAdapter: FollowAdapter by lazy { FollowAdapter(this@FollowActivity, session) }

    private var followType: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.state.observe(this, this@FollowActivity)

        setupIntent()
    }

    private fun setupIntent(){
        intent.apply {
            when (getStringExtra(FOLLOW_TYPE)){
                FOLLOW_FOLLOWING -> {
                    followType = 1
                }

                FOLLOW_FOLLOWER -> {
                    followType = 2
                }

                else -> {
                    followType = 0
                }
            }

            setupViews()
            setupSearchView()
            loadDataFollow("",true)
        }
    }

    private fun setupViews(){
        binding.apply {
            when(followType){
                0 -> {
                    tvToolbarTitle.text = "Users"
                }
                1 -> {
                    tvToolbarTitle.text =  "Following"
                }
                2 -> {
                    tvToolbarTitle.text =  "Follower"
                }
            }

            rvFollow.adapter = followAdapter
            rvFollow.adapter = followAdapter.withLoadStateFooter(
                footer = FooterLoadingAdapter{ followAdapter.retry() }
            )
            followAdapter.listener = this@FollowActivity

            navBack.setOnClickListener {
                onBackPressed()
                this@FollowActivity.finish()
            }
        }
    }

    private fun loadDataFollow(query: String, withLoading: Boolean){
        binding.apply {
            lifecycleScope.launch {
                mViewModel.userList(query, followType).collectLatest {
                    followAdapter.submitData(it)
                }
            }

            followAdapter.addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading){
                    if (withLoading){
                        progressBar.visibility = View.VISIBLE
                    }
                } else {
                    progressBar.visibility = View.GONE
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    if (loadState.append.endOfPaginationReached) {
                        if (followAdapter.itemCount > 0) {
                            errorState.let {
                                showSnackbar(it?.error?.message ?: "", root)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setupSearchView(){
        binding.apply {

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    loadDataFollow(query?:"", true)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText.isNullOrEmpty()){
                        loadDataFollow("", true)
                    }

                    return false
                }

            })

            val clearButton = searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn)
            clearButton.setOnClickListener {
                searchView.setQuery("", false)
                loadDataFollow("", true)
            }
        }
    }

    private fun showSnackbar(msg: String, view: View){
        if (msg.isNotEmpty()){
            val snack = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
            snack.setAction("Retry", View.OnClickListener {
                followAdapter.retry()
            })
            snack.show()
        }
    }

    override fun onItemFollowClicked(data: UserListResponse.User) {
        mViewModel.doFollow(getParamHeader(), data.uuid?:"")
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onChanged(state: FollowState?) {
        when(state){
            is FollowState.OnSuccessFollow -> {

            }

            is FollowState.OnErrorState -> {
                val msg = state.t.localizedMessage
                logE(TAG, msg)
            }
        }
    }
}