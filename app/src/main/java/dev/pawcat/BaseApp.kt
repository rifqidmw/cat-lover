package dev.pawcat

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import dev.pawcat.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BaseApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            androidLogger()
            modules(listOf(networkModule, dataModule, repositoryModule, viewModelModule))
        }
        AndroidThreeTen.init(applicationContext)
    }
}