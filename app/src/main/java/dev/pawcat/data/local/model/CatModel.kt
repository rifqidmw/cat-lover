package dev.pawcat.data.local.model

data class CatModel(
    val id: Int,
    val name: String,
    val imgUrl: String,
    val desc: String,
    val gender: Int
)
