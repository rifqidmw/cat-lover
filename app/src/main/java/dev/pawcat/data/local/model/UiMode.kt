package dev.pawcat.data.local.model

enum class UiMode {
    LIGHT, DARK
}