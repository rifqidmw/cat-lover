package dev.pawcat.data.local.model

data class ScheduleModel(
    val id: Int,
    val name: String,
    val type: String,
    val date: String,
    val time: String
)
