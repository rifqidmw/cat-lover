package dev.pawcat.data.local.model

data class GenderModel(
    var id: String,
    var title: String
)