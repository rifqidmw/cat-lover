package dev.pawcat.data.local

import dev.pawcat.R
import dev.pawcat.data.local.model.CatModel
import dev.pawcat.data.local.model.CommentModel
import dev.pawcat.data.local.model.ScheduleModel

/*fun getDataPost() = listOf(
    PostModel(
        id = 1,
        imgUrl = "https://asset.kompas.com/crops/AOqycoSV_pH5eU51rYStWW_zVFY=/1x0:1000x666/750x500/data/photo/2019/11/04/5dbfff829ebe6.jpg",
        username = "John",
        userPhoto = R.drawable.ic_student,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        date = "1 hour ago",
        liked = 200,
        comment = 48
    ),
    PostModel(
        id = 2,
        imgUrl = "https://media.suara.com/pictures/970x544/2020/08/28/10634-kucing-mirip-pikachu.jpg",
        username = "Bloop",
        userPhoto = R.drawable.ic_student,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        date = "4 hour ago",
        liked = 200,
        comment = 48
    ),
    PostModel(
        id = 3,
        imgUrl = "https://mmc.tirto.id/image/2019/01/08/kucing-sedih-istockphoto_ratio-16x9.jpg",
        username = "Yolo",
        userPhoto = R.drawable.ic_student,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        date = "yesterday",
        liked = 200,
        comment = 48
    ),
    PostModel(
        id = 4,
        imgUrl = "https://img.okezone.com/content/2020/10/23/18/2298597/bunuh-kucing-hamil-dengan-air-mendidih-karyawan-perusahaan-keamanan-dipecat-I7osD0ZjB7.jpg",
        username = "Rifqi",
        userPhoto = R.drawable.ic_student,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        date = "yesterday",
        liked = 200,
        comment = 48
    ),
    PostModel(
        id = 5,
        imgUrl = "https://cdns.klimg.com/bola.net/library/upload/21/2020/11/ternyata-kucing-albi_a7be7f0.jpg",
        username = "Darmawan",
        userPhoto = R.drawable.ic_student,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        date = "a week ago",
        liked = 200,
        comment = 48
    ),
    PostModel(
        id = 6,
        imgUrl = "https://akcdn.detik.net.id/visual/2014/12/30/2e6aeb6f-e98e-4194-ac97-a39de33dcb21_169.jpg?w=650",
        username = "QuiQui",
        userPhoto = R.drawable.ic_student,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        date = "2 month ago",
        liked = 200,
        comment = 48
    )
)*/

fun getDataCat() = listOf(
    CatModel(
        id = 1,
        name = "Coco",
        imgUrl = "https://asset.kompas.com/crops/AOqycoSV_pH5eU51rYStWW_zVFY=/1x0:1000x666/750x500/data/photo/2019/11/04/5dbfff829ebe6.jpg",
        gender = 1,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
    ),
    CatModel(
        id = 2,
        name = "Anya",
        imgUrl = "https://media.suara.com/pictures/970x544/2020/08/28/10634-kucing-mirip-pikachu.jpg",
        gender = 2,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
    ),
    CatModel(
        id = 3,
        name = "Ollie",
        imgUrl = "https://mmc.tirto.id/image/2019/01/08/kucing-sedih-istockphoto_ratio-16x9.jpg",
        gender = 2,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
    ),
    CatModel(
        id = 4,
        name = "Moona",
        imgUrl = "https://img.okezone.com/content/2020/10/23/18/2298597/bunuh-kucing-hamil-dengan-air-mendidih-karyawan-perusahaan-keamanan-dipecat-I7osD0ZjB7.jpg",
        gender = 1,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
    ),
    CatModel(
        id = 5,
        name = "Pekora",
        imgUrl = "https://cdns.klimg.com/bola.net/library/upload/21/2020/11/ternyata-kucing-albi_a7be7f0.jpg",
        gender = 1,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
    ),
    CatModel(
        id = 6,
        name = "Miko",
        imgUrl = "https://akcdn.detik.net.id/visual/2014/12/30/2e6aeb6f-e98e-4194-ac97-a39de33dcb21_169.jpg?w=650",
        gender = 2,
        desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
    )
)

fun getDataSchedule() = listOf(
    ScheduleModel(
        id = 1,
        name = "Ollie",
        type = "Food",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Coco",
        type = "Vaccine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Roboco",
        type = "Medicine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Ollie",
        type = "Medicine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Miko",
        type = "Vaccine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Pekora",
        type = "Food",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Miko",
        type = "Food",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Hachama",
        type = "Medicine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Miko",
        type = "Medicine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Anya",
        type = "Food",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Anya",
        type = "Vaccine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Reine",
        type = "Food",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Iofi",
        type = "Vaccine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Moona",
        type = "Medicine",
        date = "January 23, 2021",
        time = "13:40 WIB"
    ),
    ScheduleModel(
        id = 1,
        name = "Risu",
        type = "Food",
        date = "January 23, 2021",
        time = "13:40 WIB"
    )
)

fun getDataComment() = listOf(
    CommentModel(
        id = 1,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Isaak",
        comment = "Very Beautiful",
        date = "2 hour ago"
    ),
    CommentModel(
        id = 2,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Newton",
        comment = "i want too",
        date = "2 hour ago"
    ),
    CommentModel(
        id = 3,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Alexandra",
        comment = "Beautiful cat",
        date = "4 hour ago"
    ),
    CommentModel(
        id = 4,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Daniel",
        comment = "love it",
        date = "5 hour"
    ),
    CommentModel(
        id = 5,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Astrid",
        comment = "looking cat like this",
        date = "yesterday"
    ),
    CommentModel(
        id = 6,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Rifqi Darmawan",
        comment = "Just spamming",
        date = "a week ago"
    ),
    CommentModel(
        id = 7,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Karlyn Tarver",
        comment = "I have cat like this",
        date = "2 week ago"
    ),
    CommentModel(
        id = 8,
        userPhotoProfile = R.drawable.ic_no_image,
        username = "Skylar Grey",
        comment = "Hi kak, jual follower disini!!",
        date = "a month ago"
    )
)