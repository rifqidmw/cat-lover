package dev.pawcat.data.local

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.preferencesKey
import androidx.datastore.preferences.createDataStore
import dev.pawcat.data.local.model.UiMode
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

class DataStoreManager (context: Context) {
    private val dataStore = context.createDataStore(name = "settings_pref")

    val uiModeFlow: Flow<UiMode> = dataStore.data
        .catch {
            if (it is IOException){
                it.printStackTrace()
                emit(emptyPreferences())
            } else {
                throw it
            }
        }.map { preferences ->
            val isDarkMode = preferences[IS_DARK_MODE] ?: false

            when(isDarkMode){
                true -> UiMode.DARK
                false -> UiMode.LIGHT
            }
        }

    suspend fun setUiMode(uiMode: UiMode){
        dataStore.edit { preferences ->
            preferences[IS_DARK_MODE] = when(uiMode){
                UiMode.LIGHT -> false
                UiMode.DARK -> true
            }
        }
    }

    companion object{
        val IS_DARK_MODE = preferencesKey<Boolean>("dark_mode")

    }
}