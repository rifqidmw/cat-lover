package dev.pawcat.data.local

import android.content.Context
import android.content.SharedPreferences

class Session (context: Context){
    internal var public_MODE = 0

    init {
        preferences = context.getSharedPreferences(
            IS_NAV, public_MODE)
        editor = preferences.edit()
    }

    companion object {
        private val attempt = 1
        private val context: Context? = null
        private lateinit var preferences: SharedPreferences
        private lateinit var editor: SharedPreferences.Editor
        val IS_NAV = "DisableNavigation"
    }

    fun save(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun save(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun save(key: String, dataSet: HashSet<String>){
        editor.putStringSet(key, dataSet)
        editor.commit()
    }

    fun save(key: String, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    fun save(key: String, value: Float) {
        editor.putFloat(key, value)
        editor.commit()
    }

    fun save(key: String, value: Long?) {
        save(key, value.toString())
    }

    operator fun get(key: String): String? {
        return preferences.getString(key, null as String?)
    }

    fun getBool(key: String): Boolean? {
        return preferences.getBoolean(key, true)
    }

    operator fun contains(key: String): Boolean {
        return java.lang.Boolean.valueOf(preferences.contains(key))
    }

    fun removeKey(key: String) {
        editor.remove(key)
        editor.commit()
    }

    fun clear() {
        editor.clear()
        editor.commit()
    }
}