package dev.pawcat.data.local.model

data class CommentModel(
    val id: Int,
    val userPhotoProfile: Int,
    val username: String,
    val comment: String,
    val date: String
)
