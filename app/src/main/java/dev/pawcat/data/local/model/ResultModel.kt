package dev.pawcat.data.local.model

sealed class ResultModel<out T: Any>{
    data class Success<out T: Any>(val data: T) : ResultModel<T>()
    data class Error(val error: Exception) : ResultModel<Nothing>()
}
