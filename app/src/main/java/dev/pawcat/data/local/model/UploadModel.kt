package dev.pawcat.data.local.model

data class UploadModel(
    val id: Int,
    val fileUrl: String,
    val type: String
)