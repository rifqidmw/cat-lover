package dev.pawcat.data.local.model

data class TypePetModel(
    var id: String,
    var title: String
)