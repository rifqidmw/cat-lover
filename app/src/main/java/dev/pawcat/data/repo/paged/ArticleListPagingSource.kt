package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.ArticleListResponse
import dev.pawcat.data.repo.PagedListRepo

class ArticleListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val name: String,
    private val content: String
) : PagingSource<Int, ArticleListResponse.Article>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ArticleListResponse.Article> {
        return try {
            val nextPage = params.key ?: 1
            val articleListResponse = pagedListRepo.getArticleList(header = headers, name = name, content = content, page = nextPage).data!!
            LoadResult.Page(
                data = articleListResponse.article!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < articleListResponse.lastPage!!.toInt())
                    articleListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}