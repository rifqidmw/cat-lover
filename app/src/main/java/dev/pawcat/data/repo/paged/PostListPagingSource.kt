package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.repo.PagedListRepo

class PostListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val uuid: String,
    private val all: String
) : PagingSource<Int, PostListResponse.Post>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PostListResponse.Post> {
        return try {
            val nextPage = params.key ?: 1
            val postListResponse = pagedListRepo.getPost(header = headers, uuid = uuid, all = all, page = nextPage).data

            LoadResult.Page(
                data = postListResponse?.post!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < postListResponse.lastPage!!.toInt())
                    postListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}