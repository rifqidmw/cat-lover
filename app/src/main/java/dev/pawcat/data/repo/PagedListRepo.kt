package dev.pawcat.data.repo

import dev.pawcat.data.local.model.ResultModel
import dev.pawcat.data.remote.PagedListService
import dev.pawcat.data.remote.response.*

class PagedListRepo(private val service: PagedListService) {
    suspend fun getPost(header: Map<String, String>, uuid: String, all: String, page: Int) : PostListResponse {
        return when(val result = service.fetchPostList(
            header = header,
            uuid = uuid,
            perpage = 20,
            all = all,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getCat(header: Map<String, String>, userUUID: String, name: String,
                       available: String, jenisPet: String, provinsi: String, kota: String,
                       gender: String, umurMax: String, umurMin: String, page: Int) : CatListResponse {
        return when(val result = service.fetchCatList(
            header = header,
            userUUID = userUUID,
            name = name,
            available = available,
            perpage = 20,
            page = page,
            jenisPet = jenisPet,
            provinsi = provinsi,
            kota = kota,
            gender = gender,
            umurMax = umurMax,
            umurMin = umurMin
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getReminder(header: Map<String, String>, page: Int) : ReminderListResponse {
        return when(val result = service.fetchReminderList(
            header = header,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getCommentPost(header: Map<String, String>, postUUID: String, page: Int) : CommentPostListResponse {
        return when(val result = service.fetchCommentPostList(
            header = header,
            postUUID = postUUID,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getCommentCat(header: Map<String, String>, catUUID: String, page: Int) : CommentCatListResponse {
        return when(val result = service.fetchCommentCatList(
            header = header,
            catUUID = catUUID,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getCommentArticle(header: Map<String, String>, articleUUID: String, page: Int) : CommentArticleListResponse {
        return when(val result = service.fetchCommentArticleList(
            header = header,
            articleUUID = articleUUID,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getUserList(header: Map<String, String>, name: String, status: Int, page: Int) : UserListResponse {
        return when(val result = service.fetchUserList(
            header = header,
            name = name,
            status = status,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getArticleList(header: Map<String, String>, name: String, content: String, page: Int) : ArticleListResponse {
        return when(val result = service.fetchArticleList(
            header = header,
            name = name,
            content = content,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getSearchUserList(header: Map<String, String>, title: String, type: String, page: Int) : UserListResponse {
        return when(val result = service.fetchSearchUserList(
            header = header,
            title = title,
            type = type,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getSearchPostList(header: Map<String, String>, title: String, type: String, page: Int) : PostListResponse {
        return when(val result = service.fetchSearchPostList(
            header = header,
            title = title,
            type = type,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }

    suspend fun getSearchPetList(header: Map<String, String>, title: String, type: String, page: Int) : CatListResponse {
        return when(val result = service.fetchSearchPetList(
            header = header,
            title = title,
            type = type,
            perpage = 20,
            page = page
        )){
            is ResultModel.Success -> {
                result.data
            }
            is ResultModel.Error -> throw result.error
        }
    }
}