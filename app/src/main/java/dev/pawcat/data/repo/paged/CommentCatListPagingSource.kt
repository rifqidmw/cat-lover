package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.CommentCatListResponse
import dev.pawcat.data.repo.PagedListRepo

class CommentCatListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val catUUID: String
) : PagingSource<Int, CommentCatListResponse.Comment>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CommentCatListResponse.Comment> {
        return try {
            val nextPage = params.key ?: 1
            val commentListResponse = pagedListRepo.getCommentCat(headers, catUUID, nextPage).data!!
            LoadResult.Page(
                data = commentListResponse.comment!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < commentListResponse.lastPage!!.toInt())
                    commentListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}