package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.CommentArticleListResponse
import dev.pawcat.data.remote.response.CommentPostListResponse
import dev.pawcat.data.repo.PagedListRepo

class CommentArticleListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val articleUUID: String
) : PagingSource<Int, CommentArticleListResponse.Comment>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CommentArticleListResponse.Comment> {
        return try {
            val nextPage = params.key ?: 1
            val commentListResponse = pagedListRepo.getCommentArticle(headers, articleUUID, nextPage).data!!
            LoadResult.Page(
                data = commentListResponse.comment!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < commentListResponse.lastPage!!.toInt())
                    commentListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}