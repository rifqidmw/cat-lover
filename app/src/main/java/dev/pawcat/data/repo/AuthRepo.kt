package dev.pawcat.data.repo

import com.google.gson.JsonObject
import dev.pawcat.data.remote.ApiService
import dev.pawcat.data.remote.response.*
import dev.pawcat.data.remote.response.base.BaseResponse
import io.reactivex.Single
import okhttp3.MultipartBody

class AuthRepo (private val service: ApiService){
    fun doLogin(header: Map<String, String>, data: JsonObject) : Single<LoginResponse> {
        return service.doLogin(header, data)
    }

    fun doLogout(header: Map<String, String>) : Single<LogoutResponse> {
        return service.doLogout(header)
    }

    fun doRegister(header: Map<String, String>, data: JsonObject) : Single<RegisterResponse> {
        return service.doRegister(header, data)
    }

    fun doUpdateAvatar(header: Map<String, String>, imageFile: MultipartBody.Part) : Single<UpdateAvatarResponse>{
        return service.doUpdateAvatar(header, imageFile)
    }

    fun doUpdateProfile(header: Map<String, String>, data: JsonObject) : Single<UpdateProfileResponse> {
        return service.doUpdateProfile(header, data)
    }

    fun doUpdatePassword(header: Map<String, String>, data: JsonObject) : Single<UpdatePasswordResponse> {
        return service.doUpdatePassword(header, data)
    }

    fun doGetDetailUser(header: Map<String, String>, uuid: String) : Single<DetailUserResponse> {
        return service.detailUser(header, uuid)
    }

    fun doFollow(header: Map<String, String>, uuid: String) : Single<BaseResponse> {
        return service.followUser(header, uuid)
    }

    fun getProfile(header: Map<String, String>) : Single<GetProfileResponse> {
        return service.getProfile(header)
    }

    fun getProvinsi(header: Map<String, String>) : Single<ProvinsiListResponse> {
        return service.getProvinsi(header)
    }

    fun getKota(header: Map<String, String>, id: String) : Single<KotaListResponse> {
        return service.getKota(header, id)
    }

    fun getPetType(header: Map<String, String>) : Single<PetTypeResponse> {
        return service.getPetType(header)
    }
}