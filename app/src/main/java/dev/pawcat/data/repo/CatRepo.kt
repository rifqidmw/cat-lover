package dev.pawcat.data.repo

import com.google.gson.JsonObject
import dev.pawcat.data.local.model.UploadModel
import dev.pawcat.data.remote.ApiService
import dev.pawcat.data.remote.response.*
import dev.pawcat.data.remote.response.base.AddCommentCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class CatRepo(private val service: ApiService) {
    fun doAddCat(header: Map<String, String>,
                 filePath: String,
                 name: String,
                 content: String,
                 breed: String,
                 gender: String,
                 age: String,
                 weight: String,
                 available: String,
                 idProvince: String,
                 idCity: String,
                 petType: String
    ) : Single<AddCatResponse> {

        return service.addCat(header,
            name = name.toRequestBody(),
            content = content.toRequestBody(),
            breed = breed.toRequestBody(),
            gender = gender.toRequestBody(),
            age = age.toRequestBody(),
            weight = weight.toRequestBody(),
            available = available.toRequestBody(),
            idProvince = idProvince.toRequestBody(),
            idCity = idCity.toRequestBody(),
            petType = petType.toRequestBody(),
            image = prepareFilePart(filePath, "image"))
    }

    fun doAddGallery(header: Map<String, String>,
                     catUUID: String,
                     filePath: List<UploadModel>
    ) : Single<BaseResponse> {
        return service.addGallery(header,
            catUUID = catUUID.toRequestBody(),
            image = getFilePart(filePath))
    }

    fun doGetGallery(header: Map<String, String>,
                     catUUID: String
    ) : Single<CatGalleryResponse> {
        return service.getGallery(header,
            catUUID = catUUID)
    }

    private fun getFilePart(filePath: List<UploadModel>): List<MultipartBody.Part>{
        val list: ArrayList<MultipartBody.Part> = ArrayList()

        for (i in filePath.indices){
            list.add(prepareFilePart(filePath[i].fileUrl, "file_cat[]"))
        }

        return list
    }

    private fun prepareFilePart(filePath: String, type: String): MultipartBody.Part{
        val file = File(filePath)
        val fileRequestBody = file.asRequestBody()
        val fileMultipartBody = MultipartBody.Part.createFormData(type,file.name,fileRequestBody)

        return fileMultipartBody
    }

    fun doAddComment(header: Map<String, String>, data: JsonObject) : Single<AddCommentCatResponse> {
        return service.doAddCatComment(header, data)
    }

    fun doUpdateCat(header: Map<String, String>, data: JsonObject) : Single<BaseResponse> {
        return service.updateCat(header, data)
    }

    fun doDeleteCat(header: Map<String, String>, uuid: String) : Single<BaseResponse> {
        return service.deleteCat(header, uuid)
    }
}