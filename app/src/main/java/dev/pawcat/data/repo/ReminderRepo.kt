package dev.pawcat.data.repo

import com.google.gson.JsonObject
import dev.pawcat.data.remote.ApiService
import dev.pawcat.data.remote.response.AddReminderResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import io.reactivex.Single

class ReminderRepo(private val service: ApiService) {
    fun doAddReminder(header: Map<String, String>, data: JsonObject) : Single<AddReminderResponse> {
        return service.doAddReminder(header, data)
    }

    fun doUpdateReminder(header: Map<String, String>, data: JsonObject, uuid: String) : Single<BaseResponse> {
        return service.updateReminder(header, data, uuid)
    }

    fun doDeleteReminder(header: Map<String, String>, uuid: String) : Single<BaseResponse> {
        return service.deleteReminder(header, uuid)
    }
}