package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.ReminderListResponse
import dev.pawcat.data.repo.PagedListRepo

class ReminderListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>
) : PagingSource<Int, ReminderListResponse.Reminder>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ReminderListResponse.Reminder> {
        return try {
            val nextPage = params.key ?: 1
            val reminderListResponse = pagedListRepo.getReminder(headers, nextPage).data!!
            LoadResult.Page(
                data = reminderListResponse.post!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < reminderListResponse.lastPage!!.toInt())
                    reminderListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}