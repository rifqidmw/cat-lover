package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.remote.response.PostListResponse
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.data.repo.PagedListRepo

class SearchPetListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val title: String,
    private val type: String
) : PagingSource<Int, CatListResponse.Cat>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CatListResponse.Cat> {
        return try {
            val nextPage = params.key ?: 1
            val petListResponse = pagedListRepo.getSearchPetList(header = headers, title = title, type = type, page = nextPage).data!!
            LoadResult.Page(
                data = petListResponse.cat!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < petListResponse.lastPage!!.toInt())
                    petListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}