package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.UserListResponse
import dev.pawcat.data.repo.PagedListRepo

class SearchUserListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val title: String,
    private val type: String
) : PagingSource<Int, UserListResponse.User>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UserListResponse.User> {
        return try {
            val nextPage = params.key ?: 1
            val userListResponse = pagedListRepo.getSearchUserList(header = headers, title = title, type = type, page = nextPage).data!!
            LoadResult.Page(
                data = userListResponse.user!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < userListResponse.lastPage!!.toInt())
                    userListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}