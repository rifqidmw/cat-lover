package dev.pawcat.data.repo.paged

import androidx.paging.PagingSource
import dev.pawcat.data.remote.response.CatListResponse
import dev.pawcat.data.repo.PagedListRepo

class CatListPagingSource(
    private val pagedListRepo: PagedListRepo,
    private val headers: HashMap<String, String>,
    private val userUUID: String,
    private val name: String,
    private val available: String,
    private val jenisPet: String,
    private val provinsi: String,
    private val kota: String,
    private val gender: String,
    private val umurMax: String,
    private val umurMin: String
) : PagingSource<Int, CatListResponse.Cat>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CatListResponse.Cat> {
        return try {
            val nextPage = params.key ?: 1
            val catListResponse = pagedListRepo.getCat(header = headers, userUUID = userUUID, name = name,
                available = available, page = nextPage, jenisPet = jenisPet, provinsi = provinsi, kota = kota,
                gender = gender, umurMax = umurMax, umurMin = umurMin).data!!
            LoadResult.Page(
                data = catListResponse.cat!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < catListResponse.lastPage!!.toInt())
                    catListResponse.currentPage!!.toInt().plus(1) else null
            )
        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}