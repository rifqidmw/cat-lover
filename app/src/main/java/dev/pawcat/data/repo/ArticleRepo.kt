package dev.pawcat.data.repo

import com.google.gson.JsonObject
import dev.pawcat.data.remote.ApiService
import dev.pawcat.data.remote.response.AddCommentArticleResponse
import dev.pawcat.data.remote.response.AddCommentPostResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import io.reactivex.Single

class ArticleRepo (private val service: ApiService){
    fun doLike(header: Map<String, String>, uuid: String) : Single<BaseResponse> {
        return service.likeArticle(header, uuid)
    }

    fun doAddComment(header: Map<String, String>, data: JsonObject) : Single<AddCommentArticleResponse> {
        return service.doAddArticleComment(header, data)
    }
}