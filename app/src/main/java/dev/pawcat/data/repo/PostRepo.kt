package dev.pawcat.data.repo

import com.google.gson.JsonObject
import dev.pawcat.data.remote.ApiService
import dev.pawcat.data.remote.response.AddCommentPostResponse
import dev.pawcat.data.remote.response.AddPostResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class PostRepo(private val service: ApiService) {
    fun doAddPost(header: Map<String, String>, filePath: String, content: String) : Single<AddPostResponse> {
        val file = File(filePath)
        val fileRequestBody = file.asRequestBody()
        val fileMultipartBody = MultipartBody.Part.createFormData("image",file.name,fileRequestBody)


        return service.addPost(header, content = content.toRequestBody(), image = fileMultipartBody)
    }

    fun doAddComment(header: Map<String, String>, data: JsonObject) : Single<AddCommentPostResponse> {
        return service.doAddPostComment(header, data)
    }

    fun doLike(header: Map<String, String>, uuid: String) : Single<BaseResponse> {
        return service.likePost(header, uuid)
    }

}