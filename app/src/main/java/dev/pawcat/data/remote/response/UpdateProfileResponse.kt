package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class UpdateProfileResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
}