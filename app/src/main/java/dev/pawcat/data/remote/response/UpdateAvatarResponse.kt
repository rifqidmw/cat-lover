package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class UpdateAvatarResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("avatar") val avatar: String ? = null
}