package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CommentPostListResponse : Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("total") val totalItem: Int? = null
        @SerializedName("current_page") val currentPage: Int? = null
        @SerializedName("perpage") val perpage: String? = null
        @SerializedName("last_page") val lastPage: String? = null
        @SerializedName("comment") val comment: List<Comment>? = null
    }

    class Comment : Serializable{
        @SerializedName("comment_uuid") var commentUUID: String? = null
        @SerializedName("post_uuid") var postUUID: String? = null
        @SerializedName("users_uuid") var usersUUID: String? = null
        @SerializedName("users_name") var userName: String? = null
        @SerializedName("users_avatar") var userAvatar: String? = null
        @SerializedName("content") var content: String? = null
        @SerializedName("created_at") var createdAt: String? = null
//        @SerializedName("balasan") val balasan: String? = null
    }
}