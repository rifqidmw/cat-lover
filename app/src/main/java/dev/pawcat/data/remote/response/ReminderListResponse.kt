package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ReminderListResponse: Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("total_item") val totalItem: Int? = null
        @SerializedName("current_page") val currentPage: Int? = null
        @SerializedName("perpage") val perpage: String? = null
        @SerializedName("last_page") val lastPage: Int? = null
        @SerializedName("post") val post: List<Reminder>? = null
    }

    class Reminder : Serializable{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("cats_uuid") val catsUUID: String? = null
        @SerializedName("cats_name") val catsName: String? = null
        @SerializedName("cats_image") val catsImage: String? = null
        @SerializedName("schedule") val schedule: String? = null
        @SerializedName("date") val date: String? = null
        @SerializedName("time") val time: String? = null
        @SerializedName("description") val description: String? = null
        @SerializedName("repeat") val repeat: String? = null
        @SerializedName("created_at") val createdAt: String? = null

    }
}