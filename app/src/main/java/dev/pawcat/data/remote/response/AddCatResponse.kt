package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class AddCatResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("users_uuid") val usersUUID: String? = null
        @SerializedName("users_name") val usersName: String? = null
        @SerializedName("users_avatar") val usersAvatar: String? = null
        @SerializedName("image") val image: String? = null
        @SerializedName("content") val content: String? = null
        @SerializedName("name") val name: String? = null
        @SerializedName("breed") val breed: String? = null
        @SerializedName("gender") val gender: String? = null
        @SerializedName("gender_text") val genderText: String? = null
        @SerializedName("age") val age: String? = null
        @SerializedName("weight") val weight: String? = null
        @SerializedName("available") val available: String? = null
        @SerializedName("like") val like: String? = null
        @SerializedName("comment") val comment: String? = null
        @SerializedName("created_at") val createdAt: String? = null
    }
}