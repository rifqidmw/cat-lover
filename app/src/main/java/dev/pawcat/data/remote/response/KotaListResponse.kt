package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class KotaListResponse: Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: List<Data>? = null

    class Data{
        @SerializedName("id_kota") val idKota: String? = null
        @SerializedName("id_provinsi") val idProvinsi: String? = null
        @SerializedName("name") val name: String? = null
    }
}