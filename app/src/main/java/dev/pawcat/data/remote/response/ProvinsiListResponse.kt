package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProvinsiListResponse: Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: List<Data>? = null

    class Data{
        @SerializedName("id_provinsi") val idProvinsi: String? = null
        @SerializedName("name") val name: String? = null
    }
}