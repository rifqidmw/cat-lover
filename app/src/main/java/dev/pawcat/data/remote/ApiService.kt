package dev.pawcat.data.remote

import com.google.gson.JsonObject
import dev.pawcat.data.remote.response.*
import dev.pawcat.data.remote.response.base.AddCommentCatResponse
import dev.pawcat.data.remote.response.base.BaseResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    //Auth
    @POST("login")
    fun doLogin(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<LoginResponse>

    @POST("register")
    fun doRegister(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<RegisterResponse>

    @Multipart
    @POST("update_avatar")
    fun doUpdateAvatar(
        @HeaderMap headers: Map<String, String>,
        @Part imageFile: MultipartBody.Part
    ) : Single<UpdateAvatarResponse>

    @POST("update_profile")
    fun doUpdateProfile(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<UpdateProfileResponse>

    @POST("update_password")
    fun doUpdatePassword(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<UpdatePasswordResponse>

    @GET("profile")
    fun getProfile(
        @HeaderMap headers: Map<String, String>
    ) : Single<GetProfileResponse>

    @GET("logout")
    fun doLogout(
        @HeaderMap headers: Map<String, String>
    ) : Single<LogoutResponse>

    //Post
    @Multipart
    @POST("post")
    fun addPost(
        @HeaderMap headers: Map<String, String>,
        @Part("content") content: RequestBody,
        @Part image: MultipartBody.Part
    ) : Single<AddPostResponse>

    @GET("post")
    suspend fun getPost(
        @HeaderMap headers: Map<String, String>,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?,
        @Query("user_uuid") user_uuid: String?,
        @Query("all") all: String?,
        @Query("content") content: String?
    ) : Response<PostListResponse>

    @GET("post/like/{uuid}")
    fun likePost(
        @HeaderMap headers: Map<String, String>,
        @Path("uuid") uuid: String?
    ) : Single<BaseResponse>

    @POST("post/add_comment")
    fun doAddPostComment(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<AddCommentPostResponse>


    //Cat
    @Multipart
    @POST("cat")
    fun addCat(
        @HeaderMap headers: Map<String, String>,
        @Part("name") name: RequestBody,
        @Part("content") content: RequestBody,
        @Part("breed") breed: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part("age") age: RequestBody,
        @Part("weight") weight: RequestBody,
        @Part("available") available: RequestBody,
        @Part("id_provinsi") idProvince: RequestBody,
        @Part("id_kota") idCity: RequestBody,
        @Part("jenis_pet") petType: RequestBody,
        @Part image: MultipartBody.Part
    ) : Single<AddCatResponse>

    @Multipart
    @POST("cat_galeri")
    fun addGallery(
        @HeaderMap headers: Map<String, String>,
        @Part("cats_uuid") catUUID: RequestBody,
        @Part image: List<MultipartBody.Part>
    ) : Single<BaseResponse>

    @GET("cat_galeri")
    fun getGallery(
        @HeaderMap headers: Map<String, String>,
        @Query("cats_uuid") catUUID: String
    ) : Single<CatGalleryResponse>

    @POST("cat/update")
    fun updateCat(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<BaseResponse>

    @GET("cat")
    suspend fun getCat(
        @HeaderMap headers: Map<String, String>,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?,
        @Query("user_uuid") user_uuid: String?,
        @Query("content") content: String?,
        @Query("name") name: String?,
        @Query("available") available: String?,
        @Query("jenis_pet") jenisPet: String?,
        @Query("id_provinsi") provinsi: String?,
        @Query("id_kota") kota: String?,
        @Query("gender") gender: String?,
        @Query("umur_max") umurMax: String?,
        @Query("umur_min") umurMin: String?
    ) : Response<CatListResponse>

    @GET("cat/destroy/{uuid}")
    fun deleteCat(
        @HeaderMap headers: Map<String, String>,
        @Path("uuid") uuid: String
    ) : Single<BaseResponse>

    @POST("cat/add_comment")
    fun doAddCatComment(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<AddCommentCatResponse>

    //Reminder
    @POST("reminder")
    fun doAddReminder(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<AddReminderResponse>

    @GET("reminder")
    suspend fun getReminder(
        @HeaderMap headers: Map<String, String>,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?,
        @Query("cats_uuid") cats_uuid: String?,
        @Query("content") content: String?
    ) : Response<ReminderListResponse>


    @PUT("reminder/{uuid}")
    fun updateReminder(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject,
        @Path("uuid") uuid: String
    ) : Single<BaseResponse>

    @GET("reminder/destroy/{uuid}")
    fun deleteReminder(
        @HeaderMap headers: Map<String, String>,
        @Path("uuid") uuid: String
    ) : Single<BaseResponse>

    //Comment
    @GET("post/comment/{post_uuid}")
    suspend fun getCommentPost(
        @HeaderMap headers: Map<String, String>,
        @Path("post_uuid") postUUID: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<CommentPostListResponse>

    @GET("cat/comment/{cat_uuid}")
    suspend fun getCommentCat(
        @HeaderMap headers: Map<String, String>,
        @Path("cat_uuid") catUUID: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<CommentCatListResponse>

    @GET("artikel/comment/{article_uuid}")
    suspend fun getCommentArticle(
        @HeaderMap headers: Map<String, String>,
        @Path("article_uuid") articleUUID: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<CommentArticleListResponse>

    //Users
    @GET("user")
    suspend fun getUserList(
        @HeaderMap headers: Map<String, String>,
        @Query("name") name: String?,
        @Query("status") status: Int?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<UserListResponse>

    @GET("user/detail/{uuid}")
    fun detailUser(
        @HeaderMap headers: Map<String, String>,
        @Path("uuid") uuid: String?
    ) : Single<DetailUserResponse>

    @GET("user/user_follow/{uuid}")
    fun followUser(
        @HeaderMap headers: Map<String, String>,
        @Path("uuid") uuid: String?
    ) : Single<BaseResponse>


    //Article
    @GET("artikel")
    suspend fun getArticleList(
        @HeaderMap headers: Map<String, String>,
        @Query("name") name: String?,
        @Query("content") content: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<ArticleListResponse>

    @GET("artikel/like/{uuid}")
    fun likeArticle(
        @HeaderMap headers: Map<String, String>,
        @Path("uuid") uuid: String?
    ) : Single<BaseResponse>

    @POST("artikel/add_comment")
    fun doAddArticleComment(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<AddCommentArticleResponse>

    //Filter
    @GET("provinsi")
    fun getProvinsi(
        @HeaderMap headers: Map<String, String>
    ) : Single<ProvinsiListResponse>

    @GET("kota/{id}")
    fun getKota(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: String?
    ) : Single<KotaListResponse>

    @GET("jenis_pet")
    fun getPetType(
        @HeaderMap headers: Map<String, String>
    ) : Single<PetTypeResponse>

    //Search
    @GET("search")
    suspend fun getSearchUserList(
        @HeaderMap headers: Map<String, String>,
        @Query("title") title: String?,
        @Query("type") type: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<UserListResponse>

    @GET("search")
    suspend fun getSearchPostList(
        @HeaderMap headers: Map<String, String>,
        @Query("title") title: String?,
        @Query("type") type: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<PostListResponse>

    @GET("search")
    suspend fun getSearchPetList(
        @HeaderMap headers: Map<String, String>,
        @Query("title") title: String?,
        @Query("type") type: String?,
        @Query("perpage") perpage: Int?,
        @Query("page") page: Int?
    ) : Response<CatListResponse>
}