package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PetTypeResponse: Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: List<Data>? = null

    class Data{
        @SerializedName("id") val id: Int? = null
        @SerializedName("title") val title: String? = null
    }
}