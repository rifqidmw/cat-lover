package dev.pawcat.data.remote.response.base

import com.google.gson.annotations.SerializedName

class BaseResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
}