package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserListResponse : Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("total") val totalItem: Int? = null
        @SerializedName("current_page") val currentPage: Int? = null
        @SerializedName("perpage") val perpage: String? = null
        @SerializedName("last_page") val lastPage: String? = null
        @SerializedName("user") val user: List<User>? = null
    }

    class User : Serializable{
        @SerializedName("uuid") var uuid: String? = null
        @SerializedName("users_name") var usersName: String? = null
        @SerializedName("users_email") var usersEmail: String? = null
        @SerializedName("users_no_telp") var usersPhone: String? = null
        @SerializedName("users_avatar") var userAvatar: String? = null
        @SerializedName("status_following") var statusFollowing: String? = null
        @SerializedName("followers") var followers: String? = null
        @SerializedName("following") var following: String? = null
        @SerializedName("cat") var cat: String? = null
        @SerializedName("post") var post: String? = null

    }
}