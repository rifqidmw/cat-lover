package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CatListResponse :Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("total_item") val totalItem: Int? = null
        @SerializedName("current_page") val currentPage: Int? = null
        @SerializedName("perpage") val perpage: String? = null
        @SerializedName("last_page") val lastPage: Int? = null
        @SerializedName("cat") val cat: List<Cat>? = null
    }

    class Cat : Serializable{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("users_uuid") val usersUUID: String? = null
        @SerializedName("users_name") val usersName: String? = null
        @SerializedName("users_avatar") val usersAvatar: String? = null
        @SerializedName("users_no_telp") val usersPhone: String? = null
        @SerializedName("image") val image: String? = null
        @SerializedName("content") val content: String? = null
        @SerializedName("name") val name: String? = null
        @SerializedName("breed") val breed: String? = null
        @SerializedName("gender") val gender: String? = null
        @SerializedName("gender_text") val genderText: String? = null
        @SerializedName("age") val age: String? = null
        @SerializedName("weight") val weight: String? = null
        @SerializedName("available") val available: String? = null
        @SerializedName("jenis_pet") val jenisPet: String? = null
        @SerializedName("jenis_pet_text") val jenisPetText: String? = null
        @SerializedName("id_kota") val idKota: String? = null
        @SerializedName("id_provinsi") val idProvinsi: String? = null
        @SerializedName("kota") val kota: String? = null
        @SerializedName("provinsi") val provinsi: String? = null
        @SerializedName("url_share") val urlShare: String? = null
        @SerializedName("galeri") val galeri: String? = null
        @SerializedName("like") val like: String? = null
        @SerializedName("status_like") val statusLike: String? = null
        @SerializedName("comment") val comment: String? = null
        @SerializedName("created_at") val createdAt: String? = null

    }
}