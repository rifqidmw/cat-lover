package dev.pawcat.data.remote

import dev.pawcat.data.local.model.ResultModel
import dev.pawcat.data.remote.exceptions.NoInternetException
import dev.pawcat.data.remote.exceptions.NotFoundException
import dev.pawcat.data.remote.exceptions.UnAuthorizedException
import dev.pawcat.data.remote.exceptions.UnKnownException
import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseService {

    protected suspend fun<T: Any> createCall(call: suspend () -> Response<T>) : ResultModel<T> {

        val response: Response<T>
        try {
            response = call.invoke()
        }catch (t: Throwable){
            t.printStackTrace()
            return ResultModel.Error(mapToNetworkError(t))
        }

        if (response.isSuccessful){
            if (response.body() != null){
                return ResultModel.Success(response.body()!!)
            }
        }
        else{
            val errorBody = response.errorBody()
            return if (errorBody != null){
                ResultModel.Error(mapApiException(response.code()))
            } else ResultModel.Error(mapApiException(0))
        }
        return ResultModel.Error(HttpException(response))
    }

    private fun mapApiException(code: Int): Exception {
        return when(code){
            HttpURLConnection.HTTP_NOT_FOUND -> NotFoundException()
            HttpURLConnection.HTTP_UNAUTHORIZED -> UnAuthorizedException()
            else -> UnKnownException()
        }
    }

    private fun mapToNetworkError(t: Throwable): Exception {
        return when(t){
            is SocketTimeoutException
                -> SocketTimeoutException("Connection Timed Out")
            is UnknownHostException
                -> NoInternetException()
            else
                -> UnKnownException()

        }
    }
}