package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ArticleListResponse : Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("total_item") val totalItem: Int? = null
        @SerializedName("current_page") val currentPage: Int? = null
        @SerializedName("perpage") val perpage: String? = null
        @SerializedName("last_page") val lastPage: Int? = null
        @SerializedName("artikel") val article: List<Article>? = null
    }

    class Article : Serializable{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("users_uuid") val usersUUID: String? = null
        @SerializedName("users_name") val usersName: String? = null
        @SerializedName("users_avatar") val usersAvatar: String? = null
        @SerializedName("image") val image: String? = null
        @SerializedName("kategori_uuid") val kategoriUUID: String? = null
        @SerializedName("kategori_title") val kategoriTitle: String? = null
        @SerializedName("title") val title: String? = null
        @SerializedName("content") val content: String? = null
        @SerializedName("like") val like: String? = null
        @SerializedName("status_like") val statusLike: String? = null
        @SerializedName("comment") val comment: String? = null
        @SerializedName("created_at") val createdAt: String? = null
    }
}