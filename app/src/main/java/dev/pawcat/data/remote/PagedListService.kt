package dev.pawcat.data.remote

import dev.pawcat.data.local.model.ResultModel
import dev.pawcat.data.remote.response.*

class PagedListService(private val apiService: ApiService) : BaseService() {

    suspend fun fetchPostList(header: Map<String, String>, uuid: String, all: String, perpage: Int, page: Int) : ResultModel<PostListResponse>{
        return createCall { apiService.getPost(
            headers = header,
            perpage = perpage,
            page = page,
            user_uuid = uuid,
            content = "",
            all = all
        ) }
    }

    suspend fun fetchCatList(header: Map<String, String>, userUUID: String, name: String,
                             available: String, jenisPet: String, provinsi: String, kota: String,
                             gender: String, umurMax: String, umurMin: String, perpage: Int, page: Int) : ResultModel<CatListResponse>{
        return createCall { apiService.getCat(
            headers = header,
            perpage = perpage,
            page = page,
            user_uuid = userUUID,
            content = "",
            name = name,
            available = available,
            jenisPet = jenisPet,
            provinsi = provinsi,
            kota = kota,
            gender = gender,
            umurMax = umurMax,
            umurMin = umurMin
        ) }
    }

    suspend fun fetchReminderList(header: Map<String, String>, perpage: Int, page: Int) : ResultModel<ReminderListResponse>{
        return createCall { apiService.getReminder(
            headers = header,
            perpage = perpage,
            page = page,
            cats_uuid = "",
            content = ""
        ) }
    }

    suspend fun fetchCommentPostList(header: Map<String, String>, postUUID: String, perpage: Int, page: Int) : ResultModel<CommentPostListResponse>{
        return createCall { apiService.getCommentPost(
            headers = header,
            postUUID = postUUID,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchCommentCatList(header: Map<String, String>, catUUID: String, perpage: Int, page: Int) : ResultModel<CommentCatListResponse>{
        return createCall { apiService.getCommentCat(
            headers = header,
            catUUID = catUUID,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchCommentArticleList(header: Map<String, String>, articleUUID: String, perpage: Int, page: Int) : ResultModel<CommentArticleListResponse>{
        return createCall { apiService.getCommentArticle(
            headers = header,
            articleUUID = articleUUID,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchUserList(header: Map<String, String>, name: String, status: Int, perpage: Int, page: Int) : ResultModel<UserListResponse>{
        return createCall { apiService.getUserList(
            headers = header,
            name = name,
            status = status,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchArticleList(header: Map<String, String>, name: String, content: String, perpage: Int, page: Int) : ResultModel<ArticleListResponse>{
        return createCall { apiService.getArticleList(
            headers = header,
            name = name,
            content = content,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchSearchUserList(header: Map<String, String>, title: String, type: String, perpage: Int, page: Int) : ResultModel<UserListResponse>{
        return createCall { apiService.getSearchUserList(
            headers = header,
            title = title,
            type = type,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchSearchPostList(header: Map<String, String>, title: String, type: String, perpage: Int, page: Int) : ResultModel<PostListResponse>{
        return createCall { apiService.getSearchPostList(
            headers = header,
            title = title,
            type = type,
            perpage = perpage,
            page = page
        ) }
    }

    suspend fun fetchSearchPetList(header: Map<String, String>, title: String, type: String, perpage: Int, page: Int) : ResultModel<CatListResponse>{
        return createCall { apiService.getSearchPetList(
            headers = header,
            title = title,
            type = type,
            perpage = perpage,
            page = page
        ) }
    }
}