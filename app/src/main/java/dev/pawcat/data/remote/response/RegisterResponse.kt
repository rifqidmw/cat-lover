package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class RegisterResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("token") val token: String ? = null
}