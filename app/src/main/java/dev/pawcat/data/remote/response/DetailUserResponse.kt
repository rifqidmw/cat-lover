package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class DetailUserResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null
    
    class Data{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("users_name") val usersName: String? = null
        @SerializedName("users_email") val usersEmail: String? = null
        @SerializedName("users_no_telp") val usersPhone: String? = null
        @SerializedName("users_avatar") val usersAvatar: String? = null
        @SerializedName("status_following") val statusFollowing: String? = null
        @SerializedName("followers") val follower: String? = null
        @SerializedName("following") val following: String? = null
        @SerializedName("cat") val cat: String? = null
        @SerializedName("post") val post: String? = null
    }
}