package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class LoginResponse{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("error") val error: String? = null
    @SerializedName("token") val token: String ? = null
}
