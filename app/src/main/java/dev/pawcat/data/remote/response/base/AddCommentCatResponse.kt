package dev.pawcat.data.remote.response.base

import com.google.gson.annotations.SerializedName

class AddCommentCatResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("comment_uuid") val commentUUID: String? = null
        @SerializedName("cat_uuid") val catUUID: String? = null
        @SerializedName("users_uuid") val usersUUID: String? = null
        @SerializedName("users_name") val usersName: String? = null
        @SerializedName("users_avatar") val usersAvatar: String? = null
        @SerializedName("content") val content: String? = null
        @SerializedName("parent_uuid") val parentUUID: String? = null
        @SerializedName("created_at") val createdAt: String? = null
    }
}