package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName

class GetProfileResponse {
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null
    
    class Data{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("name") val name: String? = null
        @SerializedName("email") val email: String? = null
        @SerializedName("no_telp") val noTelp: String? = null
        @SerializedName("post") val post: String? = null
        @SerializedName("cat") val cat: String? = null
        @SerializedName("cat_available") val catAvailable: String? = null
        @SerializedName("reminder") val reminder: String? = null
        @SerializedName("followers") val followers: String? = null
        @SerializedName("following") val following: String? = null
        @SerializedName("avatar") val avatar: String? = null
    }
}