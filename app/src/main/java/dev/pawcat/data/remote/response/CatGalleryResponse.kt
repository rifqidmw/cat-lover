package dev.pawcat.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CatGalleryResponse : Serializable{
    @SerializedName("code") val code: String? = null
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    class Data{
        @SerializedName("total_item") val totalItem: Int? = null
        @SerializedName("current_page") val currentPage: Int? = null
        @SerializedName("perpage") val perpage: String? = null
        @SerializedName("last_page") val lastPage: Int? = null
        @SerializedName("post") val post: List<Post>? = null
    }

    class Post : Serializable{
        @SerializedName("uuid") val uuid: String? = null
        @SerializedName("cats_uuid") val catsUUID: String? = null
        @SerializedName("cats_name") val catsName: String? = null
        @SerializedName("cats_image") val catsImage: String? = null
        @SerializedName("type") val type: String? = null
        @SerializedName("url_file") val urlFile: String? = null
        @SerializedName("created_at") val createdAt: String? = null
    }
}