package dev.pawcat.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.github.marlonlom.utilities.timeago.TimeAgo
import com.github.marlonlom.utilities.timeago.TimeAgoMessages
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mikhaellopez.circularimageview.CircularImageView
import dev.pawcat.R
import dev.pawcat.data.local.Session
import dev.pawcat.ui.detailuser.DetailUserActivity
import org.koin.android.BuildConfig
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Period
import org.threeten.bp.format.DateTimeFormatter
import java.io.*
import java.lang.Math.log10
import java.lang.reflect.Type
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.pow


fun toJsonElement(any: Any): JsonElement = Gson().toJsonTree(any)

fun fromJsonToList(data: String) : List<String>{
    val listType: Type = object : TypeToken<List<String?>?>() {}.type

    return Gson().fromJson(
        data,
        listType
    )
}

fun logI(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.i(tag, msg)
}

fun logW(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.w(tag, msg)
}

fun logE(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.e(tag, msg)
}

fun logD(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.d(tag, msg)
}

fun logV(tag: String, msg: String){
    if (BuildConfig.DEBUG) Log.v(tag, msg)
}

fun EditText.getString() = text.toString()

fun Chip.getString() = text.toString()

fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.toastLong(message: String){
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun regex(): Regex{
    val regex = Regex("[^A-Za-z0-9 ]")

    return regex
}

fun removeSpecialCharInInt(data: String):Int{
    return regex().replace(data, "").toInt()
}

fun getDateFormat(): SimpleDateFormat {
    val dateFormat = SimpleDateFormat("MM/dd/yy, hh:mm:ss a")

    return dateFormat
}

fun getTimeNow(format: String = "MM/dd/yy, hh:mm:ss a"):String{
    val dateFormat = SimpleDateFormat(format)
    return dateFormat.format(Date())
}

fun getTimestamp(): String{
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        return DateTimeFormatter.ISO_INSTANT.format(Instant.now()).toString()
    } else {
        return System.currentTimeMillis().toString()
    }
}

fun deleteFile(target: String) {
    val path = File(target)
    path.delete()
    if (path.exists() && path.isDirectory) {
        for (child in path.listFiles()!!) {
            child.delete()
        }
    }
}

fun hideKeyboard(activity: Activity){
    val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus

    if (view == null){
        view = View(activity)
    }

    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Long.timeMillisToDateTime(format: String? = null):String{
    var format = format
    if (format == null){
        format = "MM/dd/yy, hh:mm:ss aa"
    }
    val sdf = SimpleDateFormat(format)
    return sdf.format(Date(this))
}

fun Long.timeMillisToDate(format: String? = null):String{
    var format = format
    if (format == null){
        format = "MM/dd/yy"
    }
    val sdf = SimpleDateFormat(format)
    return sdf.format(Date(this))
}

fun Long.timeMillisToTime(format: String? = null):String{
    var format = format
    if (format == null){
        format = "hh:mm a"
    }
    val sdf = SimpleDateFormat(format)
    return sdf.format(Date(this))
}

@SuppressLint("SimpleDateFormat")
fun String.changeDateFormat(): String{
    return if (Build.VERSION.SDK_INT < 26){
        val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val formatter = SimpleDateFormat("dd MM yyyy, HH:mm")
        val formattedDate = formatter.format(parser.parse(this))

        formattedDate
    } else {
        val parsedDate = LocalDateTime.parse(this, DateTimeFormatter.ISO_DATE_TIME)
        val formattedDate = parsedDate.format(DateTimeFormatter.ofPattern("dd MM yyyy, HH:mm"))

        formattedDate
    }
}

fun String.getYear(): Int{
    val parser =  SimpleDateFormat("yyyy-MM-dd")
    val formatter = SimpleDateFormat("yyyy")
    val formattedDate = formatter.format(parser.parse(this))

    return formattedDate.toInt()
}

fun String.getMonth(): Int{
    val parser =  SimpleDateFormat("yyyy-MM-dd")
    val formatter = SimpleDateFormat("MM")
    val formattedDate = formatter.format(parser.parse(this))

    return formattedDate.toInt()
}

fun String.getDay(): Int{
    val parser =  SimpleDateFormat("yyyy-MM-dd")
    val formatter = SimpleDateFormat("dd")
    val formattedDate = formatter.format(parser.parse(this))

    return formattedDate.toInt()
}

fun String.getBOD(): String{

    val year = this.getYear()
    val month = this.getMonth()
    val dayOfMonth = this.getDay()

    return Period.between(
        LocalDate.of(year, month, dayOfMonth),
        LocalDate.now()
    ).months.toString()
}

fun ChipGroup.addChip(context: Context, label: String, checked: Boolean){
    Chip(ContextThemeWrapper(context, R.style.Widget_MaterialComponents_Chip_Filter)).apply {
        id = View.generateViewId()
        text = label
        isChecked = checked
        isClickable = true
        isCheckable = true
        isCheckedIconVisible = true
        isFocusable = true
        addView(this)
    }
}

fun Context.getParamHeader() : HashMap<String, String>{
    val session = Session(this)
    val headers = HashMap<String, String>()
    headers["Accept"] = "application/json"
    headers["Authorization"] = "Bearer ${session[USER_TOKEN]}"

    return headers
}

fun Context.getUserUUID(): String{
    val session = Session(this)

    return session[USER_UUID]!!
}

fun Context.intentToUser(uuid: String?){
    val intent = Intent(this, DetailUserActivity::class.java)
    intent.putExtra(USER_UUID, uuid)
    startActivity(intent)
}

fun ConstraintLayout.setConstraintBackground(context: Context, imgUrl: Int){
    this.background = ContextCompat.getDrawable(context, imgUrl)
}

fun ImageView.setImageviewBackground(context: Context, imgUrl: Int){
    this.background = ContextCompat.getDrawable(context, imgUrl)
}

fun ImageView.setImageviewLocal(context: Context, imgUrl: Int){
    Glide.with(context)
        .asBitmap()
        .load(imgUrl)
        .placeholder(R.drawable.background_gradient_gray)
        .override(600, 600)
        .error(R.drawable.ic_no_image)
        .into(this)
}

fun ImageView.setImageviewUrl(context: Context, imgUrl: String){

    Glide.with(context)
        .asBitmap()
        .load(imgUrl)
        .placeholder(R.drawable.background_gradient_gray)
        .override(600, 600)
        .error(R.drawable.ic_no_image)
        .into(this)
}

fun ImageView.setImageviewCenterUrl(context: Context, imgUrl: String){
    /*val requestOptions = RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
        .skipMemoryCache(true)*/
    Glide.with(context)
        .asBitmap()
        .load(imgUrl)
        .placeholder(R.drawable.background_gradient_gray)
        .error(R.drawable.ic_no_image)
        .override(600, 600)
//        .apply(requestOptions)
        .centerCrop()
        .into(this)
}

fun CircularImageView.setPhotoCircularLocal(context: Context, imgUrl: Int){

    Glide.with(context)
        .asBitmap()
        .load(imgUrl)
        .placeholder(R.drawable.background_gradient_gray)
        .override(600, 600)
        .error(R.drawable.ic_no_image)
        .into(this)
}

fun CircularImageView.setPhotoCircularUrl(context: Context, imgUrl: String){

    Glide.with(context)
        .asBitmap()
        .load(imgUrl)
        .placeholder(R.drawable.background_gradient_gray)
        /*.diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)*/
        .override(600, 600)
//        .apply(requestOptions)
        .error(R.drawable.ic_no_image)
        .into(this)
}


fun Context.setupStatusBarGradient(window: Window, context: Context){
    val background: Drawable = ContextCompat.getDrawable(context, R.drawable.background_gradient)!!
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.statusBarColor = Color.TRANSPARENT
    window.navigationBarColor = Color.TRANSPARENT
    window.setBackgroundDrawable(background)
}



fun Context.showDatePickerDialog(dateChoose: ((date: String) -> Unit?)? = null){
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)

    val dpd = DatePickerDialog(
        this,
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            dateChoose?.invoke("$year-${monthOfYear + 1}-$dayOfMonth")
        },
        year,
        month,
        day
    )
    dpd.show()
}

fun Context.showTimePickerDialog(timeChoose: ((time: String) -> Unit?)? = null) {
    val c = Calendar.getInstance()
    val hour = c.get(Calendar.HOUR)
    val minute = c.get(Calendar.MINUTE)

    val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->

        timeChoose?.invoke("$h : $m")

    }), hour, minute, true)

    tpd.show()
}

fun Context.showDialogDelete(title: String, doDelete: (() -> Unit?)? = null){

    val customView = LayoutInflater.from(this).inflate(R.layout.dialog_delete_item, null, false)
    val dialog = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
        .setView(customView)
        .setCancelable(true)
        .create()

    dialog.show()
    val tvMessage = customView.findViewById<TextView>(R.id.tv_message)
    val btnDelete = customView.findViewById<Button>(R.id.btn_delete)
    val btnCancel = customView.findViewById<Button>(R.id.btn_cancel)

    btnCancel.setOnClickListener {
        dialog.dismiss()
    }

    btnDelete.setOnClickListener {
        doDelete?.invoke()
        dialog.dismiss()
    }

    tvMessage.text = title
}

fun Context.showDialogAdopted(title: String, doDelete: (() -> Unit?)? = null){

    val customView = LayoutInflater.from(this).inflate(R.layout.dialog_update_item, null, false)
    val dialog = MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_rounded)
        .setView(customView)
        .setCancelable(true)
        .create()

    dialog.show()
    val tvMessage = customView.findViewById<TextView>(R.id.tv_message)
    val btnDelete = customView.findViewById<Button>(R.id.btn_delete)
    val btnCancel = customView.findViewById<Button>(R.id.btn_cancel)

    btnCancel.setOnClickListener {
        dialog.dismiss()
    }

    btnDelete.setOnClickListener {
        doDelete?.invoke()
        dialog.dismiss()
    }

    tvMessage.text = title
}

fun Context.showSettingsDialog(
    doPositiveButton: (() -> Unit?)? = null,
    doNegativeButton: (() -> Unit?)? = null
){
    val builder = AlertDialog.Builder(this)
    /* builder.setTitle(getString(R.string.dialog_permission_title))
     builder.setMessage(getString(R.string.dialog_permission_message))*/
    builder.setPositiveButton("") { dialog, which ->
        dialog.cancel()
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
        doPositiveButton?.invoke()
    }
    builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which ->
        dialog.cancel()
        doNegativeButton?.invoke()
    }
    builder.show()
}

fun Activity.setupPermissionsCameraView(doGranted: () -> Unit, doOnNoGrandted: () -> Unit) {

    Dexter.withActivity(this)
        .withPermissions(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        .withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                if (report.areAllPermissionsGranted()) {
                    doGranted()
                }



                if (report.isAnyPermissionPermanentlyDenied) {
                    doOnNoGrandted()
                }

            }

            override fun onPermissionRationaleShouldBeShown(
                permissions: MutableList<PermissionRequest>?,
                token: PermissionToken?
            ) {
                token?.continuePermissionRequest()
            }
        }
        ).check()

}

@SuppressLint("SimpleDateFormat")
fun String.setTimeAgo() : String{
    val timemillis = SimpleDateFormat("dd MM yyyy, HH:mm").parse(this).time

    val localeLang = Locale.forLanguageTag("en")
    val builder = TimeAgoMessages.Builder().withLocale(localeLang).build()

    val timeAgo = TimeAgo.using(timemillis, builder)

    return timeAgo
}

fun createVideoCacheDir(){
    val sdCard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
    val dir = File(sdCard.absolutePath + "/pawcats/cache/video")
    if (!dir.exists()) {
        dir.mkdirs()
    }
}

fun Context.saveBitmapToLocal(image: Bitmap, email: String): String? {
    var savedImagePath: String? = null
    val imageFileName = "JPEG_$email.jpg"
    val storageDir = File(this.externalCacheDir!!.absolutePath, "/avatar/$imageFileName")
    var success = true
    if (!storageDir.exists()) {
        success = storageDir.mkdirs()
    }
    if (success) {
        val imageFile = File(storageDir, imageFileName)
        savedImagePath = imageFile.getAbsolutePath()
        try {
            val fOut: OutputStream = FileOutputStream(imageFile)
            image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
            fOut.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    return savedImagePath
}

@Suppress("DEPRECATION")
fun Context.saveByteArrayImageToLocal(dataImage: ByteArray, email: String) : File{
    val bMap = BitmapFactory.decodeByteArray(dataImage, 0, dataImage.size)

    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
    val imageFileName = "IMG_${timeStamp}.png"
    val sdCard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
    val dir = File(sdCard.absolutePath + "/pawcats/cache/image")
    if (!dir.exists()) {
        dir.mkdirs()
    }
    var file = File(dir.absolutePath, imageFileName)

    if (file.exists()) {
        file.deleteOnExit()
        file = File(dir.absolutePath, imageFileName)
    }

    val fos = FileOutputStream(file)
    bMap.compress(Bitmap.CompressFormat.JPEG, 40, fos)

    fos.flush()
    fos.close()

    bMap.recycle()

    return file
}

fun Context.saveImageToLocal(data: File, email: String) : String {
    val inputStream = FileInputStream(data.absolutePath)
    var compressBitmap = BitmapFactory.decodeStream(inputStream)

    val mediaStorageDir = File(this.externalCacheDir!!.absolutePath, "/avatar/$email.jpg")
    if (!mediaStorageDir.exists()) {
        mediaStorageDir.mkdirs()
    }
    var file = File(mediaStorageDir.absolutePath, data.name)

    if (file.exists()) {
        file.deleteOnExit()
        file = File(mediaStorageDir.absolutePath, data.name)
    }

    val fos = FileOutputStream(file)
    compressBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos)

    fos.flush()
    fos.close()

    inputStream.close()
    compressBitmap.recycle()

    return file.absolutePath
}

fun getMediaPath(context: Context, uri: Uri): String {

    val resolver = context.contentResolver
    val projection = arrayOf(MediaStore.Video.Media.DATA)
    var cursor: Cursor? = null
    try {
        cursor = resolver.query(uri, projection, null, null, null)
        return if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(columnIndex)

        } else ""

    } catch (e: Exception) {
        resolver.let {
            val filePath = (context.applicationInfo.dataDir + File.separator
                    + System.currentTimeMillis())
            val file = File(filePath)

            resolver.openInputStream(uri)?.use { inputStream ->
                FileOutputStream(file).use { outputStream ->
                    val buf = ByteArray(4096)
                    var len: Int
                    while (inputStream.read(buf).also { len = it } > 0) outputStream.write(
                        buf,
                        0,
                        len
                    )
                }
            }
            return file.absolutePath
        }
    } finally {
        cursor?.close()
    }
}

fun getFileSize(size: Long): String {
    if (size <= 0)
        return "0"

    val units = arrayOf("B", "KB", "MB", "GB", "TB")
    val digitGroups = (log10(size.toDouble()) / log10(1024.0)).toInt()

    return DecimalFormat("#,##0.#").format(
        size / 1024.0.pow(digitGroups.toDouble())
    ) + " " + units[digitGroups]
}

//The following methods can be alternative to [getMediaPath].
// todo(abed): remove [getPathFromUri], [getVideoExtension], and [copy]
fun getPathFromUri(context: Context, uri: Uri): String {
    var file: File? = null
    var inputStream: InputStream? = null
    var outputStream: OutputStream? = null
    var success = false
    try {
        val extension: String = getVideoExtension(uri)
        inputStream = context.contentResolver.openInputStream(uri)
        file = File.createTempFile("compressor", extension, context.cacheDir)
        file.deleteOnExit()
        outputStream = FileOutputStream(file)
        if (inputStream != null) {
            copy(inputStream, outputStream)
            success = true
        }
    } catch (ignored: IOException) {
    } finally {
        try {
            inputStream?.close()
        } catch (ignored: IOException) {
        }
        try {
            outputStream?.close()
        } catch (ignored: IOException) {
            // If closing the output stream fails, we cannot be sure that the
            // target file was written in full. Flushing the stream merely moves
            // the bytes into the OS, not necessarily to the file.
            success = false
        }
    }
    return if (success) file!!.path else ""
}

/** @return extension of video with dot, or default .mp4 if it none.
 */
private fun getVideoExtension(uriVideo: Uri): String {
    var extension: String? = null
    try {
        val imagePath = uriVideo.path
        if (imagePath != null && imagePath.lastIndexOf(".") != -1) {
            extension = imagePath.substring(imagePath.lastIndexOf(".") + 1)
        }
    } catch (e: Exception) {
        extension = null
    }
    if (extension == null || extension.isEmpty()) {
        //default extension for matches the previous behavior of the plugin
        extension = "mp4"
    }
    return ".$extension"
}

private fun copy(`in`: InputStream, out: OutputStream) {
    val buffer = ByteArray(4 * 1024)
    var bytesRead: Int
    while (`in`.read(buffer).also { bytesRead = it } != -1) {
        out.write(buffer, 0, bytesRead)
    }
    out.flush()
}
