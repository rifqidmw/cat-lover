package dev.pawcat.utils.view.imageslider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.SliderViewAdapter
import dev.pawcat.R


class ImageSliderAdapter(val context: Context, var mSliderItems: ArrayList<ImageSliderModel>) :
    SliderViewAdapter<ImageSliderAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): MyViewHolder {
        val inflate: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_image_slider, null)
        return MyViewHolder(inflate)
    }

    override fun onBindViewHolder(
        holder: MyViewHolder,
        position: Int
    ) {
        val sliderItem = mSliderItems[position]
        Glide.with(context).load(sliderItem.imgUrl)
            .centerInside()
            .into(holder.imgCover)
    }

    override fun getCount(): Int {
        //slider view count could be dynamic size
        return mSliderItems.size
    }

    class MyViewHolder(itemView: View) :
        ViewHolder(itemView) {
        internal var imgCover: ImageView = itemView.findViewById(R.id.img_cover_slider)
    }
}