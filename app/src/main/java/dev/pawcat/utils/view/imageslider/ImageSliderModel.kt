package dev.pawcat.utils.view.imageslider

data class ImageSliderModel(
    var id: String,
    var imgUrl: String
)