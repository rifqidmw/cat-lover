package dev.pawcat.utils

object ExtKt {
    @JvmStatic
    fun List<String>.formatToString(): String {
        return this.joinToString()
    }
}