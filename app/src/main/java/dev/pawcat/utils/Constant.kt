package dev.pawcat.utils

//FIRST TIME
const val IS_FIRST_TIME = "is_first_time"

//USER_PROFILE
const val USER_TOKEN = "token"
const val USER_UUID = "user_uuid"
const val USERNAME = "username"
const val USER_EMAIL = "user_email"
const val USER_PHONE = "user_phone"
const val USER_POST = "user_post"
const val USER_CAT = "user_cat"
const val USER_CAT_AVAILABLE = "user_cat_available"
const val USER_REMINDER = "user_reminder"
const val USER_FOLLOWER = "user_follower"
const val USER_FOLLOWING = "user_following"
const val USER_AVATAR ="user_avatar"

//INTENT DATA
const val DATA_POST = "data_post"
const val DATA_CAT = "data_cat"
const val DATA_REMINDER = "data_reminder"
const val DATA_ARTICLE = "data_article"

//INTENT TYPE INPUT
const val INPUT_TYPE = "input_type"
const val INPUT_NEW = "input_new"
const val INPUT_UPDATE = "input_update"

//INTENT TYPE FOLLOW
const val FOLLOW_TYPE = "follow_type"
const val FOLLOW_FOLLOWING = "follow_following"
const val FOLLOW_FOLLOWER = "follow_follower"

//INTENT INPUT SUCCESS
const val INPUT_SUCCESS = "input_success"
const val INPUT_SUCCESS_POST = "input_success_post"
const val INPUT_SUCCESS_CAT = "input_success_cat"
const val INPUT_SUCCESS_UPDATE_CAT = "input_success_update_cat"
const val INPUT_SUCCESS_REMINDER = "input_success_reminder"
const val INPUT_SUCCESS_UPDATE_REMINDER = "input_success_update_reminder"
